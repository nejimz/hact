@extends("hact.layouts.layout_admin")

@section("content")

    <div class='row'>
        <div class='large-12 columns'>
            <ul class="breadcrumbs">
                <li><a href="{{ route('dashboard') }}">Home</a></li>
                <li><a href="{{ route('item_list') }}">Medicine</a></li>
                <li class="current"><a href="#">New Medicine</a></li>
            </ul>
        </div>
    </div>

    <div class="panel">
		<div class="row">
			<div class="large-12 columns">
				@include("hact.messages.success")
				@include("hact.messages.error_list")
				<form method="post" action="{{ $action }}">
					<fieldset>
						<legend>New Medicine</legend>
						<div class="row">
							<div class="large-12 columns">
								<label>Drug Description and Formulation:</label>
								<input type="text" id="item_name" name="item_name" placeholder="Drug Description and Formulation" value="{{ $item_name }}">
							</div>
						</div>
						<div class="row">
							<div class="large-6 columns">
								<label>Code:</label>
								<input type="text" id="item_code" name="item_code" placeholder="Item Code" value="{{ $item_code }}">
							</div>
							<div class="large-6 columns">
								<label>LOT Number:</label>
								<input type="text" id="lot_number" name="lot_number" placeholder="LOT Number" value="{{ $lot_number }}">
							</div>
						</div>
						<div class="row">
							<div class="large-6 columns">
								<label>Quantity per Bottle:</label>
								<input type="number" id="quantity_per_bottle" name="quantity_per_bottle" placeholder="Quantity" value="{{ $quantity_per_bottle }}">
							</div>
							<div class="large-6 columns">
								<label>Expiry Date:</label>
								<input type="text" id="expiration_date" class="fdatepicker" name="expiration_date" placeholder="MM dd,yyyy" value="{{ $expiration_date }}" readonly>	
							</div>
						</div>
						<div class="row">	
							<div class="large-12 columns">
								<button type="submit" class="button small alert">Save</button>
					    		<a class="button small info" href="{{ route('item_list') }}"><strong>Back</strong></a>
								{!! csrf_field() !!}
							</div>
						</div>
					</fieldset>
				</form>
			</div>	
		</div>
	</div>

@endsection