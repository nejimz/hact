@extends('hact.layouts.layout_admin')

@section('content')

	<div class='row'>
		<div class='large-7 columns'>
			<ul class="breadcrumbs">
				<li><a href="{{ route('dashboard') }}">Home</a></li>
				<li class="current"><a href="#">Medicine</a></li>
			</ul>
		</div>
		<div class='large-5 columns'>
			<form>
				<div class="row">
					<div class="large-12 columns">
						<div class="row collapse">
							<div class="small-9 columns">
								<input name="search" type="text" placeholder="Search" value="{{ $search }}">
							</div>
					    	<div class="small-3 columns">
					      		<button type="submit" class="button alert postfix"><i class="fa fa-search"></i></buatton>
					    	</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class='row'>
		<div class='large-12 columns'>
			@include('hact.messages.success')
			@include('hact.messages.error_list')
			{!! str_replace('/?', '?', $items->appends($pagination)->render()) !!}

			<table width="100%">
				<thead>
					<tr>
						<th width="12%"></th>
						<th width="18%"><a href="{{ $item_code_sort }}">Medicine Code</a></th>
						<th width="30%"><a href="{{ $item_name_sort }}">Medicine Name</a></th>
						<th width="18%"><a href="{{ $lot_number_sort }}">Lot Number</a></th>
						<th width="8%"><a href="{{ $quantity_per_bottle_sort }}">Qty./Btl.</a></th>
						<th width="14%"><a href="{{ $expiration_date_sort }}">Expiry Date</a></th>
					</tr>
				</thead>
				<tbody>
				@foreach($items as $item)
				<tr>
					<td>
						<div id="myModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
						  <h2 id="modalTitle">Delete Confirmation</h2>
						  <p class="lead"></p>
						  <p>You are about to delete an item. It cannot be restored once it is deleted. Please review your decision before proceeding.</p>
						  <a class="close-reveal-modal" aria-label="Close">&#215;</a><a class = "button alert round" href="{{ route('item_destroy',$item->id) }}">Continue</a>
						</div>
						<a href="{{ route('item_edit', $item->id) }}" title="Edit"><i class="fa fa-lg fa-pencil-square-o"></i></a>
						<a href="#" data-reveal-id="myModal" title="Delete"><i class="fa fa-lg fa-times-circle"></i></a> 
						<a href="{{ route('receive_create',['id' => $item->id, 'search_item' => $item->item_name]) }}" title="Receive"><i class="fa fa-lg fa-plus-circle"></i></a>
						<a href="{{ route('dispense_create',['id' => $item->id, 'search_item' => $item->item_name]) }}" title="Dispense"><i class="fa fa-lg fa-minus-circle"></i></a>
					</td>
					<td>{{ $item->item_code }}</td>
					<td>{{ $item->item_name }}</td>
					<td>{{ $item->lot_number }}</td>
					<td>{{ $item->total_quantity }}</td>
					<td>{{ $item->expiration_date }}</td>
				</tr>
				@endforeach	
				</tbody>
			</table>
			{!! str_replace('/?', '?', $items->appends($pagination)->render()) !!}
		</div>
	</div>

@endsection