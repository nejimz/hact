@extends('hact.layouts.layout_admin')

@section('content')

    <div class='row'>
        <div class='large-12 columns'>
            <ul class="breadcrumbs">
                <li><a href="{{ route('dashboard') }}">Home</a></li>
                <li><a href="{{ route('item_list') }}">Items</a></li>
                <li class="current"><a href="#">New Item</a></li>
            </ul>
        </div>
    </div>

	<form method='post' action="{{ route('item_update',[$item->id]) }}">
		@include('hact.messages.success')
		@include('hact.messages.error_list')
		<div class='row'>
			<div class='large-12 columns'>
				<label>Item Name:
				<input type='text' id='item_name' name='item_name' placeholder='Item Name' value='{{ $item->item_name }}'>
				</label>
			</div>
		</div>
		
		<div class='row'>
			<div class='large-6 columns'>
				<label>Unit:
					<input type='text' id='unit' name='unit' placeholder='Unit' value='{{ $item->unit }}'>
				</label>
			</div>
			<div class='large-6 columns'>
				<label>Quantity per Bottle:
					<input type='number' id='quantity_per_bottle' name='quantity_per_bottle' placeholder='Quantity Per Bottle' value='{{ $item->quantity_per_bottle }}'>
				</label>
			</div>
		</div>
					
		<div class='row'>	
			<div class='large-12 columns'>
				<input type='submit' value='submit'>	
				{!! csrf_field() !!}
			</div>
		</div>
	</form>
	
@endsection