@extends('hact.layouts.layout_admin')

@section('content')
<div class="panel">
<div class="row box">
	<div class="large-11 column text-center">HIV CARE FORM</div>
	<div class="large-1 column text-center">C</div>
</div>
<div class="row box">
	<div class="large-12 column text-center">
		The law on Reporting Disease (R.A. 3573) & the Philippine AIDS Prevention and Control Act (R.A. 8504) requires phycisians to report all diagnosed HIVinfections to the HIV & AIDS Registrar at the Epidermiology Bureau, DOH Please write in CAPITAL LETTERS and CHECKS the appropriate boxes.
	</div>
</div>
<div class="row box">
	<div class="row">
		<div class="large-12 columns">&nbsp;</div>
	</div>
	<div class="large-3 column">
		<input type="checkbox" name="art_enrollment"> ART Enrollment
		<br>
		<input type="checkbox" name="refill"> Refill
	</div>
	<div class="large-9 column">
		<div class="large-6 column">
			<div class="row">
				<div class="large-12 column">Date of Visit:</div>
			</div>
			<div class="row">
				<div class="large-4 column">Visit type:</div>
				<div class="large-8 column">
					<input type="checkbox"> First consult at this facility<br>
						 Trans-in from:
					<br>
					<input type="checkbox">Follow-up
				</div>
			</div>
		</div>
		<div class="large-6 column">
			<div class="large-12 column">Physician's name:</div>
			<div class="large-12 column">Facility name:</div>
			<div class="large-12 column">Address:</div>
			<div class="large-12 column">Contact #:</div>
		</div>
	</div>
</div>
<!-- Patient Info -->
<div class="row box">
	<div class="large-1 columns text-center rotate">
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>PATIENT&nbsp;INFO</p>
	</div>
	<div class="large-11 columns boxleft">
		<div class="row">
			<div class="large-3 columns">SACCL Confirmatory Code:</div>
			<div class="large-3 columns">&nbsp;</div>
			<div class="large-2 columns">Patient Code:</div>
			<div class="large-4 columns">&nbsp;</div>
		</div>
		<div class="row">
			<div class="large-12 columns">
				<fieldset>
					<div class="row">
						<div class="large-12 columns">If first visit, please fill out this section</div>
					</div>
					<div class="row">
						<div class="large-4 columns">
							UIC:
							<u >0</u> <u>0</u> | <u>0</u> <u>0</u> | <u>0</u> <u>0</u> <u>0</u> <u>0</u> <u>0</u> <u>0</u> <u>0</u> <u>0</u> 
						</div>
						<div class="large-2 columns">Philhealth No.:</div>
						<div class="large-6 columns">&nbsp;</div>
					</div>
					<div class="row">
						<div class="large-12 columns">*UIC: First two letters of mother's name, first two letters of father's name, two-digit birth order, birthdate(MM-DD-YYYY)</div>
					</div>
					<div class="row">
						<div class="large-3 columns">Patient's Full Name:</div>
						<div class="large-4 columns">&nbsp;</div>
						<div class="large-1 columns">Sex:</div>
						<div class="large-2 columns"><input type="checkbox" />&nbsp;M <input type="checkbox" />&nbsp;F</div>
						<div class="large-1 columns">Age:</div>
						<div class="large-1 columns text-center">12</div>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
</div>
<!-- LABORATORY TEST -->
<div class="row box">
	<div class="large-1 columns text-center rotate">
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>LABORATORY&nbsp;TEST</p>
	</div>
	<div class="large-11 columns boxleft">
		<div class="row">
			<div class="large-4 columns">
				<div class="row text-center">
					<div class="large-12 column">Latest laboratory results</div>
				</div>
				<div class="row">
					<div class="large-2 column"></div>
					<div class="large-8 column">Hemoglobin</div>
				</div>
				<div class="row">
					<div class="large-2 column"></div>
					<div class="large-8 column">CD4 Test</div>
				</div>
				<div class="row">
					<div class="large-2 column"></div>
					<div class="large-8 column">Viral Load</div>
				</div>
				<div class="row">
					<div class="large-2 column"></div>
					<div class="large-8 column">Chest X-Ray</div>
				</div>
				<div class="row">
					<div class="large-2 column"></div>
					<div class="large-8 column">Gene Xpert</div>
				</div>
				<div class="row">
					<div class="large-2 column"></div>
					<div class="large-8 column">DSSM/DST</div>
				</div>
				<div class="row">
					<div class="large-2 column"></div>
					<div class="large-8 column">HIVDR & Genotype</div>
				</div>
			</div>
			<div class="large-4 columns text-center">
				<div class="row">
					<div class="large-12 column">Date Done</div>
				</div>
				<div class="row">
					<div class="large-12 column"><input type="text"></div>
				</div>
				<div class="row">
					<div class="large-12 column"><input type="text"></div>
				</div>
				<div class="row">
					<div class="large-12 column"><input type="text"></div>
				</div>
				<div class="row">
					<div class="large-12 column"><input type="text"></div>
				</div>
				<div class="row">
					<div class="large-12 column"><input type="text"></div>
				</div>
				<div class="row">
					<div class="large-12 column"><input type="text"></div>
				</div>
				<div class="row">
					<div class="large-12 column"><input type="text"></div>
				</div>
			</div>
			<div class="large-4 columns text-center">
				<div class="row">
					<div class="large-12 column">Reason</div>					
				</div>
				<div class="row">
					<div class="large-6 column"><input type="text"></div>
					<div class="large-6 column text-left">g/L</div>
				</div>
				<div class="row">
					<div class="large-6 column"><input type="text"></div>
					<div class="large-6 column text-left">cells/uL</div>
				</div>
				<div class="row">
					<div class="large-6 column"><input type="text"></div>
					<div class="large-6 column text-left">copies/mL</div>
				</div>
				<div class="row">
					<div class="large-12 column"><input type="text"></div>
				</div>
				<div class="row">
					<div class="large-12 column"><input type="text"></div>
				</div>
				<div class="row">
					<div class="large-12 column"><input type="text"></div>
				</div>
				<div class="row">
					<div class="large-12 column"></div>
				</div>
			</div>			
		</div>
	</div>
</div>

<div class="row box">
	<div class="large-1 columns text-center rotate">
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>TB&nbsp;INFORMATION</p>
	</div>
	<div class="large-11 columns boxleft">
		<div class="row">
			<div class="large-9 columns">Presence of at least one of the following: weight loss, cough, night sweats, fever?</div>
			<div class="large-3 columns"><input type="checkbox" />&nbsp;Yes <input type="checkbox" />&nbsp;No</div>
		</div>
		<div class="row">
			<div class="large-12 columns"> TB Status:</div>
		</div>
		<div class="row">
			<div class="large-2 columns"><input type="checkbox" /> No Active TB</div>
			<div class="large-2 columns"> On IPT?</div>
			<div class="large-2 columns"><input type="checkbox" />&nbsp;Yes <input type="checkbox" />&nbsp;No</div>
			<div class="large-2 columns"> IPT Outcome?</div>
			<div class="large-4 columns">
				<input type="checkbox" /> Completed 
				<input type="checkbox" /> Failed 
				<input type="checkbox" /> Other: 
			</div>
		</div>
		<div class="row">
			<div class="large-12 columns"><input type="checkbox" /> With Active TB</div>
		</div>
		<div class="row">
			<div class="large-1 columns">Site:</div>
			<div class="large-2 columns"><input type="checkbox" /> Pulmonary</div>
			<div class="large-3 columns">&nbsp;</div>
			<div class="large-2 columns">Drug Resistance:</div>
			<div class="large-2 columns"><input type="checkbox" /> Susceptible</div>
			<div class="large-2 columns"><input type="checkbox" /> XDR</div>
		</div>
		<div class="row">
			<div class="large-1 columns">&nbsp;</div>
			<div class="large-3 columns"><input type="checkbox" /> Extrapulmonary</div>
			<div class="large-2 columns">&nbsp;</div>
			<div class="large-2 columns">&nbsp;</div>
			<div class="large-2 columns"><input type="checkbox" /> MDR/RR</div>
			<div class="large-2 columns"><input type="checkbox" /> Other: </div>
		</div>
		<div class="row">
			<div class="large-6 column">
				<div class="row">
					<div class="large-4 column"> Current TB Regimen:</div>
					<div class="large-4 column"><input type="checkbox"> Category I</div>
					<div class="large-4 column"><input type="checkbox"> Category Ia</div>
				</div>
				<div class="row">
					<div class="large-4 column">&nbsp;</div>
					<div class="large-4 column"><input type="checkbox"> Category II</div>
					<div class="large-4 column"><input type="checkbox"> Category IIa</div>
				</div>
				<div class="row">
					<div class="large-4 column">&nbsp;</div>
					<div class="large-4 column"><input type="checkbox"> SRDR</div>
					<div class="large-4 column"><input type="checkbox"> XDR-TB regimen</div>
				</div>	
			</div>
			<div class="large-6 column">
				<div class="row">
					<div class="large-4 column">&nbsp;</div>
					<div class="large-4 column">&nbsp;</div>
					<div class="large-4 column">&nbsp;</div>
				</div>
				<div class="row">
					<div class="large-4 column">&nbsp;</div>
					<div class="large-4 column">&nbsp;</div>
					<div class="large-4 column">&nbsp;</div>
				</div>
				<div class="row">
					<div class="large-4 column">Tx outcome:</div>
					<div class="large-4 column"><input type="checkbox"> Cured</div>
					<div class="large-4 column"><input type="checkbox"> Failed</div>
				</div>
				<div class="row">
					<div class="large-4 column">&nbsp;</div>
					<div class="large-4 column"><input type="checkbox"> Completed</div>
					<div class="large-4 column"><input type="checkbox"> Other:</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row box">
	<div class="large-1 column text-center rotate">
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>INFECTIONS</p>
	</div>
	<div class="large-11 column boxleft">
		<div class="large-6 column"> 
			<div class="row">
				<div class="large-4 column">WHO Classification:</div>
				<div class="large-2 column"><input type="checkbox"> I</div>
				<div class="large-2 column"><input type="checkbox"> II</div>
				<div class="large-2 column"><input type="checkbox"> III</div>
				<div class="large-2 column"><input type="checkbox"> IV</div>
			</div>
			<div class="row">
				<div class="large-12 column">Infection Currently present (check all that apply):</div>
				<div class="large-12 column"><input type="checkbox"> Hepatitis B</div>
				<div class="large-12 column"><input type="checkbox"> Hepatitis C</div>
				<div class="large-12 column"><input type="checkbox"> Pneumocystis pneumonia</div>
				<div class="large-12 column"><input type="checkbox"> Oropharyngeal candidiasis</div>
			</div>
		</div>
		<div class="large-6 column"> 
			<div class="row">
				<div class="large-12 column">&nbsp;</div>
			</div>
			<div class="row">
				<div class="large-12 column">&nbsp;</div>
			</div>
			<div class="row">
				<div class="large-12 column">&nbsp;</div>
			</div>
			<div class="row">
				<div class="large-12 column"><input type="checkbox"> Syphilis</div>
				<div class="large-12 column"><input type="checkbox"> STIs (specify)</div>
				<div class="large-12 column"><input type="checkbox"> Other (specify)</div>
			</div>
		</div>
	</div>
</div>
<div class="row box">
	<div class="large-1 column text-center rotate">
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>OB</p>
	</div>
	<div class="large-11 column boxleft">
		<div class="row">
			<div class="large-6 column">
				<div class="row">
					<div class="large-6 column">Currently Pregnant:</div>
					<div class="large-3 column"><input type="checkbox"> No</div>
					<div class="large-3 column"><input type="checkbox"> Yes</div>
				</div>
				<div class="row">
					<div class="large-1 column">&nbsp;</div>
					<div class="large-11 column">If delivered, date of delivery:</div>
				</div>
				
			</div>
			<div class="large-6 column">
				<div class="row"><div class="large-12 column">If yes, age of gestation:</div></div>
				<div class="row"><div class="large-12 column">*For live births, please accomplish the PMTCT-Newborn form.</div></div>
			</div>
		</div>
		<div class="row">
			<div class="large-3 column">Type of infant feeding:</div>
			<div class="large-3 column"><input type="checkbox"> Breastfeeding</div>
			<div class="large-3 column"><input type="checkbox"> Formula Feeding</div>
			<div class="large-3 column"><input type="checkbox"> Mixed Feeding</div>
		</div>
	</div>
</div>
<div class="row box">
	<div class="large-1 column rotate">
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>ANTIRETROVIRAL&nbsp;REGIMEN</p>
	</div>
	<div class="large-11 column boxleft">
		<div class="large-6 column">
			<div class="row">
				<div class="large-5 column">
					<div class="row">
						<div class="large-12 column text-center">Drug</div>
					</div>
					<div class="row">
						<div class="large-12 column">&nbsp;</div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
				</div>
				<div class="large-3 column"> 
					<div class="row">
						<div class="large-12 column text-center">No. of pills per day</div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
				</div>
				<div class="large-4 column"> 
					<div class="row">
						<div class="large-12 column text-center">#pills missed in past 30 days</div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="large-4 column">
					<div class="row">
						<div class="large-12 column"><cite>Discontinuation codes:</cite></div>
					</div>
					<div class="row">
						<div class="large-12 column"><cite>1-Treatment Failure</cite></div>
					</div>
					<div class="row">
						<div class="large-12 column"><cite>2-Clinical progression/hospitalization </cite></div>
					</div>
					<div class="row">
						<div class="large-12 column"><cite>3-Patient Decision/Request</cite></div>
					</div>		
				</div>
				<div class="large-4 column">
					<div class="row">
						<div class="large-12 column">&nbsp;</div>
					</div>
					<div class="row">
						<div class="large-12 column"><cite>4-Compliance Difficulties</cite></div>
					</div>	
					<div class="row">
						<div class="large-12 column"><cite>5-Drug Interaction</cite></div>
					</div>
					<div class="row">
						<div class="large-12 column"><cite>6-Adverse Event(Specify)</cite></div>
					</div>
				</div>
				<div class="large-4 column">
					<div class="row">
						<div class="large-12 column">&nbsp;</div>
					</div>
					<div class="row">
						<div class="large-12 column"><cite>7-Other(Specify)</cite></div>
					</div>
					<div class="row">
						<div class="large-12 column"><cite>8-Death</cite></div>
					</div>
				</div>
			</div>
		</div>
		<div class="large-6 column">
			<div class="row">
				<div class="large-3 column">
					<div class="row">
						<div class="large-12 column text-center">No. of pills left</div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
				</div>
				<div class="large-4 column">
					<div class="row">
						<div class="large-12 column text-center">Date discontinued</div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
				</div>
				<div class="large-5 column">
					<div class="row">
						<div class="large-12 column text-center">Reason (indicate D/C code)</div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
					<div class="row">
						<div class="large-12 column"><input type="text"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="large-8 column">
					<div class="row">
						<div class="large-12 column">&nbsp;</div>
					</div>
					<div class="row">
						<div class="large-12 column">HACT Physician Approval:</div>
					</div>
				</div>
				<div class="large-4 column">
					<div class="row">
						<div class="large-12 column">&nbsp;</div>
					</div>
					<div class="row">
						<div class="large-12 column"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row box">
	<div class="large-1 column rotate">
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>PHARMACY</p>
	</div>
	<div class="large-11 column boxleft">
		<div class="large-2 column">
			<div class="row">
				<div class="large-12 column text-center">Date Dispense</div>
			</div>
			<div class="row">
				<div class="large-12 column">&nbsp;</div>
			</div>
			<div class="row">
				<div class="large-12 column"><input type="text"></div>
			</div>
			<div class="row">
				<div class="large-12 column"><input type="text"></div>
			</div>
			<div class="row">
				<div class="large-12 column"><input type="text"></div>
			</div>
			<div class="row">
				<div class="large-12 column"><input type="text"></div>
			</div>
		</div>
		<div class="large-3 column">
			<div class="row">
				<div class="large-12 column text-center">Drug</div>
			</div>
			<div class="row">
				<div class="large-12 column">&nbsp;</div>
			</div>
			<div class="row">
				<div class="large-12 column"><input type="text"></div>
			</div>
			<div class="row">
				<div class="large-12 column"><input type="text"></div>
			</div>
			<div class="row">
				<div class="large-12 column"><input type="text"></div>
			</div>
			<div class="row">
				<div class="large-12 column"><input type="text"></div>
			</div>
		</div>
		<div class="large-2 column">
			<div class="row">
				<div class="large-12 column text-center"># of pills on hand</div>
			</div>
			<div class="row">
				<div class="large-12 column"><input type="text"></div>
			</div>
			<div class="row">
				<div class="large-12 column"><input type="text"></div>
			</div>
			<div class="row">
				<div class="large-12 column"><input type="text"></div>
			</div>
		</div>
		<div class="large-2 column">
			<div class="row">
				<div class="large-12 column text-center"># of pills dispensed</div>
			</div>
			<div class="row">
				<div class="large-12 column"><input type="text"></div>
			</div>
			<div class="row">
				<div class="large-12 column"><input type="text"></div>
			</div>
			<div class="row">
				<div class="large-12 column"><input type="text"></div>
			</div>
			<div class="row">
				<div class="large-12 column"><input type="text"></div>
			</div>
		</div>
		<div class="large-3 column">
			<div class="row">
				<div class="large-12 column text-center">Dispense by:</div>
			</div>
			<div class="row">
				<div class="large-12 column">&nbsp;</div>
			</div>
			<div class="row">
				<div class="large-12 column"><input type="text"></div>
			</div>
			<div class="row">
				<div class="large-12 column"><input type="text"></div>
			</div>
			<div class="row">
				<div class="large-12 column"><input type="text"></div>
			</div>
			<div class="row">
				<div class="large-12 column"><input type="text"></div>
			</div>
		</div>
	</div>
</div>
@endsection