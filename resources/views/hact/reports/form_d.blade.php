@extends('hact.layouts.layout_admin')

@section('content')
<div class="panel">
	<div class="row box">
		<div class="large-11 column text-center">MORTALITY REPORT</div>
		<div class="large-1 column text-center">D</div>
	</div>
	<div class="row box">
		<div class="large-12 column text-center">
			The law on Reporting Disease (R.A. 3573) & the Philippine AIDS Prevention and Control Act (R.A. 8504) requires phycisians to report all diagnosed HIVinfections to the HIV & AIDS Registrar at the Epidermiology Bureau, DOH Please write in CAPITAL LETTERS and CHECKS the appropriate boxes.
		</div>
	</div>
	<div class="row box">
		<div class="large-1 column text-center inline">1</div>
		<div class="large-11 column boxleft">
			<div class="row">
				<div class="large-6 column">
					<div class="large-4 column">
						<label class="left inline" for="date_report">Date of Report:</label>
					</div>
					<div class="large-8 column">
						<div class="row">
							<div class="large-12 column top">
								<input type="text" class="fdatepicker" id="date_report" placeholder="" readonly="readonly">
							</div>
						</div>
					</div>
				</div>
				<div class="large-6 column">&nbsp;</div>
			</div>
		</div>
	</div>
	<div class="row box">
		<div class="large-12 column text-center">HIV CONFIRMATORY TEST</div>
	</div>
	<div class="row box">
		<div class="large-1 column text-center">2</div>
		<div class="large-11 column boxleft">
			<div class="row">
				<div class="large-9 column">
					<div class="large-5 column">
						<label class="left inline" for="saccl_hiv">SACCL HIV Confirmatory Code:</label>
					</div>
					<div class="large-5 column top">
						<input type="text" id="saccl_hiv" >
					</div>
					<div class="large-2 column">
						&nbsp;
					</div>
				</div>
				<div class="large-3 column">
				</div>
			</div>
		</div>
	</div>
	<div class="row box">
		<div class="large-12 column text-center">UNIQUE IDENTIFIER CODE</div>
	</div>
	<div class="row box">
		<div class="large-1 column text-center">3</div>
		<div class="large-11 column boxleft">
			<div class="large-2 column">
				<div class="row">
					<div class="large-12 column text-center">
						<cite class="small">First 2 letters of mother's real name</cite>
					</div>
				</div>
				<div class="row">
					<div class="large-12 column text-center">
						<input type="text" class="text-center">
					</div>
				</div>
			</div>
			<div class="large-2 column boxleft">
				<div class="row">
					<div class="large-12 column text-center">
						<cite class="small">First 2 letters of father's real name</cite>
					</div>
				</div>
				<div class="row">
					<div class="large-12 column text-center">
						<input type="text" class="text-center">
					</div>
				</div>
			</div>
			<div class="large-2 column boxleft">
				<div class="row">
					<div class="large-12 column text-center">
						<cite class="small">Birth order</cite>
					</div>
				</div>
				<div class="row">
					<div class="large-12 column text-center">
						&nbsp;
					</div>
				</div>
				<div class="row">
					<div class="large-12 column text-center">
						<input type="text" class="text-center">
					</div>
				</div>
			</div>
			<div class="large-2 column boxleft">
				<div class="row">
					<div class="large-12 column text-center">
						<cite class="small">Month of Birth</cite>
					</div>
				</div>
				<div class="row">
					<div class="large-12 column text-center">
						&nbsp;
					</div>
				</div>
				<div class="row">
					<div class="large-12 column text-center">
						<input type="text" class="text-center">
					</div>
				</div>
			</div>
			<div class="large-2 column boxleft">
				<div class="row">
					<div class="large-12 column text-center">
						<cite class="small">Day of Birth</cite>
					</div>
				</div>
				<div class="row">
					<div class="large-12 column text-center">
						&nbsp;
					</div>
				</div>
				<div class="row">
					<div class="large-12 column text-center">
						<input type="text" class="text-center">
					</div>
				</div>
			</div>
			<div class="large-2 column boxleft">
				<div class="row">
					<div class="large-12 column text-center">
						<cite class="small">Year of Birth</cite>
					</div>
				</div>
				<div class="row">
					<div class="large-12 column text-center">
						&nbsp;
					</div>
				</div>
				<div class="row">
					<div class="large-12 column text-center">
						<input type="text" class="text-center">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row box">
		<div class="large-12 column text-center">DEMOGRAPHIC DATA</div>
	</div>
	<div class="row box">
		<div class="large-1 column text-center">4</div>
		<div class="large-11 column boxleft">
			<div class="large-3 column">
				<label class="left inline">PHIL HEALTH NUMBER:</label>
			</div>
			<div class="large-3 column top">
				<input type="text" >
			</div>
			<div class="large-6 column">
				<div class="row top">
					<label><input type="checkbox"> Not enrolled in PhilHealth</label>
				</div>
			</div>
		</div>
	</div>
	<div class="row box">
		<div class="large-1 column text-center">5</div>
		<div class="large-11 column boxleft">
			<div class="large-4 column">
				<div class="row">
					<div class="large-12 column">
						<label>Name(Full Name)</label>
					</div>
				</div>
				<div class="row">
					<div class="large-12 column">
						<input type="text">
					</div>
				</div>
				<div class="row text-center">
					<div class="large-12 column">
						<cite class="small">First Name</cite>
					</div>
				</div>
			</div>
			<div class="large-4 column">
				<div class="row">
					<div class="large-12 column">
						<label>&nbsp;</label>
					</div>
				</div>
				<div class="row">
					<div class="large-12 column">
						<input type="text">
					</div>
				</div>
				<div class="row text-center">
					<div class="large-12 column">
						<cite class="small">Middle</cite>
					</div>
				</div>
			</div>
			<div class="large-4 column">
				<div class="row">
					<label>&nbsp;</label>
				</div>
				<div class="row">
					<input type="text">
				</div>
				<div class="row text-center">
					<cite class="small">Last Name</cite>
				</div>
			</div>
		</div>
	</div>
	<div class="row box">
		<div class="large-1 column text-center">6</div>
		<div class="large-11 column boxleft">
			<div class="large-4 column">
				<div class="row">
					<div class="large-12 column">
						<label>Mother's Maiden Name(Full Name)</label>
					</div>
				</div>
				<div class="row">
					<div class="large-12 column">
						<input type="text">
					</div>
				</div>
				<div class="row text-center">
					<div class="large-12 column">
						<cite class="small">First Name</cite>
					</div>
				</div>
			</div>
			<div class="large-4 column">
				<div class="row">
					<div class="large-12 column">
						<label>&nbsp;</label>
					</div>
				</div>
				<div class="row">
					<div class="large-12 column">
						<input type="text">
					</div>
				</div>
				<div class="row text-center">
					<div class="large-12 column">
						<cite class="small">Middle</cite>
					</div>
				</div>
			</div>
			<div class="large-4 column">
				<div class="row">
					<label>&nbsp;</label>
				</div>
				<div class="row">
					<input type="text">
				</div>
				<div class="row text-center">
					<cite class="small">Last Name</cite>
				</div>
			</div>
		</div>
	</div>
	<div class="row box">
		<div class="large-1 column text-center">7</div>
		<div class="large-11 column boxleft">
			<div class="large-4 column">
				<div class="large-6 column">
					<label for="date_of_birth" class="left inline">Date of Birth:</label>
				</div>
				<div class="large-6 column top">
					<input type="text" class="fdatepicker" id="date_of_birth" placeholder="" readonly="readonly"/>
				</div>
			</div>
			<div class="large-3 column">
				<div class="large-3 column">
					<label for="age" class="right inline">Age:</label>
				</div>
				<div class="large-5 column top">
					<input type="text">
				</div>
				<div class="large-4 column top">
					<cite >(years)</cite>
				</div>
			</div>
			<div class="large-5 column">
				<div class="large-5 column">
					<label for="age">Age in months <br><cite>(if less than 1 yr old):</cite></label>
				</div>
				<div class="large-3 column top">
					<input type="text">
				</div>
				<div class="large-4 column top">
					<cite >(years)</cite>
				</div>
			</div>
		</div>
	</div>
	<div class="row box">
		<div class="large-1 column text-center">8</div>
		<div class="large-11 column boxleft">
			<div class="large-2 column">
				<label>Sex:</label>
			</div>
			<div class="large-2 column">
				<label><input type="checkbox" id="male"> Male</label>
			</div>
			<div class="large-2 column">
				<label><input type="checkbox" id="female"> Female</label>
			</div>
			<div class="large-6 column">&nbsp;
			</div>
		</div>
	</div>
	<div class="row box">
		<div class="large-1 column text-center">9</div>
		<div class="large-11 column boxleft">
			<div class="row">
				<div class="large-3 column">
					<label for="permanent_address" class="left inline">Permanent Address:</label>
				</div>
				<div class="large-9 column top">
					<input type="text" id="permanent_address">
				</div>
			</div>
			<div class="row">
				<div class="large-3 column">
					<label for="current_place" class="left inline">Current Place of Residence:</label>
				</div>
				<div class="large-5 column top">
					<div class="large-5 column">
						<label for="municipality_city" class="right inline">Municipality/City</label>
					</div>
					<div class="large-7 column">
						<input type="text" id="municipality_city">
					</div>
				</div>
				<div class="large-4 column top">
					<div class="large-5 column">
						<label for="municipality_city" class="right inline">Province:</label>
					</div>
					<div class="large-7 column">
						<input type="text" id="municipality_city">
					</div>
				</div>
			</div>
			<div class="row">	
				<div class="large-3 column">
					<label for="current_place" class="left inline">Date of Birth:</label>
				</div>
				<div class="large-5 column top">
					<div class="large-5 column">
						<label for="municipality_city" class="right inline">Municipality/City</label>
					</div>
					<div class="large-7 column">
						<input type="text" id="municipality_city">
					</div>
				</div>
				<div class="large-4 column top">
					<div class="large-5 column">
						<label for="municipality_city" class="right inline">Province:</label>
					</div>
					<div class="large-7 column">
						<input type="text" id="municipality_city">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row box">
		<div class="large-1 column text-center">10</div>
		<div class="large-11 column boxleft">
			<div class="large-10 column">
				<div class="large-4 column">
					<label>Civil Status:</label>
				</div>
				<div class="large-2 column">
					<label for="single"><input type="checkbox" id="single"> Single</label>
				</div>
				<div class="large-2 column">
					<label for="married"><input type="checkbox" id="married"> Married</label>
				</div>
				<div class="large-2 column">
					<label for="separated"><input type="checkbox" id="separated"> Separated</label>
				</div>
				<div class="large-2 column">
					<label for="widowed"><input type="checkbox" id="widowed"> Widowed</label>
				</div>
			</div>
			<div class="large-2 column"></div>
		</div>
	</div>
	<div class="row box">
		<div class="large-1 column text-center">11</div>
		<div class="large-11 column boxleft">
			<div class="large-4 column">
				<label>Currently Living with a Partner?</label>
			</div>
			<div class="large-3 column">
				<div class="large-6 column">
					<label for="yes"><input type="checkbox" id="yes">Yes</label>
				</div>
				<div class="large-6 column">
					<label for="no"><input type="checkbox" id="no">No</label>
				</div>
			</div>
			<div class="large-5 column">
			</div>
		</div>
	</div>
	<div class="row box">
		<div class="large-12 column text-center">CIRCUMTANCES SURROUNDING DEATH</div>
	</div>
	<div class="row box">
		<div class="large-1 column text-center">12</div>
		<div class="large-11 column boxleft">
			<div class="large-2 column">
				<label class="left inline">Date of Death:</label>
			</div>
			<div class="large-4 column top">
				<input type="text" class="fdatepicker" readonly>
			</div>
			<div class="large-4 column top">
				&nbsp;
			</div>
		</div>
	</div>
	<div class="row box ">
		<div class="large-1 column text-center">13</div>
		<div class="large-11 column boxleft">
			<div class="row">
				<label>Underlying cause of death:</label>
			</div>
			<div class="row">
				<div class="row">
					<div class="large-1 column">&nbsp;</div>
					<div class="large-11 column"><label><input type="checkbox">HIV-related</label></div>
				</div>
				<div class="row">
					<div class="large-1 column">&nbsp;</div>
					<div class="large-3 column"><label for="antecendent_cause" class="right inline">Antecendent cause:</label></div>
					<div class="large-3 column"><input type="text" id="antecendent_cause_hiv"></div>
					<div class="large-2 column"><label for="icd_10_code_hiv" class="right inline">ICD-10 Code:</label></div>
					<div class="large-3 column"><input type="text" id="icd_10_code_hiv"></div>
				</div>
			</div>
			<div class="row">
				<div class="row">
					<div class="large-1 column">&nbsp;</div>
					<div class="large-11 column"><label><input type="checkbox">Not HIV-related</label></div>
				</div>
				<div class="row">
					<div class="large-1 column">&nbsp;</div>
					<div class="large-3 column"><label for="specify_cause" class="right inline">Specify Cause:</label></div>
					<div class="large-3 column"><input type="text" id="specify_cause"></div>
					<div class="large-2 column"><label for="icd_10_code_not_hiv" class="right inline">ICD-10 Code:</label></div>
					<div class="large-3 column"><input type="text" id="icd_10_code_not_hiv"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="row box ">
		<div class="large-1 column text-center">14</div>
		<div class="large-11 column boxleft">
			<div class="row">
				<div class="large-12 column">
					<label>Opportunistic infections present prior to death (check all that apply)</label>
				</div>
				<div class="row">
					<div class="large-3 column">
						<div class="row">
							<div class="large-2 column"></div>
							<div class="large-10 column">
								<label for="tuberculosis">
									<input type="checkbox" id="tuberculosis">Tuberculosis
								</label>
							</div>
						</div>
						<div class="row">
							<div class="large-2 column"></div>
							<div class="large-10 column">
								<label for="pneumocystis_pneummonia">
									<input type="checkbox" id="pneumocystis_pneummonia">Pneumocystis Pneummonia
								</label>
							</div>
						</div>
					</div>
					<div class="large-3 column">
						<div class="row">
							<div class="large-2 column"></div>
							<div class="large-10 column">
								<label for="cryptococcal_meningitis">
									<input type="checkbox" id="cryptococcal_meningitis">Cryptococcal Meningitis
								</label>
							</div>
						</div>
						<div class="row">
							<div class="large-2 column"></div>
							<div class="large-10 column">
								<label for="cytomegalovirus">
									<input type="checkbox" id="cytomegalovirus">Cytomegalovirus
								</label>
							</div>
						</div>
					</div>
					<div class="large-3 column">	
						<div class="row">
							<div class="large-2 column"></div>
							<div class="large-10 column">
								<label for="candidiasis">
									<input type="checkbox" id="candidiasis">Candidiasis
								</label>
							</div>
						</div>
						<div class="row">
							<div class="large-2 column"></div>
							<div class="large-10 column">
								<label for="toxoplasmosis">
									<input type="checkbox" id="toxoplasmosis">Toxoplasmosis
								</label>
							</div>
						</div>
					</div>
					<div class="large-3 column">
						<div class="row">
							<div class="large-2 column"></div>
							<div class="large-10 column">
								<label for="none">
									<input type="checkbox" id="none">None
								</label>
							</div>
						</div>
						<div class="row">
							<div class="large-2 column"></div>
							<div class="large-10 column">
								<label for="other">
									<input type="checkbox" id="other">Other:
									<input type="text" name="other_specify">
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row box">
		<div class="large-12 column text-center">CD4 COUNT</div>
	</div>
	<div class="row box ">
		<div class="large-1 column text-center">15</div>
		<div class="large-11 column boxleft">
			<div class="large-6 column">
				<div class="large-4 column">
					<small>Date of Last CD4 Count:</small>
				</div>
				<div class="large-4 column">
					<label for="never_done"><input type="checkbox" id="never_done">Never done</label>
				</div>
				<div class="large-4 column top">
					<input type="text" class="fdatepicker">
				</div>
			</div>
			<div class="large-6 column">
				<div class="large-7 column">
					<div class="large-6 column">
						<label for="cd4_count">CD4 Count:</label>
					</div>
					<div class="large-6 column top">
						<input type="text" id="cd4_count">
					</div>
				</div>
				<div class="large-5 column">
					<div class="large-12 column">
						<label for="cd4_information"><input type="checkbox" id="cd4_information">CD4 Information not available</label>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row box">
		<div class="large-12 column text-center">ARV HISTORY</div>
	</div>
	<div class="row box ">
		<div class="large-1 column text-center">16</div>
		<div class="large-11 column boxleft">
			<div class="large-5 column">
				<small>Did patient ever take Anti-Retrovirals (ARV) medication?</small>
			</div>
			<div class="large-2 column">
				<label for="arv_no"><input type="checkbox" id="arv_no"> No</label>
			</div> 
			<div class="large-2 column">
				<label for="arv_yes"><input type="checkbox" id="arv_yes"> Yes</label>
			</div>
			<div class="large-3 column">
				<label for="arv_history"><input type="checkbox" id="arv_history"> ARV history not available </label>
			</div>
		</div>
	</div>
	<div class="row box ">
		<div class="large-1 column text-center">17</div>
		<div class="large-11 column boxleft">
			<div class="large-3 column">
				<small><strong>Last ARV regimen:</strong></small>
			</div>
			<div class="large-3 column">
				<label for="first_line_regimen"><input type="checkbox" id="first_line_regimen">First line regimen</label>
			</div>
			<div class="large-3 column">
				<label for="second_line_regimen"><input type="checkbox" id="second_line_regimen">Second line regimen</label>
			</div>
			<div class="large-3 column">
				<label for="regimen_information"><input type="checkbox" id="regimen_information">Regimen information not available</label>
			</div>
		</div>
	</div>
	<div class="row box ">
		<div class="large-1 column text-center">18</div>
		<div class="large-11 column boxleft">
			<div class="row">
				<div class="large-3 column">
					<label for="name_and_signature" class="left inline">Name and Signature of Physician:</label>
				</div>
				<div class="large-9 column top">
					<input type="text" id="name_and_signature" >
				</div>
			</div>
			<div class="row">
				<div class="large-6 column">
					<div class="large-3 column">
						<label for="contact_number">Contact Number:</label>
					</div>
					<div class="large-9 column">
						<input type="text" id="contact_number">
					</div>
				</div>
				<div class="large-6 column">
					<div class="large-3 column">
						<label for="email_address">Email Adress:</label>
					</div>
					<div class="large-9 column">
						<input type="text" id="email_address">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="large-3 column">
					<label for="name_of_facility" class="left inline">Name of Facility:</label>
				</div>
				<div class="large-9 column">
					<input type="text" id="name_of_facility">
				</div>
			</div>
			<div class="row">
				<div class="large-3 column">
					<label for="mailing_address_facility" class="left inline">Complete Mailing Address of Facility:</label>
				</div>
				<div class="large-9 column">
					<input type="text" id="mailing_address_facility">
				</div>
			</div>
		</div>
	</div>
</div>

@endsection