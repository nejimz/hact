@extends('hact.layouts.layout_admin')

@section('content')

	<div class='row'>
		<div class='large-7 columns'>
			<ul class="breadcrumbs">
				<li><a href="{{ route('dashboard') }}">Home</a></li>
				<li class="current"><a href="#">Users</a></li>
			</ul>
		</div>
		<div class='large-5 columns'>
			<form>
				<div class="row">
					<div class="large-12 columns">
						<div class="row collapse">
							<div class="small-9 columns">
								<input name="search" type="text" placeholder="Search" value="{{ $search }}">
							</div>
					    	<div class="small-3 columns">
					      		<button type="submit" class="button alert postfix"><i class="fa fa-search"></i></buatton>
					    	</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class='row'>
		<div class='large-12 columns'>
			{!! str_replace('/?', '?', $users->appends($pagination)->render()) !!}
			<table width="100%">
				<thead>
					<tr>
						<th width="5%"></th>
						<th width="25%"><a href="{{ $first_name_sort }}">First Name</a></th>
						<th width="25%"><a href="{{ $last_name_sort }}">Last Name</a></th>
						<th width="25%"><a href="{{ $email_sort }}">Email</a></th>
						<th width="20%"><a href="{{ $role_sort }}">Role</a></th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
					<tr>
						<td><a href="{{ route('user_edit', [$user->id]) }}"><i class="fa fa-edit fa-lg"></i></a></td>
						<td>{{ $user->first_name }}</td>
						<td>{{ $user->last_name }}</td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->UserRole->role_name }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{!! str_replace('/?', '?', $users->appends($pagination)->render()) !!}
		</div>
	</div>

@endsection