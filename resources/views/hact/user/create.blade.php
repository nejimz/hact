@extends('hact.layouts.layout_admin')

@section('content')
	<div class="row">
		<div class="large-6 column">
			@include('hact.messages.success')
			@include('hact.messages.error_list')
			<form action="{{ $route }}" method="post">
				<div class="row">
			    	<div class="large-12 columns">
						<label>Email
			        		<input name="email" value="{{ $email }}" type="text" placeholder="large-12.columns" />
						</label>
			    	</div>
				</div>

				<div class="row">
			    	<div class="large-12 columns">
						<label>First Name
			        		<input name="first_name" value="{{ $first_name }}" type="text" placeholder="large-12.columns" />
						</label>
			    	</div>
				</div>

				<div class="row">
			    	<div class="large-12 columns">
						<label>Last Name
			        		<input name="last_name" value="{{ $last_name }}" type="text" placeholder="large-12.columns" />
						</label>
			    	</div>
				</div>
				
				<div class="row">
			    	<div class="large-12 columns">
						<label>Role
			        		<select name="role">
			        			<option value="">-- SELECT --</option>
			        			@foreach($roles as $role)
			        				@if($role_id == $role->id)
			        					<option value="{{ $role->id }}" selected="selected">{{ $role->role_name }}</option>
			        				@else
			        					<option value="{{ $role->id }}">{{ $role->role_name }}</option>
			        				@endif
			        			@endforeach
			        		</select>
						</label>
			    	</div>
				</div>

				@if(!empty($id))
				<div class="row">
					<input type="hidden" id="user_reset_password_url" name="user_reset_password_url" value="{{ route('user_reset_password', [$id]) }}" />
					<div class="large-12 columns">
						<br />Password
						<div class="row collapse">
							<div class="small-10 columns">
								<input type="text" name="generated_password" id="generated_password" readonly="readonly" />
							</div>
							<div class="small-2 columns">
								<a id="user_reset_password" href="#" class="button postfix">Reset</a>
							</div>
						</div>
					</div>
				</div>
				@endif

				<div class="row">
			    	<div class="large-12 columns">
			    		<br />
			    		{!! csrf_field() !!}
			    		<button class="button small alert"><strong>Save</strong></button>
			    		<a class="button small info" href="{{ route('user_index') }}"><strong>Back</strong></a>
			    	</div>
				</div>
			</form>
		</div>
	</div>
@endsection