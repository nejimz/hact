@extends('hact.layouts.layout_admin')

@section('content')

	<div class='row'>
		<div class='large-12 columns'>
			<ul class="breadcrumbs">
				<li class="current"><a href="#">Home</a></li>
			</ul>
		</div>
	</div>

	<div class='row'>
		<div class='large-12 columns'>
			<fieldset>
				<legend>Enrolment</legend>

				<div class='row'>
					<div class='large-12 columns'>
						<fieldset>
							<legend>Demographics</legend>

							<table width="100%">
								<thead>
									<tr>
										<th width="25%">UI Code</th>
										<th width="25%">Code Name</th>
										<th width="25%">SACCL</th>
										<th width="25%">Gender</th>
									</tr>
								</thead>
								<tbody>
									@foreach($demographics as $row)
									<tr>
										<td>{{ $row->ui_code }}</td>
										<td>{{ $row->code_name }}</td>
										<td>{{ $row->saccl_code }}</td>
										<td>{{ $row->gender_format }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</fieldset>
					</div>
				</div>

				<div class='row'>
					<div class='large-12 columns'>
						<fieldset>
							<legend>Clinical and Immunologic</legend>

							<table width="100%">
								<thead>
									<tr>
										<th width="20%">Code Name</th>
										<th width="20%">SACCL</th>
										<th width="20%">Laboratory</th>
										<th width="20%">Result</th>
										<th width="20%">Date</th>
									</tr>
								</thead>
								<tbody>
									@foreach($clinical_and_immunologic as $row)
									<tr>
										<td>{{ $row->EnrolmentDemographics->code_name }}</td>
										<td>{{ $row->EnrolmentDemographics->saccl_code }}</td>
										<td>{{ ($row->laboratory == 'other')? $row->laboratory_others : $row->laboratory }}</td>
										<td>{{ $row->result }}</td>
										<td>{{ $row->date_result }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</fieldset>
					</div>
				</div>

				<div class='row'>
					<div class='large-12 columns'>
						<fieldset>
							<legend>Opportunistic Infection</legend>

							<table width="100%">
								<thead>
									<tr>
										<th width="20%">Code Name</th>
										<th width="20%">SACCL</th>
										<th width="20%">Started w/ IPT</th>
										<th width="40%">Reason</th>
									</tr>
								</thead>
								<tbody>
									@foreach($opportunistic_infection as $row)
									<tr>
										<td>{{ $row->EnrolmentDemographics->code_name }}</td>
										<td>{{ $row->EnrolmentDemographics->saccl_code }}</td>
										<td>{{ $row->started_with_ipt }}</td>
										<td>{{ $row->no_ipt_reason }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</fieldset>
					</div>
				</div>

				<div class='row'>
					<div class='large-12 columns'>
						<fieldset>
							<legend>Medication</legend>

							<table width="100%">
								<thead>
									<tr>
										<th width="20%">Code Name</th>
										<th width="20%">SACCL</th>
										<th width="20%">Medicine</th>
										<th width="20%">Date Started</th>
										<th width="20%">Date Discontinued</th>
									</tr>
								</thead>
								<tbody>
									@foreach($medication as $row)
									<tr>
										<td>{{ $row->EnrolmentDemographics->code_name }}</td>
										<td>{{ $row->EnrolmentDemographics->saccl_code }}</td>
										<td>{{ $row->Item->item_name }}</td>
										<td>{{ $row->date_started }}</td>
										<td>{{ $row->date_discontinued }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</fieldset>
					</div>
				</div>
			</fieldset>
		</div>
	</div>


	<div class='row'>
		<div class='large-12 columns'>
			<fieldset>
				<legend>Event</legend>

				<div class='row'>
					<div class='large-12 columns'>
						<fieldset>
							<legend>ARV</legend>
							<table width="100%">
								<thead>
									<tr>
										<th width="20%">Code Name</th>
										<th width="20%">SACCL</th>
										<th width="20%">ARV Date</th>
										<th width="40%">Reccomendation</th>
									</tr>
								</thead>
								<tbody>
									@foreach($arv as $row)
									<tr>
										<td>{{ $row->EnrolmentDemographics->code_name }}</td>
										<td>{{ $row->EnrolmentDemographics->saccl_code }}</td>
										<td>{{ $row->arv_date }}</td>
										<td>{{ $row->recommendations }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</fieldset>
					</div>
				</div>

				<div class='row'>
					<div class='large-12 columns'>
						<fieldset>
							<legend>ARV Refill</legend>
							<table width="100%">
								<thead>
									<tr>
										<th width="20%">Code Name</th>
										<th width="20%">SACCL</th>
										<th width="60%">Reccomendation</th>
									</tr>
								</thead>
								<tbody>
									@foreach($arv_refill as $row)
									<tr>
										<td>{{ $row->EnrolmentDemographics->code_name }}</td>
										<td>{{ $row->EnrolmentDemographics->saccl_code }}</td>
										<td>{{ $row->recommendation }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</fieldset>
					</div>
				</div>

				<div class='row'>
					<div class='large-12 columns'>
						<fieldset>
							<legend>Check Up</legend>
							<table width="100%">
								<thead>
									<tr>
										<th width="20%">Code Name</th>
										<th width="20%">SACCL</th>
										<th width="60%">Reccomendation</th>
									</tr>
								</thead>
								<tbody>
									@foreach($checkup as $row)
									<tr>
										<td>{{ $row->EnrolmentDemographics->code_name }}</td>
										<td>{{ $row->EnrolmentDemographics->saccl_code }}</td>
										<td>{{ $row->recommendation }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</fieldset>
					</div>
				</div>

				<div class='row'>
					<div class='large-12 columns'>
						<fieldset>
							<legend>Death</legend>
							<table width="100%">
								<thead>
									<tr>
										<th width="20%">Code Name</th>
										<th width="20%">SACCL</th>
										<th width="20%">Date</th>
										<th width="40%">Cause of Death</th>
									</tr>
								</thead>
								<tbody>
									@foreach($death as $row)
									<tr>
										<td>{{ $row->EnrolmentDemographics->code_name }}</td>
										<td>{{ $row->EnrolmentDemographics->saccl_code }}</td>
										<td>{{ $row->death_date }}</td>
										<td>{{ $row->death_cause }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</fieldset>
					</div>
				</div>
			</fieldset>
		</div>
	</div>


@endsection