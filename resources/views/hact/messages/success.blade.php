@if(Session::has('status'))
    <div class="alert-box success">
        {{ Session::get('status') }}
    </div>
@endif