<head>
    <title>HACT (HIV and AIDS Core Team)</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/foundation.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/foundation.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/foundation-datepicker.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/normalize.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">

    <script type="text/javascript" src="{{ asset('js/jquery/jquery-1.11.3.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/foundation.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/foundation/foundation-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-ui.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/date.js') }}"></script>
</head>