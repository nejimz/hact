<!DOCTYPE html>
<html>
    @include('hact.layouts.admin_header')
    <body>
    	@include('hact.layouts.admin_menu')
	    
	    <div class="row">
	    	<div class="large-12 columns" role="content">
	    		@yield('content')
	    	</div>
	    	<!--aside  class="large-3 columns">
	    		Right
	    	</aside -->
	    </div>

    	@include('hact.layouts.admin_footer')
    </body>
</html>