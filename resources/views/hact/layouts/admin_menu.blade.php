
<nav class="top-bar" data-topbar role="navigation" style="margin-bottom: 30px;">
	<ul class="title-area">
		<li class="name">
			<h1><a href="{{ route('dashboard') }}" title="HIV and Aids Core Team">HACT</a></h1>
		</li>
		<li class="toggle-topbar menu-icon">
			<a href="#"><span>Menu</span></a>
		</li>
	</u>
	<section class="top-bar-section">
		<ul class="right">
			<li class="has-dropdown">
				<a href="{{ route('user_index') }}"><i class="fa fa-user-plus fa-lg"></i> Users</a>
				<ul class="dropdown">
					<li>
						<a href="{{ route('user_create') }}"><i class="fa fa-plus fa-lg"></i> Add</a>
					</li>
					<li>
						<a href="{{ route('user_create') }}"><i class="fa fa-book fa-lg"></i> Activity Log</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="{{ route('logout') }}"><i class="fa fa-sign-out fa-lg"></i> Logout</a>
			</li>
		</ul>
		<ul class="left">
			<li>
				<a href="{{ route('dashboard') }}"><i class="fa fa-home fa-lg"></i>  Home</a>
			</li>
			<li class="has-dropdown">
				<a href="{{ route('vct_index') }}"><i class="fa fa-calendar-check-o fa-lg"></i> VCT</a>
				<ul class="dropdown">
					<li class="has-dropdown">
						<li>
							<a href="{{ route('vct_create') }}"><i class="fa fa-plus fa-lg"></i>  New Record</a>
						</li>
					</li>
				</ul>
			</li>
			<li class="has-dropdown">
				<a href="#"><i class="fa fa-list-alt fa-lg"></i>  Enrolment</a>
				<ul class="dropdown">
					<li class="has-dropdown">
						<a href="{{ route('demographics_index') }}"><i class="fa fa-users fa-lg"></i> Demographics</a>
						<ul class="dropdown">
							<li>
								<a href="{{ route('demographics_create') }}"><i class="fa fa-plus fa-lg"></i> New Record</a>
							</li>
						</ul>
					</li>
					<li class="has-dropdown">
						<a href="{{ route('caip_index') }}"><i class="fa fa-heartbeat fa-lg"></i> Clinical & Immunologic Profile</a>
						<ul class="dropdown">
							<li>
								<a href="{{ route('caip_create') }}"><i class="fa fa-plus fa-lg"></i> New Record</a>
							</li>
						</ul>
					</li>
					<li class="has-dropdown">
						<a href="{{ route('oi_profile_index') }}"><i class="fa fa-eyedropper fa-lg"></i> Opportunistic Infections Profile</a>
						<ul class="dropdown">
							<li>
								<a href="{{ route('oi_profile_create') }}"><i class="fa fa-plus fa-lg"></i> New Record</a>
							</li>
						</ul>
					</li>
					<li class="has-dropdown">
						<a href="{{ route('medication_index') }}"><i class="fa fa-minus-circle fa-lg"></i> Medication Profile</a>
						<ul class="dropdown">
							<li>
								<a href="{{ route('medication_create') }}"><i class="fa fa-plus fa-lg"></i> New Record</a>
							</li>
						</ul>
					</li>
					
					</ul>
				</li>
			<li class="has-dropdown">
				<a href="#"><i class="fa fa-calendar fa-lg"></i> Event</a>
				<ul class="dropdown">
					<li class="has-dropdown">
						<a href="{{ route('arv_index') }}"><i class="fa fa-calendar fa-lg"></i> ARV Enrolment</a>
						<ul class="dropdown">
							<li>
								<a href="{{ route('arv_create') }}"><i class="fa fa-plus fa-lg"></i> New Event</a>
							</li>
						</ul>
					</li>
					<li class="has-dropdown">
						<a href="{{ route('arv_refill_index') }}"><i class="fa fa-refresh fa-lg"></i> ARV Refill</a>
						<ul class="dropdown">
							<li>
								<a href="{{ route('arv_refill_create') }}"><i class="fa fa-plus fa-lg"></i> New Event</a>
							</li>
						</ul>
					</li>
					<li class="has-dropdown">
						<a href="{{ route('checkup_index') }}"><i class="fa fa-stethoscope fa-lg"></i> Check Up</a>
						<ul class="dropdown">
							<li>
								<a href="{{ route('checkup_create') }}"><i class="fa fa-plus fa-lg"></i> New Event</a>
							</li>
						</ul>
					</li>
					<li class="has-dropdown">
						<a href="{{ route('death_index') }}"><i class="fa fa-bed fa-lg"></i> Death</a>
						<ul class="dropdown">
							<li>
								<a href="{{ route('death_create') }}"><i class="fa fa-plus fa-lg"></i> New Event</a>
							</li>
						</ul>
					</li>
				</ul>
			</li>
			<li class="has-dropdown">
				<a href="{{ route('item_list') }}"><i class="fa fa-medkit fa-lg"></i> Medicine</a>
				<ul class="dropdown">
					<li>
						<a href="{{ route('item_create') }}"><i class="fa fa-plus fa-lg"></i> New Medicine</a>
					</li>
					<li>
						<a href="{{ route('receive_index') }}"><i class="fa fa-arrow-circle-down fa-lg"></i> Receive</a>
					</li>
					<li>
						<a href="{{ route('dispense_index') }}"><i class="fa fa-arrow-circle-up fa-lg"></i> Dispense</a>
					</li>
				</ul>
			</li>
		
			<li class="has-dropdown">
				<a href="#"><i class="fa fa-files-o fa-lg"></i> Reports</a>
				<ul class="dropdown">
					<li>
						<a href="{{ route('hctdaily_report') }}"> HCT Daily Registry</a>
					</li>
					<li>
						<a href="{{ route('hctmonthly_report') }}"> HCT Monthly Report</a>
					</li>
					<li>
						<a href=""> Monthly ART Registry</a>
					<li>
						<a href=""> HIV Care Monthly Report</a>
					</li>
					<li>
						<a href=""> Pharmacy Inventory Report</a>
					</li>
					<li>
						<a href=""> HIV Statistic Monthly Report</a>
					</li>
					<li>
						<a href=""> Client Master List</a>
					</li>
					</li>
				</ul>
			</li>
		</ul>
	</section>
</nav>

