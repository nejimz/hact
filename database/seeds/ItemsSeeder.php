<?php

use Illuminate\Database\Seeder;
use App\Item;

class ItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        //Item::truncate();
        for ($i=0; $i < 50; $i++) 
        {
        	$start = \Carbon\Carbon::createFromTimeStamp( $faker->dateTimeBetween( '1 year', '+3 year' )->getTimestamp());
        	Item::create([
    			'item_code' 			=> $faker->isbn10,
    			'item_name' 			=> $faker->sentence(1),
    			'lot_number' 			=> strtoupper(str_random(10)),
    			'expiration_date' 		=> $start->toDateTimeString(),
    			'quantity_per_bottle' 	=> rand(100, 150)
        	]);
        }
    }
}
