<?php

use Illuminate\Database\Seeder;
use App\EnrolmentDemographics;

class DemographicsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        //Item::truncate();
        for ($i=0; $i < 50; $i++) 
        {
            $ui_code_1 = substr(str_random(10), 0, 2);
            $ui_code_2 = substr(str_random(10), 0, 2);
            $ui_code_3 = rand(01, 12);
            $ui_code = $ui_code_1 . '-' . $ui_code_2 . '-' . $ui_code_3;

        	$birth_date 			= \Carbon\Carbon::createFromTimeStamp( $faker->dateTimeBetween( '-60 year', '-10 year' )->getTimestamp() );
        	$date_retest 			= \Carbon\Carbon::createFromTimeStamp( $faker->dateTimeBetween( '1 days', '+7 days' )->getTimestamp());
        	$enrolment_date 		= \Carbon\Carbon::createFromTimeStamp( $faker->dateTimeBetween( '1 day', '+3 days' )->getTimestamp());
        	$diagnosis_date 		= \Carbon\Carbon::createFromTimeStamp( $faker->dateTimeBetween( '1 day', '+3 days' )->getTimestamp());
        	$initial_contract_date 	= \Carbon\Carbon::createFromTimeStamp( $faker->dateTimeBetween( '1 day', '+3 days' )->getTimestamp());

        	$random_gender = rand(0, 1);
        	$gender = ($random_gender == 1)? 'male' : 'female';

        	$occupational_exposure = rand(0, 1);
        	$provided_post_exposure_prophylaxis = ($occupational_exposure == 1)? rand(0, 1) : $occupational_exposure;

        	$hiv_tested = rand(0, 1);
        	$reason_for_not_testing = ($hiv_tested == 0)? $faker->sentence(1) : '';

        	$hiv_positive = rand(0, 1);
        	$hiv_positive_date_retest = ($hiv_positive == 1)? $date_retest->toDateTimeString() : null;

        	$civil_status_random = rand(1, 4);

        	if($civil_status_random = 1){
        		$civil_status = 'single';
        	}elseif($civil_status_random = 2){
        		$civil_status = 'married';
        	}elseif($civil_status_random = 3){
        		$civil_status = 'separated';
        	}else{
        		$civil_status = 'widowed';
        	}

        	EnrolmentDemographics::create([
    			'ui_code' 			=> $ui_code,
    			'first_name' 		=> $faker->firstName($gender),
    			'middle_name' 		=> $faker->lastName(),
    			'last_name' 		=> $faker->lastName($gender),
    			'gender' 			=> $gender,
    			'birth_date' 		=> $birth_date->toDateTimeString(),

    			'mother_first_name'	=> $faker->firstName('female'),
    			'mother_middle_name'=> $faker->lastName('female'),
    			'mother_last_name'	=> $faker->lastName('female'),

    			'street'	=> $faker->streetSuffix,
    			'sitio'		=> $faker->streetAddress,
    			'brgy'		=> $faker->address,
    			'city'		=> $faker->city,

    			'contact_number' => $faker->phoneNumber,


		        'initial_contract_date' => $initial_contract_date->toDateTimeString(),
		        'enrolment_date'        => $enrolment_date->toDateTimeString(),
		        'code_name'             => $faker->word,
		        'saccl_code'            => $faker->word,
		        'diagnosis_date'        => $diagnosis_date->toDateTimeString(),
		        'civil_status'          => $civil_status,
		        'birth_place_city'      => $faker->city,
		        'birth_place_province'  => $faker->country,
		        'philheath_number'      => $faker->isbn10,

    			'blood_transfusion'		=> rand(0, 1),
    			'injecting_drug_user'	=> rand(0, 1),
    			'substance_abuse'		=> rand(0, 1),

    			'occupational_exposure'	=> $occupational_exposure,
    			'provided_post_exposure_prophylaxis'=> $provided_post_exposure_prophylaxis,

    			'sexually_transmitted_infections'	=> rand(0, 1),
    			'multiple_sexual_partners' 			=> rand(0, 1),
    			'male_sex_with_other_males' 		=> rand(0, 1),
    			'sex_worker_client'	=> rand(0, 1),
    			'sex_worker'		=> rand(0, 1),
    			'child_of_hiv_infected_mother'	=> rand(0, 1),

    			'other_hiv_risks'	=> '',

    			'physician_user_id'	=> 1
        	]);
        }
    }
}
