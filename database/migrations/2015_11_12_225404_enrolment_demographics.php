<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EnrolmentDemographics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enrolment_demographics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ui_code', 20)->unique();

            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');

            $table->string('gender', 10);
            $table->date('birth_date');

            $table->string('mother_first_name');
            $table->string('mother_middle_name');
            $table->string('mother_last_name');

            $table->string('street');
            $table->string('sitio');
            $table->string('brgy');
            $table->string('city');

            $table->string('contact_number');
            //
            $table->date('initial_contract_date');
            $table->date('enrolment_date');
            $table->string('code_name');
            $table->string('saccl_code');
            $table->date('diagnosis_date');

            $table->string('civil_status', 20);
            $table->string('birth_place_city');
            $table->string('birth_place_province');
            $table->string('philheath_number', 60);
            // VCT HIV Risks
            $table->tinyInteger('blood_transfusion');
            $table->tinyInteger('injecting_drug_user');
            $table->tinyInteger('substance_abuse');

            $table->tinyInteger('occupational_exposure');
            $table->tinyInteger('provided_post_exposure_prophylaxis');

            $table->tinyInteger('sexually_transmitted_infections');
            $table->tinyInteger('multiple_sexual_partners');
            $table->tinyInteger('male_sex_with_other_males');
            $table->tinyInteger('sex_worker_client');
            $table->tinyInteger('sex_worker');
            $table->tinyInteger('child_of_hiv_infected_mother');
            $table->string('other_hiv_risks');
            // Attending Phycisian
            $table->integer('physician_user_id')->unsigned();

            //  Foreign Keys
            $table->foreign('physician_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('enrolment_demographics');
    }
}
