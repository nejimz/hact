<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventArvRefill extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_arv_refill', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('demographics_id')->unsigned();
            $table->string('blood_pressure');
            $table->string('temperature');
            $table->string('pulse_rate');
            $table->string('respiration_rate');
            $table->string('weight');
            $table->tinyInteger('change_arv_regimen');
            $table->string('change_arv_regimen_yes_reason');
            $table->date('date_change')->nullable()->default(null);
            $table->string('new_arv_regimen');
            $table->string('recommendations');

            //  Foreign Keys
            $table->foreign('demographics_id')->references('id')->on('enrolment_demographics');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_arv_refill');
    }
}
