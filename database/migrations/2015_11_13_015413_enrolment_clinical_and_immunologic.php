<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EnrolmentClinicalAndImmunologic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enrolment_clinical_and_immunologic', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('demographics_id')->unsigned();
            $table->string('laboratory', 50);
            $table->string('laboratory_others');
            $table->date('date_result');
            $table->string('result');
            $table->timestamp('date_created')->default(DB::raw('CURRENT_TIMESTAMP'));
            
            // Foreign
            $table->foreign('demographics_id')->references('id')->on('enrolment_demographics');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('enrolment_clinical_and_immunologic');
    }
}
