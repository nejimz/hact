<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ItemReceive extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_receive', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id')->unsigned();
            $table->integer('quantity');
            $table->integer('user_id')->unsigned();
            $table->timestamp('transaction_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            //  Foreign Keys
            $table->foreign('item_id')->references('id')->on('item');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('item_receive');
    }
}
