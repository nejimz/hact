<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->integer('role_id')->unsigned();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

            //  Foreign Keys
            $table->foreign('role_id')->references('id')->on('user_role');
        });

        App\User::create([
            'first_name'    => 'John',
            'last_name'     => 'Doe',
            'email'         => 'admin@gmail.com',
            'password'      => bcrypt('p@ssw0rd'),
            'role_id'       => 1
        ]);

        App\User::create([
            'first_name'    => 'John',
            'last_name'     => 'Duenas',
            'email'         => 'john@hactbacolod.com',
            'password'      => bcrypt('p@ssw0rd'),
            'role_id'       => 1
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
