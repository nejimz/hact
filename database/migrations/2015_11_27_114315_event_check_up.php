<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventCheckUp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::create('event_checkup', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('demographics_id')->unsigned();

            $table->date('checkup_date');
            $table->string('blood_pressure');
            $table->string('temperature');
            $table->string('pulse_rate');
            $table->string('respiration_rate');
            $table->string('weight');

            $table->tinyInteger('tuberculosis');
            $table->string('site');
            $table->string('regimen');
            $table->string('drug_resistance_value');
            $table->string('drug_resistance_value_specify');
            $table->tinyInteger('diagnosed_other');
            $table->string('diagnosed_other_input');

            $table->tinyInteger('hepatitis_b');
            $table->tinyInteger('hepatitis_c');
            $table->tinyInteger('pneumocystis_pneumonia');
            $table->tinyInteger('oropharyngeal_candidiasis');
            $table->tinyInteger('syphilis');
            $table->tinyInteger('stis');
            $table->tinyInteger('sti_reason');
            $table->string('recommendation');

            //foreign key
            $table->foreign('demographics_id')->references('id')->on('enrolment_demographics');   
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('event_checkup');
    }
}
