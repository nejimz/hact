<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EnrolmentOpportunisticInfections extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enrolment_opportunistic_infections', function (Blueprint $table){
            $table->increments('id');
            $table->integer('demographics_id')->unsigned();
            $table->tinyInteger('started_with_ipt')->default(0);
            $table->string('no_ipt_reason')->nullable();

            $table->tinyInteger('hepatitis_b')->default(0);
            $table->tinyInteger('hepatitis_c')->default(0);
            $table->tinyInteger('pneumocystis_pneumonia')->default(0);
            $table->tinyInteger('oropharyngeal_candidiasis')->default(0);
            $table->tinyInteger('syphilis')->default(0);
            $table->tinyInteger('stis')->default(0);
            $table->string('stis_specify')->default(0);
            $table->tinyInteger('others')->default(0);
            $table->string('others_specify')->nullable();

            $table->string('site_value')->default('');

            $table->string('tb_regimen_value', 30)->default('');

            $table->string('drug_resistance_value', 30)->default('');
            $table->string('drug_resistance_value_specify')->default('');

            $table->string('treatment_outcome_value', 30)->default('');
            $table->string('treatment_outcome_value_specify')->default('');

            $table->foreign('demographics_id')->references('id')->on('enrolment_demographics');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('enrolment_opportunistic_infections');
    }
}
