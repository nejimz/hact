<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ARV extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_arv', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('demographics_id')->unsigned();

            $table->string('blood_pressure');
            $table->string('temperature');
            $table->string('pulse_rate');
            $table->string('respiration_rate');
            $table->string('weight');

            $table->tinyInteger('tuberculosis');
            $table->string('site');
            $table->string('regimen');
            $table->string('drug_resistance_value');
            $table->string('drug_resistance_value_specify');

            $table->tinyInteger('hepatitis_b');
            $table->tinyInteger('hepatitis_c');
            $table->tinyInteger('pneumocystis_pneumonia');
            $table->tinyInteger('oropharyngeal_candidiasis');
            $table->tinyInteger('syphilis');
            $table->tinyInteger('stis');
            $table->string('stis_reason');
            $table->tinyInteger('diagnosed_other');
            $table->string('diagnosed_other_input');

            $table->string('classification');
            $table->date('arv_date');
            $table->tinyInteger('low_cd4_count');
            $table->tinyInteger('active_tb');
            $table->tinyInteger('child_5yr');
            $table->tinyInteger('hep_b_c');
            $table->tinyInteger('pregnant_breastfeeding');
            $table->tinyInteger('who_classification');
            $table->string('reason_other');
            $table->string('recommendations');

            //  Foreign Keys
            $table->foreign('demographics_id')->references('id')->on('enrolment_demographics');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('event_arv');
    }
}
