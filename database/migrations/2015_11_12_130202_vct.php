<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Vct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vct', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ui_code', 20)->unique();
            $table->date('vct_date');

            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');

            $table->string('gender', 10);
            $table->date('birth_date');

            $table->string('mother_first_name');
            $table->string('mother_middle_name');
            $table->string('mother_last_name');

            $table->string('street');
            $table->string('sitio');
            $table->string('brgy');
            $table->string('city');

            $table->string('contact_number');
            
            $table->tinyInteger('blood_transfusion');
            $table->tinyInteger('injecting_drug_user');
            $table->tinyInteger('substance_abuse');
            //  Occupational Exposure
            $table->tinyInteger('occupational_exposure');
            $table->tinyInteger('provided_post_exposure_prophylaxis');

            $table->tinyInteger('sexually_transmitted_infections');
            $table->tinyInteger('multiple_sexual_partners');
            $table->tinyInteger('male_sex_with_other_male');
            $table->tinyInteger('sex_worker_client');
            $table->tinyInteger('sex_worker');
            $table->tinyInteger('child_of_hiv_infected_mother');
            //  HIV Tested
            $table->tinyInteger('hiv_tested');
            $table->string('reason_for_not_testing')->nullable();
            //  HIV Positive
            $table->tinyInteger('hiv_positive');
            $table->date('hiv_positive_date_retest')->nullable();

            $table->tinyInteger('provide_post_test_counselling_and_hiv_result');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vct');
    }
}
