<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EnrolmentMedication extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enrolment_medication', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('demographics_id')->unsigned();
            $table->integer('item_id')->unsigned();
            $table->string('dosage');
            $table->string('frequency');
            $table->date('date_started');
            $table->date('date_discontinued')->nullable()->default(null);
            $table->string('discontinuation_reason')->nullable()->default(null);

            //  Foreign Keys
            $table->foreign('demographics_id')->references('id')->on('enrolment_demographics');
            $table->foreign('item_id')->references('id')->on('item');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('enrolment_medication');
    }
}
