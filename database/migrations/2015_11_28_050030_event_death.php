<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventDeath extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_death', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('demographics_id')->unsigned();
            $table->date('death_date');
            $table->string('death_cause');
            $table->string('final_diagnosis');

            $table->tinyInteger('tuberculosis');
            $table->tinyInteger('pneumocystic_pneumonia');
            $table->tinyInteger('cryptococcal_meningitis');
            $table->tinyInteger('cytomegalovirus');
            $table->tinyInteger('candidiasis');
            $table->tinyInteger('toxoplasmosis');
            $table->tinyInteger('none');
            $table->string('other');

            //  Foreign Keys
            $table->foreign('demographics_id')->references('id')->on('enrolment_demographics');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_death');
    }
}
