<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VCT extends Model
{
    protected $table = 'vct';

    public $timestamps = false;

    /*
    Relationships
    */

    public function EnrolmentDemographics()
    {
        return $this->belongsTo('App\EnrolmentDemographics', 'id', 'id');
    }

    public function EnrolmentClinicalAndImmunologic()
    {
        return $this->belongsTo('App\EnrolmentClinicalAndImmunologic', 'id', 'id');
    }

    public function OpportunisticInfections()
    {
        return $this->belongsTo('App\OpportunisticInfections', 'id', 'id');
    }

    /*
    Set Functions
    */

    public function setUiCodeAttribute($value)
    {
        $this->attributes['ui_code'] = strtoupper($value);
    }

    public function setHivPositiveDateRetestAttribute($value)
    {
        if(is_null($value)){
            $value = null;
        }else{
            $value = date('Y-m-d', strtotime($value));
        }

        $this->attributes['hiv_positive_date_retest'] = $value;
    }

    /*
    Get Functions
    */

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getFullName2Attribute()
    {
        return $this->last_name . ', ' . $this->first_name;
    }

    public function getGenderAttribute($value)
    {
        return ucwords($value);
    }

    public function getBirthDateAttribute($value)
    {
        return date('F d, Y', strtotime($value));
    }

    public function getHivPositiveFormatAttribute()
    {
        if($this->hiv_positive){
            return 'Yes';
        }else{
            return 'No';
        }
    }
    /*
    Other Functions
    */

    public function scopeSearchVctList($query, $search)
    {
        $search = '%' . $search . '%';

        return $query->where('first_name', 'LIKE', $search)
                    ->orWhere('last_name', 'LIKE', $search)
                    ->orWhere('ui_code', 'LIKE', $search)
                    ->orWhere('gender', 'LIKE', $search)
                    ->orWhere('vct_date', 'LIKE', $search);
    }

    public function maleCapital()
    {
    	return strtoupper($this->gender);
    }
}
