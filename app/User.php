<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'email', 'password', 'role_id', 'reset'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that assign to delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function UserRole()
    {
        return $this->hasOne('App\UserRole', 'id', 'role_id');
    }

    public function scopeSearchUserList($query, $search)
    {
        $search = '%' . $search . '%';

        return $query->where('first_name', 'LIKE', $search)
                    ->orWhere('last_name', 'LIKE', $search)
                    ->orWhere('email', 'LIKE', $search);
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getFullName2Attribute()
    {
        return $this->last_name . ', ' . $this->first_name;
    }

    public function item()
    {
        return $this->hasMany('App\Item');
    }

    public function item_receive()
    {
        return $this->hasMany('App\ItemReceive');
    }

    public function item_dispense()
    {
        return $this->hasMany('App\ItemDispense');
    }
}
