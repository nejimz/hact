<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkup extends Model
{
    //
    protected $table = 'event_checkup';

    public $timestamps = false;
    /**
    Relationships
    **/

    public function EnrolmentDemographics()
    {
        return $this->hasOne('App\EnrolmentDemographics', 'id', 'demographics_id');
    }

    /**
    Setters to Insert
    **/
    public function setHepatitisBAttribute($value)
    {
        if(is_null($value)){
            $value = 0;
        }else{
            $value = 1;
        }

        $this->attributes['hepatitis_b'] = $value;
    }

    public function setHepatitisCAttribute($value)
    {
        if(is_null($value)){
            $value = 0;
        }else{
            $value = 1;
        }

        $this->attributes['hepatitis_c'] = $value;
    }
    public function setPneumocystisPneumoniaAttribute($value)
    {
        if(is_null($value)){
            $value = 0;
        }else{
            $value = 1;
        }

        $this->attributes['pneumocystis_pneumonia'] = $value;
    }
    public function setOropharyngealCandidiasisAttribute($value)
    {
        if(is_null($value)){
            $value = 0;
        }else{
            $value = 1;
        }

        $this->attributes['oropharyngeal_candidiasis'] = $value;
    }
    public function setSyphilisAttribute($value)
    {
        if(is_null($value)){
            $value = 0;
        }else{
            $value = 1;
        }

        $this->attributes['syphilis'] = $value;
    }
    public function setStisAttribute($value)
    {
        if(is_null($value)){
            $value = 0;
        }else{
            $value = 1;
        }

        $this->attributes['stis'] = $value;
    }
    public function setDiagnosedOtherAttribute($value)
    {
        if(is_null($value)){
            $value = 0;
        }else{
            $value = 1;
        }

        $this->attributes['diagnosed_other'] = $value;
    }
    public function setDiagnosedOtherInputAttribute($value)
    {
        if(is_null($value)){
            $value = 0;
        }else{
            $value = 1;
        }

        $this->attributes['diagnosed_other_input'] = $value;
    }

    public function scopeSearchDemographicsList($query, $search)
    {
        $search = '%' . $search . '%';

        return $query->where('first_name', 'LIKE', $search)
                    ->orWhere('last_name', 'LIKE', $search)
                    ->orWhere('saccl_code', 'LIKE', $search)
                    ->orWhere('code_name', 'LIKE', $search);
    }
    public function getCheckUpFormatAttribute()
    {
        return date('F d, Y', strtotime($this->checkup_date));
    }
    
}
