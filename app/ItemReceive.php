<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemReceive extends Model
{
    protected $table = 'item_receive';

    public function setTransactionDateAttribute($value)
    {
        $this->attributes['transaction_date'] = date("Y-m-d", strtotime($value));
    }

    public function scopeSearchItemReceiveList($query, $search)
    {
        $search = '%' . $search . '%';

        return $query->where('item.item_name', 'LIKE', $search)
                    ->orWhere('quantity', 'LIKE', $search);
    }


    public function Item()
    {
        return $this->belongsTo('App\Item', 'id', 'id');
    }

    public function ItemMany()
    {
        return $this->hasMany('App\Item');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public $timestamps = false;
}
