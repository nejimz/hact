<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table = 'user_role';
    protected $fillable = ['role_name'];

    public $timestamps = false;

    public function User()
    {
        return $this->belongsTo('App\User', 'id', 'id');
    }
}
