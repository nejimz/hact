<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnrolmentDemographics extends Model
{
    //
    protected $table = 'enrolment_demographics';

    public $timestamps = false;

    /**
    Relationships
    **/

    public function EnrolmentClinicalAndImmunologic()
    {
        return $this->belongsTo('App\EnrolmentClinicalAndImmunologic', 'id', 'id');
    }

    public function EnrolmentOpportunisticInfections()
    {
        return $this->belongsTo('App\EnrolmentOpportunisticInfections', 'id', 'id');
    }

    public function EnrolmentMedication()
    {
        return $this->belongsTo('App\EnrolmentMedication', 'id', 'id');
    }

    public function ARVR()
    {
        return $this->belongsTo('App\ARV', 'demographics_id', 'id');
    }

    public function ARVRefill()
    {
        return $this->belongsTo('App\ARVRefill', 'demographics_id', 'id');
    }
    public function Checkup()
    {
        return $this->belongsTo('App\Checkup', 'demographics_id', 'id');
    }

    public function Death()
    {
        return $this->belongsTo('App\Death', 'demographics_id', 'id');
    }

    #

    /*public function DeathData()
    {
        return $this->hasOne('App\EnrolmentDemographics', 'demographics_id', 'id');
    }*/

    /**
    Setters to Insert
    **/

    public function setUiCodeAttribute($value)
    {
        $this->attributes['ui_code'] = strtoupper($value);
    }

    public function setBirthDateAttribute($value)
    {
        $this->attributes['birth_date'] = date('Y-m-d', strtotime($value));
    }

    public function scopeSearchDemographicsList($query, $search)
    {
        $search = '%' . $search . '%';

        return $query->where('first_name', 'LIKE', $search)
                    ->orWhere('last_name', 'LIKE', $search)
                    ->orWhere('saccl_code', 'LIKE', $search)
                    ->orWhere('code_name', 'LIKE', $search);
    }

    public function setInitialContractDateAttribute($value)
    {
        $this->attributes['initial_contract_date'] = date('Y-m-d', strtotime($value));
    }

    public function setEnrolmentDateAttribute($value)
    {
    	$this->attributes['enrolment_date'] = date('Y-m-d', strtotime($value));
    }

    public function setDiagnosisDateAttribute($value)
    {
    	$this->attributes['diagnosis_date'] = date('Y-m-d', strtotime($value));
    }

    /**
    Getters to Select
    **/

    public function getInitialContractDateAttribute($value)
    {
        return date('F d, Y', strtotime($value));
    }

    public function getEnrolmentDateAttribute($value)
    {
        return date('F d, Y', strtotime($value));
    }

    public function getDiagnosisDateAttribute($value)
    {
        return date('F d, Y', strtotime($value));
    }

    /**
    Other functionality
    **/

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getFullName2Attribute()
    {
        return $this->last_name . ', ' . $this->first_name;
    }

    public function getGenderFormatAttribute()
    {
        return ucwords($this->gender);
    }

    public function getBirthDateFormatAttribute($value)
    {
        return date('F d, Y', strtotime($this->birth_date));
    }
}
