<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ARVRefill extends Model
{
    //
    protected $table = 'event_arv_refill';

    public $timestamps = false;

    /**
    Relationships
    **/

    public function EnrolmentDemographics()
    {
        return $this->hasOne('App\EnrolmentDemographics', 'id', 'demographics_id');
    }

    /**
    Setters to Insert
    **/

    public function setDateChangeAttribute($value)
    {
    	$this->attributes['date_change'] = date('Y-m-d', strtotime($value));
    }

    /**
    Other functionality
    **/

    public function Demographics(){
        return $this->ARVRefill;
    }
}
