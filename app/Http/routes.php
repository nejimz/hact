<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function (Request $request) {
	$data = [
		'request'=>$request
	];
    return view('laravel.login', $data);
});*/

Route::get('/', ['as' =>'login_page','uses' => 'Auth\AuthController@getLogin']);
Route::post('/', ['as' =>'login_auth','uses' => 'Auth\AuthController@postLogin']);

//	Portal
Route::group(['prefix' => 'portal', 'middleware' => 'auth'], function(){
	Route::get('/logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);
	Route::get('/',['as' =>'dashboard','uses' => 'DashboardController@index']);
	

	//	Password Reset
	Route::group(['prefix' => 'password-reset'], function(){
		Route::get('/',['as' =>'reset_password','uses' => 'UserController@reset_password']);
		Route::post('/',['as' =>'password_reset','uses' => 'UserController@password_reset']);
	});// Password Reset

	//	Enrolment
	Route::group(['prefix' => 'enrolment'], function(){
		// Search Item
		Route::get('/search-item',['as' =>'search_item', 'uses' => 'ItemController@search']);
		// Search Physician
		Route::get('/search-physician',['as' =>'search_physician','uses' => 'UserController@search_physician']);
		// Search VCT Patient
		Route::get('/search-patient',['as' =>'search_vct_patient','uses' => 'VCTController@search_patient']);
		// Get Patient Record on VCT
		Route::get('/vct-record',['as' =>'vct_record','uses' => 'VCTController@vct_record']);
		// Search Demographics Patient
		Route::get('/search-demographics-patient',['as' =>'search_demographics_patient','uses' => 'DemographicsController@search_patient']);
		
		//	VCT
		Route::group(['prefix' => 'vct'], function(){
			Route::get('/',['as' =>'vct_index','uses' => 'VCTController@index']);
					
			Route::get('/create',['as' =>'vct_create', 'uses' => 'VCTController@create']);
			Route::post('/store',['as' =>'vct_store', 'uses' => 'VCTController@store']);
					
			Route::get('/edit/{id}',['as' =>'vct_edit', 'uses' => 'VCTController@edit']);
			Route::post('/update/{id}',['as' =>'vct_update', 'uses' => 'VCTController@update']);
		});// VCT
		//	Demographics
		Route::group(['prefix' => 'demographics'], function(){
			Route::get('/',['as' =>'demographics_index','uses' => 'DemographicsController@index']);
					
			Route::get('/create',['as' =>'demographics_create', 'uses' => 'DemographicsController@create']);
			Route::post('/store',['as' =>'demographics_store', 'uses' => 'DemographicsController@store']);
					
			Route::get('/edit/{id}',['as' =>'demographics_edit', 'uses' => 'DemographicsController@edit']);
			Route::post('/update/{id}',['as' =>'demographics_update', 'uses' => 'DemographicsController@update']);
		});// Demographics
		//	Clinical and Immunological Profile
		Route::group(['prefix' => 'clinical-and-immunologic-profile'], function(){
			Route::get('/',['as' =>'caip_index','uses' => 'ClinicalAndImmunologicProfileController@index']);
			Route::get('/show/{id}',['as' =>'caip_show', 'uses' => 'ClinicalAndImmunologicProfileController@show']);	
					
			Route::get('/create/{id?}',['as' =>'caip_create', 'uses' => 'ClinicalAndImmunologicProfileController@create']);
			Route::post('/store',['as' =>'caip_store', 'uses' => 'ClinicalAndImmunologicProfileController@store']);	

			Route::get('/edit/{id}',['as' =>'caip_edit', 'uses' => 'ClinicalAndImmunologicProfileController@edit']);
			Route::post('/update/{id}',['as' =>'caip_update', 'uses' => 'ClinicalAndImmunologicProfileController@update']);

			Route::get('/destroy/{id}',['as' =>'caip_destroy', 'uses' => 'ClinicalAndImmunologicProfileController@destroy']);
		});// Clinical and Immunological Profile
		//	Opportunistic Infections Profile
		Route::group(['prefix' => 'opportunistic-infections-profile'], function(){
			Route::get('/',['as' =>'oi_profile_index','uses' => 'OpportunisticInfectionsProfileController@index']);
			Route::get('/show/{id}',['as' =>'oi_profile_show', 'uses' => 'OpportunisticInfectionsProfileController@show']);
					
			Route::get('/create',['as' =>'oi_profile_create', 'uses' => 'OpportunisticInfectionsProfileController@create']);
			Route::post('/store',['as' =>'oi_profile_store', 'uses' => 'OpportunisticInfectionsProfileController@store']);
					
			Route::get('/edit/{id}',['as' =>'oi_profile_edit', 'uses' => 'OpportunisticInfectionsProfileController@edit']);
			Route::post('/update/{id}',['as' =>'oi_profile_update', 'uses' => 'OpportunisticInfectionsProfileController@update']);
		});// Medication Profile
		//	Medication Profile
		Route::group(['prefix' => 'medication-profile'], function(){
			Route::get('/',['as' =>'medication_index','uses' => 'MedicationProfileController@index']);
			Route::get('/show/{id}',['as' =>'medication_show', 'uses' => 'MedicationProfileController@show']);		
					
			Route::get('/create/{id?}',['as' =>'medication_create', 'uses' => 'MedicationProfileController@create']);
			Route::post('/store',['as' =>'medication_store', 'uses' => 'MedicationProfileController@store']);
					
			Route::get('/edit/{id}',['as' =>'medication_edit', 'uses' => 'MedicationProfileController@edit']);
			Route::post('/update/{id}',['as' =>'medication_update', 'uses' => 'MedicationProfileController@update']);

			Route::get('/edit-stop/{id}',['as' =>'medication_edit_stop', 'uses' => 'MedicationProfileController@edit_stop']);
			Route::post('/update-stop/{id}',['as' =>'medication_update_stop', 'uses' => 'MedicationProfileController@update_stop']);

			Route::get('/destroy/{id}',['as' =>'medication_destroy', 'uses' => 'MedicationProfileController@destroy']);	
			Route::get('/continue-medication/{id}',['as' =>'medication_continue', 'uses' => 'MedicationProfileController@continue_medication']);			
		});// Medication Profile

	});// Enrolment

	//	Event
	Route::group(['prefix' => 'event'], function(){
		//ARV
		Route::group(['prefix' => 'arv'], function(){
			Route::get('/',['as' => 'arv_index','uses' => 'ARVController@index']);
			Route::get('/show/{id}',['as' =>'arv_show', 'uses' => 'ARVController@show']);

			Route::get('/create/{id?}', ['as' => 'arv_create', 'uses' => 'ARVController@create']);
			Route::post('/store', ['as' => 'arv_store', 'uses' => 'ARVController@store']);
					
			Route::get('/edit/{id}',['as' => 'arv_edit', 'uses' => 'ARVController@edit']);
			Route::post('/update/{id}',['as' => 'arv_update', 'uses' => 'ARVController@update']);
		});	//ARV

		//	ARV Refill
		Route::group(['prefix' => 'arv-refill'], function(){
			Route::get('/',['as' => 'arv_refill_index','uses' => 'ARVRefillController@index']);
					
			Route::get('/create',['as' => 'arv_refill_create', 'uses' => 'ARVRefillController@create']);
			Route::post('/store',['as' => 'arv_refill_store', 'uses' => 'ARVRefillController@store']);
					
			Route::get('/edit/{id}',['as' => 'arv_refill_edit', 'uses' => 'ARVRefillController@edit']);
			Route::post('/update/{id}',['as' => 'arv_refill_update', 'uses' => 'ARVRefillController@update']);
		});// ARV Refill
		// Check UP
		Route::group(['prefix' => 'check-up'], function(){
			Route::get('/', ['as' => 'checkup_index', 'uses' => 'CheckUpController@index']);
			Route::get('/create', ['as' => 'checkup_create', 'uses' => 'CheckUpController@create']);
			Route::post('/store', ['as' => 'checkup_store', 'uses' => 'CheckUpController@store']);
			Route::get('/edit/{id}', ['as' => 'checkup_edit', 'uses' => 'CheckUpController@edit']);
			Route::post('/update/{id}', ['as' => 'checkup_update', 'uses' => 'CheckUpController@update']);
			Route::get('/show/{id}', ['as' => 'checkup_show', 'uses' => 'CheckUpController@show']);
		});// Check UP

		//	Death
		Route::group(['prefix' => 'death'], function(){
			Route::get('/',['as' => 'death_index','uses' => 'DeathController@index']);

			Route::get('/create', ['as' => 'death_create', 'uses' => 'DeathController@create']);
			Route::post('/store', ['as' => 'death_store', 'uses' => 'DeathController@store']);
					
			Route::get('/edit/{id}',['as' => 'death_edit', 'uses' => 'DeathController@edit']);
			Route::post('/update/{id}',['as' => 'death_update', 'uses' => 'DeathController@update']);
		});	//ARV

	});// Event

	//	Reports
	Route::group(['prefix' => 'reports'], function(){
		Route::get('/form-b/{id?}',['as' =>'form_b','uses' => 'PrintFormsController@form_b']);
		Route::get('/form-c/{id?}',['as' =>'form_c','uses' => 'PrintFormsController@form_c']);
		Route::get('/form-d/{id?}',['as' =>'form_d','uses' => 'PrintFormsController@form_d']);
	});// Password Reset

	//	Item
	Route::group(['prefix' => 'item'], function(){
		Route::get('/',['as' =>'item_list', 'uses' => 'ItemController@index']);
			
		Route::get('/create',['as' =>'item_create','uses' => 'ItemController@create']);
		Route::post('/store',['as' =>'item_store','uses' => 'ItemController@store']);

		Route::get('/edit/{id}',['as' =>'item_edit','uses' => 'ItemController@edit']);
		Route::post('/update/{id}',['as' =>'item_update','uses' => 'ItemController@update']);

		Route::get('/destroy/{id}',['as' =>'item_destroy','uses' => 'ItemController@destroy']);

		/*
		//	Category
		Route::group(['prefix' => 'category'], function(){
			Route::get('/',['as' =>'item_category_list','uses' => 'ItemCategoryController@index']);

			Route::get('/create',['as' =>'item_category_create','uses' => 'ItemCategoryController@create']);
			Route::post('/store',['as' =>'item_category_store','uses' => 'ItemCategoryController@store']);

			Route::get('/edit/{id}',['as' =>'item_category_edit','uses' => 'ItemCategoryController@edit']);
			Route::post('/update/{id}',['as' =>'item_category_update','uses' => 'ItemCategoryController@update']);

		});// Category
		*/

		//	Receive
		Route::group(['prefix' => 'receive'], function(){
			Route::get('/',['as' =>'receive_index','uses' => 'ItemReceiveController@index']);
			Route::get('/create',['as' =>'receive_create','uses' => 'ItemReceiveController@create']);
			Route::get('/store',['as' =>'receive_store','uses' => 'ItemReceiveController@store']);
			Route::get('/edit/{id}',['as' =>'receive_edit','uses' => 'ItemReceiveController@edit']);
			Route::get('/update/{id}',['as' =>'receive_update','uses' => 'ItemReceiveController@update']);
			Route::get('/destroy/{id}',['as' =>'receive_destroy','uses' => 'ItemReceiveController@destroy']);
		});// Receive

		//	Dispense
		Route::group(['prefix' => 'dispense'], function(){
			Route::get('/',['as' =>'dispense_index','uses' => 'ItemDispenseController@index']);
			Route::get('/create',['as' =>'dispense_create','uses' => 'ItemDispenseController@create']);
			Route::get('/store',['as' =>'dispense_store','uses' => 'ItemDispenseController@store']);
			Route::get('/edit/{id}',['as' =>'dispense_edit','uses' => 'ItemDispenseController@edit']);
			Route::get('/update/{id}',['as' =>'dispense_update','uses' => 'ItemDispenseController@update']);
			Route::get('/destroy/{id}',['as' =>'dispense_destroy','uses' => 'ItemDispenseController@destroy']);

		});// Dispense
	});// Item

	//Report
	Route::group(['prefix' => 'report'], function(){
		Route::get('/hctdaily' , ['as' => 'hctdaily_report' , 'uses' => 'ReportController@hctdaily']);
		Route::get('/hctmonthly' , ['as' => 'hctmonthly_report' , 'uses' => 'ReportController@hctmonthly']);
	});//Report

	//	Admin
	Route::group(['prefix' => 'admin'], function(){
		Route::get('/',['as' =>'admin_home','uses' => 'DashboardController@index']);

		//	User
		Route::group(['prefix' => 'user'], function(){
			Route::get('/',['as' =>'user_index','uses' => 'UserController@index']);
				
			Route::get('/create',['as' =>'user_create','uses' => 'UserController@create']);
			Route::post('/store',['as' =>'user_store','uses' => 'UserController@store']);

			Route::get('/edit/{id}',['as' =>'user_edit','uses' => 'UserController@edit']);
			Route::post('/update/{id}',['as' =>'user_update','uses' => 'UserController@update']);

			Route::get('/destroy/{id}',['as' =>'user_destroy','uses' => 'UserController@destroy']);
			Route::get('/reset-password/{id}',['as' =>'user_reset_password','uses' => 'UserController@reset_generated_password']);

		});// User

	});// Admin
});// Portal
