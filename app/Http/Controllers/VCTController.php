<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\VCT;
use DB;

class VCTController extends Controller
{
    public function search_patient(Request $request)
    {
        //
        $search = '%' . trim($request->input('search_patient')) . '%';

        $patients = VCT::select(DB::raw('CONCAT(last_name, \', \', first_name) AS full_name_raw'), 'id')
                    ->where('first_name', 'LIKE', $search)
                    ->orWhere('last_name', 'LIKE', $search)
                    ->take(20)
                    ->lists('full_name_raw', 'id');

        return response()->json($patients);
    }

    public function vct_record(Request $request)
    {
        //
        $id = trim($request->input('vct_record_id'));

        $patient = VCT::where('id', $id)->first();

        return response()->json($patient);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $paginate   = 50;
        $search     = '';
        $order_by   = $this->vct_order_by($request);
        $sort       = $this->sort_query($request);
    
        if($request->has('search'))
        {
            $search = trim($request->input('search'));

            $patients = VCT::searchVctList($search)
                    ->orderBy($order_by, $sort)
                    ->paginate($paginate);
        }
        else
        {
            $patients = VCT::orderBy($order_by, $sort)
                    ->paginate($paginate);
        }

        $patient_name_sort             = $this->vct_link_sort('patient_name', $search, $sort, $request);
        $ui_code_sort             = $this->vct_link_sort('ui_code', $search, $sort, $request); 
        $gender_sort            = $this->vct_link_sort('gender', $search, $sort, $request); 
        $vct_date_sort       = $this->vct_link_sort('vct_date', $search, $sort, $request); 
        $hiv_positive_sort       = $this->vct_link_sort('hiv_positive', $search, $sort, $request); 
        $pagination         = ['search' => $search, 'order_by' => $order_by, 'sort' => $sort];

        $data = compact('patients', 'search', 'patient_name_sort', 'ui_code_sort', 'gender_sort', 'vct_date_sort', 'hiv_positive_sort', 'pagination');
    
        return view('hact.enrolment.vct.index', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $action     = route('vct_store');
        $id         = old('');
        $vct_date   = old('vct_date');

        $first_name     = old('first_name');
        $middle_name    = old('middle_name');
        $last_name      = old('last_name');

        $uic_mother_name = old('uic_mother_name');
        $uic_father_name = old('uic_father_name');
        $uic_birth_order = old('uic_birth_order');

        $mother_first_name  = old('mother_first_name');
        $mother_middle_name = old('mother_middle_name');
        $mother_last_name   = old('mother_last_name');

        $gender     = old('gender');
        $birth_date = old('birth_date');
        $age        = old('age');

        $street = old('street');
        $purok  = old('purok');
        $city   = old('city');
        $barangay = old('barangay');
        $contact_number = old('contact_number');

        $blood_transfusion      = old('blood_transfusion');
        $injecting_drug_user    = old('injecting_drug_user');
        $substance_abuse        = old('substance_abuse');
        $occupational_exposure  = old('occupational_exposure');

        $provided_post_exposure_prophylaxis = old('provided_post_exposure_prophylaxis');
        $sexually_transmitted_infections    = old('sexually_transmitted_infections');

        $multiple_sexual_partners = old('multiple_sexual_partners');
        $male_sex_with_other_male = old('male_sex_with_other_male');

        $sex_worker_client  = old('sex_worker_client');
        $sex_worker         = old('sex_worker');

        $child_of_hiv_infected_mother = old('child_of_hiv_infected_mother');

        $tested_for_hiv         = old('tested_for_hiv');
        $reason_for_not_testing = old('reason_for_not_testing');

        $positive_for_hiv           = old('positive_for_hiv');
        $hiv_positive_date_retest   = old('hiv_positive_date_retest');

        $provide_post_test_counselling_and_hiv_result = old('provide_post_test_counselling_and_hiv_result');

        $data = compact('action', 'vct_date', 

            'first_name', 'middle_name', 'last_name', 

            'uic_mother_name', 'uic_father_name', 'uic_birth_order', 

            'mother_first_name', 'mother_middle_name', 'mother_last_name', 

            'gender', 'birth_date', 'age',

            'street', 'purok', 'city', 'barangay', 'contact_number', 

            'blood_transfusion', 'injecting_drug_user', 'substance_abuse', 'occupational_exposure', 

            'provided_post_exposure_prophylaxis', 'sexually_transmitted_infections', 

            'multiple_sexual_partners', 'male_sex_with_other_male', 

            'sex_worker_client', 'sex_worker', 'child_of_hiv_infected_mother',

            'tested_for_hiv', 'reason_for_not_testing', 

            'positive_for_hiv', 'hiv_positive_date_retest', 

            'provide_post_test_counselling_and_hiv_result', 
            'id'
            );
        
        return view('hact.enrolment.vct.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\VCTStoreRequest $request)
    {
        $ui_code                = $request->input('uic_mother_name') . '-' . $request->input('uic_father_name') . '-' . $request->input('uic_birth_order');

        $blood_transfusion      = ($request->has('blood_transfusion'))? 1 : 0;
        $injecting_drug_user    = ($request->has('injecting_drug_user'))? 1 : 0;
        $substance_abuse        = ($request->has('substance_abuse'))? 1 : 0;
        $occupational_exposure  = ($request->has('occupational_exposure'))? 1 : 0;
        $provided_post_exposure_prophylaxis = ($request->has('provided_post_exposure_prophylaxis'))? 1 : 0;

        $sexually_transmitted_infections    = ($request->has('sexually_transmitted_infections'))? 1 : 0;
        $multiple_sexual_partners       = ($request->has('multiple_sexual_partners'))? 1 : 0;
        $male_sex_with_other_male       = ($request->has('male_sex_with_other_male'))? 1 : 0;
        $sex_worker_client              = ($request->has('sex_worker_client'))? 1 : 0;
        $sex_worker                     = ($request->has('sex_worker'))? 1 : 0;
        $child_of_hiv_infected_mother   = ($request->has('child_of_hiv_infected_mother'))? 1 : 0;

        $reason_for_not_testing         = ($request->input('tested_for_hiv') == 0)? $request->input('reason_for_not_testing') : null;
        $hiv_positive_date_retest       = ($request->input('positive_for_hiv') == 0)? date('Y-m-d', strtotime($request->input('hiv_positive_date_retest'))) : null;
        #echo $ui_code; exit;
        $vct = new VCT;
        $vct->ui_code       = strtoupper($ui_code);
        $vct->vct_date      = date('Y-m-d', strtotime($request->input('vct_date')));
        $vct->first_name    = $request->input('first_name');
        $vct->middle_name   = $request->input('middle_name');
        $vct->last_name     = $request->input('last_name');
        $vct->gender        = $request->input('sex');
        $vct->birth_date    = date('Y-m-d', strtotime($request->input('birth_date')));

        $vct->mother_first_name     = $request->input('mother_first_name');
        $vct->mother_middle_name    = $request->input('mother_middle_name');
        $vct->mother_last_name      = $request->input('mother_last_name');

        $vct->street    = $request->input('street');
        $vct->sitio     = $request->input('purok');
        $vct->brgy      = $request->input('barangay');
        $vct->city      = $request->input('city');
        $vct->contact_number = $request->input('contact_number');

        $vct->blood_transfusion     = $blood_transfusion;
        $vct->injecting_drug_user   = $injecting_drug_user;
        $vct->substance_abuse       = $substance_abuse;
        $vct->occupational_exposure = $occupational_exposure;

        $vct->provided_post_exposure_prophylaxis = $provided_post_exposure_prophylaxis;
        $vct->sexually_transmitted_infections    = $sexually_transmitted_infections;

        $vct->multiple_sexual_partners  = $multiple_sexual_partners;
        $vct->male_sex_with_other_male  = $male_sex_with_other_male;
        $vct->sex_worker_client         = $sex_worker_client;
        $vct->sex_worker                = $sex_worker;

        $vct->child_of_hiv_infected_mother = $child_of_hiv_infected_mother;

        $vct->hiv_tested             = $request->input('tested_for_hiv');
        $vct->reason_for_not_testing = $reason_for_not_testing;

        $vct->hiv_positive              = $request->input('positive_for_hiv');
        $vct->hiv_positive_date_retest  = $hiv_positive_date_retest;
        
        $vct->provide_post_test_counselling_and_hiv_result = $request->input('provide_post_test_counselling_and_hiv_result');

        $vct->save();

        return redirect()->route('vct_create')->with('status', 'Patient record successfully created!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $action  = route('vct_update', $id);
        $patient = VCT::where('id', $id)->first();
        $ui_code = explode('-', $patient->ui_code);

        $vct_date   = date('F d, Y', strtotime($patient->vct_date));

        $first_name     = $patient->first_name;
        $middle_name    = $patient->middle_name;
        $last_name      = $patient->last_name;

        $uic_mother_name = $ui_code[0];
        $uic_father_name = $ui_code[1];
        $uic_birth_order = $ui_code[2];

        $mother_first_name  = $patient->mother_first_name;
        $mother_middle_name = $patient->mother_middle_name;
        $mother_last_name   = $patient->mother_last_name;

        $gender     = $patient->gender;
        $birth_date = date('F d, Y', strtotime($patient->birth_date));
        $age        = \Carbon\Carbon::parse($patient->birth_date)->age;

        $street = $patient->street;
        $purok  = $patient->sitio;
        $city   = $patient->city;
        $barangay = $patient->brgy;
        $contact_number = $patient->contact_number;

        $blood_transfusion      = $patient->blood_transfusion;
        $injecting_drug_user    = $patient->injecting_drug_user;
        $substance_abuse        = $patient->substance_abuse;
        $occupational_exposure  = $patient->occupational_exposure;

        $provided_post_exposure_prophylaxis = $patient->provided_post_exposure_prophylaxis;
        $sexually_transmitted_infections    = $patient->sexually_transmitted_infections;

        $multiple_sexual_partners = $patient->multiple_sexual_partners;
        $male_sex_with_other_male = $patient->male_sex_with_other_male;

        $sex_worker_client  = $patient->sex_worker_client;
        $sex_worker         = $patient->sex_worker;

        $child_of_hiv_infected_mother = $patient->child_of_hiv_infected_mother;

        $tested_for_hiv         = $patient->hiv_tested;
        $reason_for_not_testing = $patient->reason_for_not_testing;

        $positive_for_hiv           = $patient->hiv_positive;
        $hiv_positive_date_retest   = date('F d, Y', strtotime($patient->hiv_positive_date_retest));

        $provide_post_test_counselling_and_hiv_result = $patient->provide_post_test_counselling_and_hiv_result;

        $data = compact('action', 'vct_date', 

            'first_name', 'middle_name', 'last_name', 

            'uic_mother_name', 'uic_father_name', 'uic_birth_order', 

            'mother_first_name', 'mother_middle_name', 'mother_last_name', 

            'gender', 'birth_date', 'age',

            'street', 'purok', 'city', 'barangay', 'contact_number', 

            'blood_transfusion', 'injecting_drug_user', 'substance_abuse', 'occupational_exposure', 

            'provided_post_exposure_prophylaxis', 'sexually_transmitted_infections', 

            'multiple_sexual_partners', 'male_sex_with_other_male', 

            'sex_worker_client', 'sex_worker', 'child_of_hiv_infected_mother',

            'tested_for_hiv', 'reason_for_not_testing', 

            'positive_for_hiv', 'hiv_positive_date_retest', 

            'provide_post_test_counselling_and_hiv_result', 
            'id'
            );
        
        return view('hact.enrolment.vct.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\VCTUpdateRequest $request, $id)
    {
        //
        #dd($request);
        $ui_code                = $request->input('uic_mother_name') . '-' . $request->input('uic_father_name') . '-' . $request->input('uic_birth_order');

        $blood_transfusion      = ($request->has('blood_transfusion'))? 1 : 0;
        $injecting_drug_user    = ($request->has('injecting_drug_user'))? 1 : 0;
        $substance_abuse        = ($request->has('substance_abuse'))? 1 : 0;
        $occupational_exposure  = ($request->has('occupational_exposure'))? 1 : 0;
        $provided_post_exposure_prophylaxis = ($request->has('provided_post_exposure_prophylaxis'))? 1 : 0;

        $sexually_transmitted_infections    = ($request->has('sexually_transmitted_infections'))? 1 : 0;
        $multiple_sexual_partners       = ($request->has('multiple_sexual_partners'))? 1 : 0;
        $male_sex_with_other_male       = ($request->has('male_sex_with_other_male'))? 1 : 0;
        $sex_worker_client              = ($request->has('sex_worker_client'))? 1 : 0;
        $sex_worker                     = ($request->has('sex_worker'))? 1 : 0;
        $child_of_hiv_infected_mother   = ($request->has('child_of_hiv_infected_mother'))? 1 : 0;

        $reason_for_not_testing         = ($request->input('tested_for_hiv') == 0)? $request->input('reason_for_not_testing') : null;
        $hiv_positive_date_retest       = ($request->input('positive_for_hiv') == 0)? date('Y-m-d', strtotime($request->input('hiv_positive_date_retest'))) : null;
        
        $vct = VCT::find($id);
        $vct->ui_code       = strtoupper($ui_code);
        $vct->vct_date      = date('Y-m-d', strtotime($request->input('vct_date')));
        $vct->first_name    = $request->input('first_name');
        $vct->middle_name   = $request->input('middle_name');
        $vct->last_name     = $request->input('last_name');
        $vct->gender        = $request->input('sex');
        $vct->birth_date    = date('Y-m-d', strtotime($request->input('birth_date')));

        $vct->mother_first_name     = $request->input('mother_first_name');
        $vct->mother_middle_name    = $request->input('mother_middle_name');
        $vct->mother_last_name      = $request->input('mother_last_name');

        $vct->street    = $request->input('street');
        $vct->sitio     = $request->input('purok');
        $vct->brgy      = $request->input('barangay');
        $vct->city      = $request->input('city');
        $vct->contact_number = $request->input('contact_number');

        $vct->blood_transfusion     = $blood_transfusion;
        $vct->injecting_drug_user   = $injecting_drug_user;
        $vct->substance_abuse       = $substance_abuse;
        $vct->occupational_exposure = $occupational_exposure;

        $vct->provided_post_exposure_prophylaxis = $provided_post_exposure_prophylaxis;
        $vct->sexually_transmitted_infections    = $sexually_transmitted_infections;

        $vct->multiple_sexual_partners  = $multiple_sexual_partners;
        $vct->male_sex_with_other_male  = $male_sex_with_other_male;
        $vct->sex_worker_client         = $sex_worker_client;
        $vct->sex_worker                = $sex_worker;

        $vct->child_of_hiv_infected_mother = $child_of_hiv_infected_mother;

        $vct->hiv_tested             = $request->input('tested_for_hiv');
        $vct->reason_for_not_testing = $reason_for_not_testing;

        $vct->hiv_positive              = $request->input('positive_for_hiv');
        $vct->hiv_positive_date_retest  = $hiv_positive_date_retest;
        
        $vct->provide_post_test_counselling_and_hiv_result = $request->input('provide_post_test_counselling_and_hiv_result');

        $vct->save();

        return redirect()->route('vct_edit', [$id])->with('status', 'Patient record successfully updated!');
    }

    
    public function vct_order_by($request)
    {
        if($request->has('order_by'))
        {
            $order_by = trim($request->input('order_by'));

            if($order_by == 'patient_name')
            {
                return 'last_name';
            }
            elseif($order_by == 'ui_code')
            {
                return 'ui_code';
            }
            elseif($order_by == 'gender')
            {
                return 'gender';
            }
            elseif($order_by == 'vct_date')
            {
                return 'vct_date';
            } 
             elseif($order_by == 'hiv_positive')
            {
                return 'hiv_positive';
            }                   
            else
            {
                return 'last_name';
            }
        }
        else
        {
            return 'last_name';
        }
    }

    public function sort_query($request)
    {

        if($request->has('sort'))
        {
            $sort = $request->input('sort');

            if($sort == 'ASC')
            {
                return 'ASC';
            }
            elseif($sort == 'DESC')
            {
                return 'DESC';
            }
        }
        else
        {
            return 'ASC';
        }
    }

    public function vct_link_sort($order_by, $search, $sort, $request)
    {
        $sort  = ($sort == 'DESC')? 'ASC' : 'DESC';

        if($request->has('page'))
        {
            return route('vct_index', ['search' => $search, 'order_by' => $order_by, 'sort' => $sort, 'page' => $request->input('page')]);
        }
        else
        {
            return route('vct_index', ['search' => $search, 'order_by' => $order_by, 'sort' => $sort]);
        }
    }
}
