<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\EnrolmentOpportunisticInfections;

class OpportunisticInfectionsProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $paginate   = 50;
        $search     = '';
        $order_by   = $this->opportunistic_infections_order_by($request);
        $sort       = $this->sort_query($request);

        if($request->has('search'))
        {
            $search = trim($request->input('search'));

            $patients   = EnrolmentOpportunisticInfections::whereHas('EnrolmentDemographics', function($query) use ($search){
                            $query->where('code_name', 'LIKE', '%'.$search.'%')
                                ->orWhere('saccl_code', 'LIKE', '%'.$search.'%')
                                ->orWhere('first_name', 'LIKE', '%'.$search.'%')
                                ->orWhere('last_name', 'LIKE', '%'.$search.'%');
                        })->paginate(50);
        }
        else
        {
            $patients   = EnrolmentOpportunisticInfections::paginate(50);
        }

        $ui_code_sort       = $this->opportunistic_infections_link_sort('ui_code', $search, $sort, $request);
        $code_name_sort     = $this->opportunistic_infections_link_sort('code_name', $search, $sort, $request);

        $saccl_code_sort     = $this->opportunistic_infections_link_sort('saccl_code', $search, $sort, $request);
        $last_name_sort     = $this->opportunistic_infections_link_sort('last_name', $search, $sort, $request); 
        $gender_sort        = $this->opportunistic_infections_link_sort('gender', $search, $sort, $request); 
        $pagination         = ['search' => $search, 'order_by' => $order_by, 'sort' => $sort];

        $data = compact('patients', 'search', 'code_name_sort', 'last_name_sort', 'ui_code_sort', 'pagination','saccl_code_sort');
        return view('hact.enrolment.opportunistic-infections-profile.index', $data);

    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = route('oi_profile_store');
        $search_patient  = old('search_patient');
        $demographics_id = old('demographics_id');

        $ipt = old('ipt');
        $ipt_reason = old('ipt_reason');
        $hepatitis_b = old('hepatitis_b');
        $hepatitis_c = old('hepatitis_c');
        $pneumocystis_pneumonia = old('pneumocystis_pneumonia');
        $oropharyngeal_candidiasis = old('oropharyngeal_candidiasis');
        $syphilis = old('syphilis');

        $pulmonary = old('pulmonary');
        $extrapulmonary = old('extrapulmonary');

        $sti = old('sti');
        $sti_reason = old('sti_reason');
        $others = old('others');
        $others_reason = old('others_reason');

        $site_value = old('site_value');

        $tb_regimen_value = old('tb_regimen_value');

        $drug_resistance_value = old('drug_resistance_value');
        $drug_resistance_value_specify = old('drug_resistance_value_specify');

        $treatment_outcome_value = old('treatment_outcome_value');
        $treatment_outcome_value_specify = old('treatment_outcome_value_specify');

        $data = compact('action',
            'search_patient','demographics_id', 
            'ipt','ipt_reason', 
            'hepatitis_b','hepatitis_c', 'pneumocystis_pneumonia', 'oropharyngeal_candidiasis', 'syphilis', 
            'sti','sti_reason', 'others', 'others_reason', 

            'site_value', 
            'tb_regimen_value',
            'drug_resistance_value', 'drug_resistance_value_specify', 
            'treatment_outcome_value', 'treatment_outcome_value_specify'
            );

        return view('hact.enrolment.opportunistic-infections-profile.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\OpportunisticInfectionsRequest $request)
    {
        //
        $row = new EnrolmentOpportunisticInfections;
        $row->demographics_id               = $request->input('demographics_id');

        $row->started_with_ipt              = $request->input('ipt');
        $row->no_ipt_reason                 = $request->input('ipt_reason');

        $row->hepatitis_b                   = $request->input('hepatitis_b');
        $row->hepatitis_c                   = $request->input('hepatitis_c');
        $row->pneumocystis_pneumonia        = $request->input('pneumocystis_pneumonia');
        $row->oropharyngeal_candidiasis     = $request->input('oropharyngeal_candidiasis');
        $row->syphilis                      = $request->input('syphilis');
        $row->stis                          = $request->input('stis');
        $row->stis_specify                  = $request->input('sti_reason');
        $row->others                        = $request->input('others');
        $row->others_specify                = $request->input('others_specify');
        
        $row->site_value                    = $request->input('site_value');
        $row->tb_regimen_value              = $request->input('tb_regimen_value');
        
        $row->drug_resistance_value         = $request->input('drug_resistance_value');
        $row->drug_resistance_value_specify = $request->input('drug_resistance_value_specify');
        
        $row->treatment_outcome_value       = $request->input('treatment_outcome_value');
        $row->treatment_outcome_value_specify = $request->input('treatment_outcome_value_specify');

        $row->save();

        return redirect()->route('oi_profile_create')->with('status', 'Patient record successfully created!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $action = route('oi_profile_update', $id);
        $row = EnrolmentOpportunisticInfections::where('id', $id)->first();

        $search_patient = $row->EnrolmentDemographics->full_name2;
        $demographics_id= $row->demographics_id;

        $ipt            = $row->started_with_ipt;
        $ipt_reason     = $row->no_ipt_reason;
        $hepatitis_b    = $row->hepatitis_b;
        $hepatitis_c    = $row->hepatitis_c;
        $pneumocystis_pneumonia     = $row->pneumocystis_pneumonia;
        $oropharyngeal_candidiasis  = $row->oropharyngeal_candidiasis;
        $syphilis       = $row->syphilis;

        $sti            = $row->stis;
        $sti_reason     = $row->stis_specify;
        $others         = $row->others;
        $others_reason  = $row->others_specify;

        $site_value     = $row->site_value;
        $tb_regimen_value = $row->tb_regimen_value;

        $drug_resistance_value = $row->drug_resistance_value;
        $drug_resistance_value_specify = $row->drug_resistance_value_specify;

        $treatment_outcome_value = $row->treatment_outcome_value;
        $treatment_outcome_value_specify = $row->treatment_outcome_value_specify;

        $data = compact('action',
            'search_patient','demographics_id', 
            'ipt','ipt_reason', 
            'hepatitis_b','hepatitis_c', 'pneumocystis_pneumonia', 'oropharyngeal_candidiasis', 'syphilis', 
            'sti','sti_reason', 'others', 'others_reason', 

            'site_value', 
            'tb_regimen_value',
            'drug_resistance_value', 'drug_resistance_value_specify', 
            'treatment_outcome_value', 'treatment_outcome_value_specify'
            );

        return view('hact.enrolment.opportunistic-infections-profile.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\OpportunisticInfectionsRequest $request, $id)
    {
        //dd($request->all());
        //
        $row = EnrolmentOpportunisticInfections::find($id);
        $row->demographics_id           = $request->input('demographics_id');

        $row->started_with_ipt          = $request->input('ipt');
        $row->no_ipt_reason             = $request->input('ipt_reason');

        $row->hepatitis_b               = $request->input('hepatitis_b');
        $row->hepatitis_c               = $request->input('hepatitis_c');
        $row->pneumocystis_pneumonia    = $request->input('pneumocystis_pneumonia');
        $row->oropharyngeal_candidiasis = $request->input('oropharyngeal_candidiasis');
        $row->syphilis                  = $request->input('syphilis');
        $row->stis                       = $request->input('sti');
        $row->stis_specify              = $request->input('sti_reason');
        $row->others                    = $request->input('others');
        $row->others_specify            = $request->input('others_reason');
        
        $row->site_value                    = $request->input('site_value');
        
        $row->tb_regimen_value              = $request->input('tb_regimen_value');
        
        $row->drug_resistance_value         = $request->input('drug_resistance_value');
        $row->drug_resistance_value_specify = $request->input('drug_resistance_value_specify');
        
        $row->treatment_outcome_value       = $request->input('treatment_outcome_value');
        $row->treatment_outcome_value_specify = $request->input('treatment_outcome_value_specify');

        $row->save();

        return redirect()->route('oi_profile_edit', $id)->with('status', 'Patient record successfully updated!');
    }
    
    public function opportunistic_infections_order_by($request)
    {
        if($request->has('order_by'))
        {
            $order_by = trim($request->input('order_by'));

            if($order_by == 'last_name')
            {
                return 'last_name';
            }
            elseif($order_by == 'code_name')
            {
                return 'code_name';
            }
            elseif($order_by == 'ui_code')
            {
                return 'ui_code';
            }
            elseif($order_by == 'saccl_code')
            {
                return 'saccl_code';
            }
            else
            {
                return 'last_name';
            }
        }
        else
        {
            return 'last_name';
        }
    }

    public function sort_query($request)
    {

        if($request->has('sort'))
        {
            $sort = $request->input('sort');

            if($sort == 'ASC')
            {
                return 'ASC';
            }
            elseif($sort == 'DESC')
            {
                return 'DESC';
            }
        }
        else
        {
            return 'ASC';
        }
    }

    public function opportunistic_infections_link_sort($order_by, $search, $sort, $request)
    {
        $sort  = ($sort == 'DESC')? 'ASC' : 'DESC';

        if($request->has('page'))
        {
            return route('oi_profile_index', ['search' => $search, 'order_by' => $order_by, 'sort' => $sort, 'page' => $request->input('page')]);
        }
        else
        {
            return route('oi_profile_index', ['search' => $search, 'order_by' => $order_by, 'sort' => $sort]);
        }
    }
}
