<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\VCT;
use App\EnrolmentDemographics;
use App\EnrolmentClinicalAndImmunologic;
use App\EnrolmentOpportunisticInfections;
use App\EnrolmentMedication;

use App\ARV;
use App\ARVRefill;
use App\Checkup;
use App\Death;

class DashboardController extends Controller
{
    public function index()
    {
        $vct = VCT::orderBy('id', 'DESC')->take(10)->get();

        $demographics = EnrolmentDemographics::orderBy('id', 'DESC')->take(10)->get();
        $clinical_and_immunologic = EnrolmentClinicalAndImmunologic::orderBy('id', 'DESC')->take(10)->get();
        $opportunistic_infection = EnrolmentOpportunisticInfections::orderBy('id', 'DESC')->take(10)->get();
        $medication = EnrolmentMedication::orderBy('id', 'DESC')->take(10)->get();

        $arv = ARV::orderBy('id', 'DESC')->take(10)->get();
        $arv_refill = ARVRefill::orderBy('id', 'DESC')->take(10)->get();
        $checkup = Checkup::orderBy('id', 'DESC')->take(10)->get();
        $death = Death::orderBy('id', 'DESC')->take(10)->get();

        $data = compact('vct', 
        				'demographics', 'clinical_and_immunologic', 'opportunistic_infection', 'medication', 
        				'arv', 'arv_refill', 'checkup', 'death' );

        return view('hact.home', $data);
    }
}
