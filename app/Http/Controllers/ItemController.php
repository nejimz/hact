<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Item;
use App\ItemReceive;
use Auth;
use DB;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $paginate   = 30;
        $search     = '';
        $order_by   = $this->item_order_by($request);
        $sort       = $this->sort_query($request);
    
        if($request->has('search'))
        {
            $search = trim($request->input('search'));

            $items = Item::searchItemList($search)
                    ->orderBy($order_by, $sort)
                    ->paginate($paginate);
        }
        else
        {
            $items = Item::orderBy($order_by, $sort)
                    ->paginate($paginate);
        }

        $item_name_sort             = $this->item_link_sort('item_name', $search, $sort, $request);
        $item_code_sort             = $this->item_link_sort('item_code', $search, $sort, $request); 
        $lot_number_sort            = $this->item_link_sort('lot_number', $search, $sort, $request); 
        $quantity_per_bottle_sort   = $this->item_link_sort('quantity_per_bottle', $search, $sort, $request); 
        $expiration_date_sort       = $this->item_link_sort('expiration_date', $search, $sort, $request); 
        $pagination         = ['search' => $search, 'order_by' => $order_by, 'sort' => $sort];

        $data = compact('items', 'search', 'item_name_sort', 'item_code_sort', 'lot_number_sort', 'quantity_per_bottle_sort', 'expiration_date_sort', 'pagination');
    
        return view('hact.item.index', $data);
    }
    
    public function item_order_by($request)
    {
        if($request->has('order_by'))
        {
            $order_by = trim($request->order_by);

            if($order_by == 'item_name')
            {
                return 'item_name';
            }
            elseif($order_by == 'item_code')
            {
                return 'item_code';
            }
            elseif($order_by == 'lot_number')
            {
                return 'lot_number';
            }
            elseif($order_by == 'expiration_date')
            {
                return 'expiration_date';
            }
            elseif($order_by == 'quantity_per_bottle')
            {
                return 'quantity_per_bottle';
            }            
            else
            {
                return 'item_name';
            }
        }
        else
        {
            return 'item_name';
        }
    }

    public function sort_query($request)
    {

        if($request->has('sort'))
        {
            $sort = $request->input('sort');

            if($sort == 'ASC')
            {
                return 'ASC';
            }
            elseif($sort == 'DESC')
            {
                return 'DESC';
            }
        }
        else
        {
            return 'ASC';
        }
    }

    public function item_link_sort($order_by, $search, $sort, $request)
    {
        $sort  = ($sort == 'DESC')? 'ASC' : 'DESC';

        if($request->has('page'))
        {
            return route('item_list', ['search' => $search, 'order_by' => $order_by, 'sort' => $sort, 'page' => $request->input('page')]);
        }
        else
        {
            return route('item_list', ['search' => $search, 'order_by' => $order_by, 'sort' => $sort]);
        }
    }

    public function search_item(Request $request)
    {
        //
        $search = '%' . trim($request->input('search_item')) . '%';

        $items = Item::select(DB::raw('CONCAT(item_name) AS item_name_raw'), 'id')
                    ->where('item_name', 'LIKE', $search)
                    ->take(20)
                    ->lists('item_name_raw', 'id');
        //return Response::json($products)->setCallback(Input::get('callback'));
        return response()->json($items);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = route('item_store');    
        $item_name = old('item_name');
        $item_code = old('item_code');
        $lot_number = old('lot_number');
        $quantity_per_bottle = old('quantity_per_bottle');
        $expiration_date = old('expiration_date');
        $data = compact('action','item_name','item_code','lot_number','quantity_per_bottle','expiration_date');

        return view('hact.item.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\ItemStoreRequest $request)
    {
        $user = User::where('id',Auth::user()->id)->first();
        $item = new Item;
        $item->item_name = $request->item_name;
        $item->item_code = $request->item_code;
        $item->lot_number = $request->lot_number;
        $item->expiration_date = $request->expiration_date;
        $item->user_id = Auth::user()->id;
        $item->quantity_per_bottle = $request->quantity_per_bottle;
        //$item->user()->associate($user);
        $item->save();

        $item_receive = new ItemReceive;
        $item_receive->item_id = $item->id;
        $item_receive->user_id = Auth::user()->id;
        $item_receive->quantity = $item->quantity_per_bottle;
        //$item_receive->item()->associate($item);
        //$item_receive->user()->associate($user);
        $item_receive->save();
        return redirect()->route('item_create')->with('status', 'Item successfully added!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::where('id','=',$id)->first();
        $action = route('item_update',$id);    
        $item_name = $item->item_name;
        $item_code = $item->item_code;
        $lot_number = $item->lot_number;
        $quantity_per_bottle = $item->quantity_per_bottle;
        $expiration_date = $item->expiration_date;
        $data = compact('action','item_name','item_code','lot_number','quantity_per_bottle','expiration_date');

        return view('hact.item.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\ItemUpdateRequest $request, $id)
    {
        $item = Item::where('id','=',$id)->first();
        $item->item_name = $request->item_name;
        $item->item_code = $request->item_code;
        $item->lot_number = $request->lot_number;
        $item->expiration_date = $request->expiration_date;
        //$item->user_id = $request->user_id;
        $item->quantity_per_bottle = $request->quantity_per_bottle;
        $item->save();

        return redirect()->route('item_list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::where('id','=',$id)->first();
        $item->delete();

        return redirect()->route('item_list')->with('status', 'Item deleted successfully!');;   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        //
        $search = '%' . trim($request->input('item_search')). '%';

        $item = Item::select(DB::raw('CONCAT(item_name, \' - \', lot_number) AS item_name'), 'id')
                ->where('item_code', 'LIKE', $search)
                ->orWhere('item_name', 'LIKE', $search )
                ->orWhere('lot_number', 'LIKE', $search )
                ->take(20)->lists('item_name', 'id');

        //return Response::json($products)->setCallback(Input::get('callback'));
        return response()->json($item);
    }
}
