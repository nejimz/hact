<?php

namespace App\Http\Controllers;
use Carbon\Carbon;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\Item;
use App\ItemReceive;
use App\User;

class ItemReceiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $paginate       = 50;
        $search         = '';
        #$order_by       = $this->item_receive_order_by($request);
        #$sort           = $this->sort_query($request);
        $action         = route('receive_store');
        $item_id        = old('id');
        $search_item    = old('search_item');
        $quantity       = old('quantity');
        $transaction_date = old('transaction_date');
    
        if($request->has('search'))
        {
            $search = trim($request->input('search'));
            $item_receives = ItemReceive::join('item','item_receive.item_id','=','item.id')
                    ->select('*','item_receive.id as receive_id')
                    ->searchItemReceiveList($search)
                    ->orderBy('transaction_date', 'DESC')
                    ->paginate($paginate);
        }
        else
        {
            $item_receives = ItemReceive::join('item','item_receive.item_id','=','item.id')
                    ->select('*','item_receive.id as receive_id')
                    ->orderBy('transaction_date', 'DESC')
                    ->paginate($paginate);
        }
        
        /*$item_name_sort         = $this->item_receives_link_sort('item_name', $search, $sort, $request); 
        $quantity_sort          = $this->item_receives_link_sort('quantity', $search, $sort, $request); 
        $transaction_date_sort  = $this->item_receives_link_sort('transaction_date', $search, $sort, $request);
        $pagination             = ['search' => $search, 'order_by' => $order_by, 'sort' => $sort]; */
        $pagination             = ['search' => $search];

        #$data = compact('action', 'quantity', 'transaction_date', 'search_item', 'item_id');

        /*$data = compact('action', 'quantity', 'transaction_date', 'search_item', 'item_id', 
            'item_receives', 'search', 'item_name_sort', 'quantity_sort', 'transaction_date_sort', 'pagination');*/
        $data = compact('action', 'quantity', 'transaction_date', 'search_item', 'item_id', 'item_receives', 'search', 'pagination');
    
        return view('hact.item.receive.index', $data);

    }

    public function create(Request $request)
    {
        //
       if($request->has('id')){
            $item_id = $request->input('id');
        }else{
            $item_id = old('id');
        }
        if($request->has('search_item')){
            $search_item  = $request->input('search_item');
        }else{
            $search_item  = old('search_item');
        }

        $action = route('receive_store');
        $quantity = old('quantity');
        $transaction_date = old('transaction_date');
        $data = compact('action','quantity','transaction_date','search_item','item_id');
        return view('hact.item.receive.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\ItemReceiveRequest $request)
    {
        //
        #$item = Item::where('id',$request->item_id)->first();
        #$user = User::where('id',Auth::user()->id)->first();
        $item_receive = new ItemReceive;
        $item_receive->item_id          = $request->item_id;
        $item_receive->quantity         = $request->quantity;
        #$item_receive->transaction_date = Carbon::now()->toDateTimeString();
        $item_receive->user_id          = Auth::user()->id;
        #$item_receive->item()->associate($item);
        #$item_receive->user()->associate($user);
        $item_receive->save();

        return redirect()->route('receive_index')->with('status', 'Item successfully added!');
    }

    public function edit($id)
    {
        //
        $item_receive = ItemReceive::where('id','=',$id)->first();
        $item = Item::where('id',$item_receive->item_id)->first();
        $search_item = $item->item_name;
        $item_id = $item->id;
        $quantity = $item_receive->quantity;
        $action = route('receive_update',$id);
        $data = compact('action','quantity','search_item','item_id');
        return view('hact.item.receive.create', $data);
    }

    public function update(Requests\ItemReceiveRequest $request, $id)
    {   
        $item_receive = ItemReceive::where('id',$id)->first();
        $user = User::where('id',Auth::user()->id)->first();
        $item = Item::where('id',$request->item_id)->first();

        $item_receive->item_id          = $request->item_id;
        $item_receive->quantity         = $request->quantity;
        $item_receive->transaction_date = Carbon::now()->toDateString();
        $item_receive->user_id          = Auth::user()->id;
        $item_receive->item()->associate($item);
        $item_receive->user()->associate($user);
        $item_receive->save();


        return redirect()->route('receive_index')->with('status', 'Storage Report successfully updated!');
    }

    public function destroy($id)
    {
        $item_Receive = ItemReceive::where('id','=',$id)->first();
        $item_Receive->delete();

        return redirect()->route('receive_index')->with('status', 'Report successfully deleted');
    }
    
    public function item_receive_order_by($request)
    {
        if($request->has('order_by'))
        {
            $order_by = trim($request->has('order_by'));

            if($order_by == 'item_name')
            {
                return 'item.item_name';
            }
            elseif($order_by == 'transaction_date')
            {
                return 'transaction_date';
            }
            elseif($order_by == 'quantity')
            {
                return 'quantity';
            }            
            else
            {
                return 'transaction_date';
            }
        }
        else
        {
            return 'transaction_date';
        }

    }

    public function sort_query($request)
    {

        if($request->has('sort'))
        {
            $sort = $request->input('sort');

            if($sort == 'ASC')
            {
                return 'ASC';
            }
            elseif($sort == 'DESC')
            {
                return 'DESC';
            }
        }
        else
        {
            return 'ASC';
        }
    }

    public function item_receives_link_sort($order_by, $search, $sort, $request)
    {
        $sort  = ($sort == 'DESC')? 'ASC' : 'DESC';

        if($request->has('page'))
        {
            return route('receive_index', ['search' => $search, 'order_by' => $order_by, 'sort' => $sort, 'page' => $request->input('page')]);
        }
        else
        {
            return route('receive_index', ['search' => $search, 'order_by' => $order_by, 'sort' => $sort]);
        }
    }
}
