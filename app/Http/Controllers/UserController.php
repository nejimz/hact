<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\User;
use App\UserRole;
use DB;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $paginate   = 1;
        $search     = '';
        $order_by   = $this->user_order_by($request);
        $sort       = $this->sort_query($request);

        if($request->has('search'))
        {
            $search = trim($request->input('search'));

            $users = User::searchUserList($search)
                    ->orderBy($order_by, $sort)
                    ->paginate($paginate);
        }
        else
        {
            $users = User::orderBy($order_by, $sort)
                    ->paginate($paginate);
        }

        $first_name_sort    = $this->user_link_sort('first_name', $search, $sort, $request);
        $last_name_sort     = $this->user_link_sort('last_name', $search, $sort, $request);
        $email_sort         = $this->user_link_sort('email', $search, $sort, $request);
        $role_sort          = $this->user_link_sort('role', $search, $sort, $request);
        $pagination         = ['search' => $search, 'order_by' => $order_by, 'sort' => $sort];

        $data = compact('users', 'search', 'first_name_sort', 'last_name_sort', 'email_sort', 'role_sort', 'pagination');

        return view('hact.user.index', $data);
    }

    public function user_order_by($request)
    {
        if($request->has('order_by'))
        {
            $order_by = trim($request->has('order_by'));

            if($order_by == 'first_name')
            {
                return 'first_name';
            }
            elseif($order_by == 'last_name')
            {
                return 'last_name';
            }
            elseif($order_by == 'email')
            {
                return 'email';
            }
            elseif($order_by == 'role')
            {
                return 'role_id';
            }
            else
            {
                return 'first_name';
            }
        }
        else
        {
            return 'first_name';
        }
    }

    public function sort_query($request)
    {

        if($request->has('sort'))
        {
            $sort = $request->input('sort');

            if($sort == 'ASC')
            {
                return 'ASC';
            }
            elseif($sort == 'DESC')
            {
                return 'DESC';
            }
        }
        else
        {
            return 'ASC';
        }
    }

    public function user_link_sort($order_by, $search, $sort, $request)
    {
        $sort  = ($sort == 'DESC')? 'ASC' : 'DESC';

        if($request->has('page'))
        {
            return route('user_index', ['search' => $search, 'order_by' => $order_by, 'sort' => $sort, 'page' => $request->input('page')]);
        }
        else
        {
            return route('user_index', ['search' => $search, 'order_by' => $order_by, 'sort' => $sort]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $id         = old('id');
        $email      = old('email');
        $first_name = old('first_name');
        $last_name  = old('last_name');
        $role_id    = old('role');
        $roles      = UserRole::all();
        $route     = route('user_store');

        $data = compact('route', 'id', 'roles', 'email', 'first_name', 'last_name', 'role_id');
        
        return view('hact.user.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\UserStoreRequest $request)
    {
        //
        User::create([
            'first_name'    => $request->input('first_name'),
            'last_name'     => $request->input('last_name'),
            'email'         => $request->input('email'),
            'password'      => bcrypt('p@ssw0rd'),
            'role_id'       => $request->input('role')
        ]);

        return redirect()->route('user_index')->with('status', 'User successfully save!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::find($id);

        $email      = $user->email;
        $first_name = $user->first_name;
        $last_name  = $user->last_name;
        $role_id    = $user->role_id;

        $roles      = UserRole::all();
        $route      = route('user_update', [$id]);

        $data = compact('route', 'id', 'roles', 'email', 'first_name', 'last_name', 'role_id');

        return view('hact.user.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UserUpdateRequest $request, $id)
    {
        //
        User::where('id', $id)
        ->update([
            'first_name'    => $request->input('first_name'),
            'last_name'     => $request->input('last_name'),
            'email'         => $request->input('email'),
            'password'      => bcrypt('p@ssw0rd'),
            'role_id'       => $request->input('role')
        ]);

        return redirect()->route('user_edit', [$id])->with('status', 'User successfully save!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reset_generated_password($id)
    {
        //
        $password = str_random(20);

        User::where('id', $id)
        ->update([
            'password'  => bcrypt($password)
        ]);

        return response()->json(['password'=>$password]);
    }

    public function reset_password()
    {
        return view('auth.reset_password');
    }

    public function password_reset(Requests\PasswordResetRequest $request)
    {
        $password = $request->input('password');

        User::where('id', Auth::user()->id)
        ->update([
            'password'  => bcrypt($password)
        ]);

        return redirect()->route('password_reset')->with('status', 'Password Successfully Reset!');
    }

    public function search_physician(Request $request)
    {
        //
        $search = '%' . trim($request->input('search_physician')) . '%';

        $patients = User::select(DB::raw('CONCAT(last_name, \', \', first_name) AS full_name_raw'), 'id')
                    ->where('role_id', 1)
                    ->where(function($query) use ($search){
                        $query->where('first_name', 'LIKE', $search)->orWhere('last_name', 'LIKE', $search);
                    })
                    ->take(20)
                    ->lists('full_name_raw', 'id');
        //return Response::json($products)->setCallback(Input::get('callback'));
        return response()->json($patients);
    }
}
