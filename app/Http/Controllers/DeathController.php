<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Death;


class DeathController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $search = '';
        $patients = Death::whereHas('EnrolmentDemographics', function($query) use ($search){
            $query->where('code_name', 'LIKE', '%'.$search.'%')
                    ->orWhere('saccl_code', 'LIKE', '%'.$search.'%')
                    ->orWhere('first_name', 'LIKE', '%'.$search.'%')
                    ->orWhere('last_name', 'LIKE', '%'.$search.'%');
        })->paginate(50);

        $data = compact('patients', 'search');

        return view('hact.event.death.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $action = route('death_store');

        $demographics_id = old('demographics_id');
        $search_patient  = old('search_patient');
        $death_date      = old('death_date');
        $death_cause     = old('death_cause');
        $final_diagnosis = old('final_diagnosis');

        $tuberculosis               = old('tuberculosis');
        $pneumocystic_pneumonia     = old('pneumocystic_pneumonia');
        $cryptococcal_meningitis    = old('cryptococcal_meningitis');
        $cytomegalovirus            = old('cytomegalovirus');
        $candidiasis                = old('candidiasis');
        $toxoplasmosis              = old('toxoplasmosis');
        $none                       = old('none');
        $other                      = old('other');
        $cause_death                = old('cause_death');

        $data = compact( 'action', 'demographics_id', 'search_patient', 
                         'death_date', 'death_cause', 'final_diagnosis', 
                         'tuberculosis', 'pneumocystic_pneumonia', 'cryptococcal_meningitis', 'cytomegalovirus', 
                         'candidiasis', 'toxoplasmosis', 'none', 'other','cause_death' );

        return view('hact.event.death.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Requests\DeathRequest $request)
    {
        $row = new Death;
        $row->demographics_id   = $request->input('demographics_id');
        $row->death_date        = $request->input('death_date');
        $row->death_cause       = $request->input('death_cause');
        $row->final_diagnosis   = $request->input('final_diagnosis');

        $row->tuberculosis              = $request->input('tuberculosis');
        $row->pneumocystic_pneumonia    = $request->input('pneumocystic_pneumonia');
        $row->cryptococcal_meningitis   = $request->input('cryptococcal_meningitis');
        $row->cytomegalovirus           = $request->input('cytomegalovirus');
        $row->candidiasis               = $request->input('candidiasis');
        $row->toxoplasmosis             = $request->input('toxoplasmosis');
        $row->none                      = $request->input('none');
        $row->other                     = $request->input('other');
        $row->save();

        return redirect()->route('death_create')->with('status','Patient record successfully created!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $action = route('death_update', $id);

        $row = Death::where('id', $id)->first();

        $demographics_id = $row->demographics_id;
        $search_patient  = $row->EnrolmentDemographics->full_name2;
        $death_date      = $row->death_dateformat;
        $death_cause     = $row->death_cause;
        $final_diagnosis = $row->final_diagnosis;

        $tuberculosis               = $row->tuberculosis;
        $pneumocystic_pneumonia     = $row->pneumocystic_pneumonia;
        $cryptococcal_meningitis    = $row->cryptococcal_meningitis;
        $cytomegalovirus            = $row->cytomegalovirus;
        $candidiasis                = $row->candidiasis;
        $toxoplasmosis              = $row->toxoplasmosis;
        $none                       = $row->none;
        $other                      = $row->other;


        $data = compact( 'action', 'demographics_id', 'search_patient', 
                         'death_date', 'death_cause', 'final_diagnosis', 
                         'tuberculosis', 'pneumocystic_pneumonia', 'cryptococcal_meningitis', 'cytomegalovirus', 
                         'candidiasis', 'toxoplasmosis', 'none', 'other' );

        return view('hact.event.death.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Requests\DeathUpdateRequest $request, $id)
    {
        $row = Death::find($id);
        $row->demographics_id   = $request->input('demographics_id');
        $row->death_date        = $request->input('death_date');
        $row->death_cause       = $request->input('death_cause');
        $row->final_diagnosis   = $request->input('final_diagnosis');

        $row->tuberculosis              = $request->input('tuberculosis');
        $row->pneumocystic_pneumonia    = $request->input('pneumocystic_pneumonia');
        $row->cryptococcal_meningitis   = $request->input('cryptococcal_meningitis');
        $row->cytomegalovirus           = $request->input('cytomegalovirus');
        $row->candidiasis               = $request->input('candidiasis');
        $row->toxoplasmosis             = $request->input('toxoplasmosis');
        $row->none                      = $request->input('none');
        $row->other                     = $request->input('other');
        $row->save();

        return redirect()->route('death_edit', $id)->with('status','Patient record successfully created!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
