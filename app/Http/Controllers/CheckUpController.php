<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Checkup;
use App\EnrolmentDemographics;

class CheckUpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $patients = Checkup::all();
        $search = '';
        $number_per_page = 10;

        if($request->has('search'))
        {
            $search = trim($request->input('search'));

            $patients = Checkup::whereHas('EnrolmentDemographics', function($query) use ($search){
                $query->where('code_name', 'LIKE', '%'.$search.'%')
                        ->orWhere('saccl_code', 'LIKE', '%'.$search.'%')
                        ->orWhere('first_name', 'LIKE', '%'.$search.'%')
                        ->orWhere('last_name', 'LIKE', '%'.$search.'%')
                        ->orderBy('ui_code', 'DESC');
            })->paginate($number_per_page);
        }
        else
        {
            $patients = Checkup::paginate($number_per_page);
        }
        $data = compact('patients','search');
        return view('hact.event.check-up.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $action             = route('checkup_store');
        $demographics_id    = old('demographics_id');
        $search_patient     = old('search_patient');
        $blood_pressure     = old('blood_pressure');
        $checkup_date       = old('checkup_date');
        $temperature        = old('temperature');
        $pulse_rate         = old('pulse_rate');
        $respiration_rate   = old('respiration_rate');
        $weight             = old('weight');
        $tuberculosis       = old('tuberculosis');
        $hepatitis_b         = old('hepatitis_b');
        $hepatitis_c         = old('hepatitis_c');
        $pneumocystis_pneumonia = old('pneumocystis_pneumonia');
        $oropharyngeal_candidiasis = old('oropharyngeal_candidiasis');
        $syphilis           = old('syphilis');
        $stis               = old('stis');
        $sti_reason         = old('sti_reason');
        $diagnosed_other    = old('diagnosed_other');
        $diagnosed_other_input = old('diagnosed_other_input');
        $site              = old('site');
        $regimen            = old('regimen');
        $drug_resistance_value = old('drug_resistance_value');
        $drug_resistance_value_specify = old('drug_resistance_value_specify');
        $recommendation    = old('recommendation');
        
        $data= compact('action','demographics_id','search_patient','checkup_date','blood_pressure','temperature', 'pulse_rate', 
            'respiration_rate', 'weight','tuberculosis', 'hepatitis_b', 'hepatitis_c',
            'pneumocystis_pneumonia', 'oropharyngeal_candidiasis', 'syphilis', 'stis',
            'sti_reason','diagnosed_other','diagnosed_other_input','site', 'regimen', 
            'drug_resistance_value','drug_resistance_value_specify','recommendation');
        return view('hact.event.check-up.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CheckupRequest $request)
    {
        //
       $row = new Checkup;
       $row->demographics_id   = $request->input('demographics_id');
       $row->checkup_date      = $request->input('checkup_date');
       $row->blood_pressure    = $request->input('blood_pressure');
       $row->temperature       = $request->input('temperature');
       $row->pulse_rate        = $request->input('pulse_rate');
       $row->respiration_rate  = $request->input('respiration_rate');
       $row->weight            = $request->input('weight');
       $row->tuberculosis      = $request->input('tuberculosis');
       $row->hepatitis_b        = $request->input('hepatitis_b');
       $row->hepatitis_c        = $request->input('hepatitis_c');
       $row->pneumocystis_pneumonia = $request->input('pneumocystis_pneumonia');
       $row->oropharyngeal_candidiasis = $request->input('oropharyngeal_candidiasis');
       $row->syphilis          = $request->input('syphilis');
       $row->stis              = $request->input('stis');
       $row->diagnosed_other   =$request->input('diagnosed_other');
       $row->diagnosed_other_input = $request->input('diagnosed_other_input');
       $row->site              = $request->input('site');
       $row->regimen           = $request->input('regimen');
       $row->drug_resistance_value  = $request->input('drug_resistance_value');
       $row->drug_resistance_value_specify= $request->input('drug_resistance_value_specify');
       $row->recommendation    = $request->input('recommendation');
       $row->save();

        return redirect()->route('checkup_create')->with('status','Patient record successfully created!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $checkup  = Checkup::where('demographics_id',$id)
                    ->orderBy('demographics_id', 'DESC')
                    ->get();
        $patient    = Checkup::where('id',$id)->first();
        $data       = compact('checkup','patient');

        return view('hact.event.check-up.show', $data);
        // dd($patient->demographics_id );
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $action = route('checkup_update', $id);

        $row = Checkup::where('id', $id)->first();
        $demographics_id    = $row->demographics_id;
        $search_patient     = $row->EnrolmentDemographics->full_name2;
        $blood_pressure     = $row->blood_pressure;
        $temperature        = $row->temperature;
        $pulse_rate         = $row->pulse_rate;
        $respiration_rate   = $row->respiration_rate;
        $weight             = $row->weight;
        $tuberculosis       = $row->tuberculosis;
        $hepatitis_b         = $row->hepatitis_b;
        $hepatitis_c         = $row->hepatitis_c;
        $pneumocystis_pneumonia = $row->pneumocystis_pneumonia;
        $oropharyngeal_candidiasis = $row->oropharyngeal_candidiasis;
        $syphilis           = $row->syphilis;
        $stis               =$row->stis;
        $sti_reason         = $row->sti_reason;
        $diagnosed_other    = $row->diagnosed_other;
        $diagnosed_other_input = $row->diagnosed_other_input;
        $site               = $row->site;
        $regimen            = $row->regimen;
        $drug_resistance_value = $row->drug_resistance_value;
        $drug_resistance_value_specify = $row->drug_resistance_value_specify;
        $recommendation    = $row->recommendation;
        
        $data= compact('action','demographics_id','search_patient','blood_pressure','temperature', 'pulse_rate', 
            'respiration_rate', 'weight','tuberculosis', 'hepatitis_b', 'hepatitis_c',
            'pneumocystis_pneumonia', 'oropharyngeal_candidiasis', 'syphilis', 'stis',
            'sti_reason','diagnosed_other','diagnosed_other_input','site', 'regimen', 
            'drug_resistance_value','drug_resistance_value_specify','recommendation');
        return view('hact.event.check-up.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\CheckupRequest $request, $id)
    {
        //
        $row = Checkup::find($id);

        $row->demographics_id   = $request->input('demographics_id');
        $row->blood_pressure    = $request->input('blood_pressure');
        $row->temperature       = $request->input('temperature');
        $row->pulse_rate        = $request->input('pulse_rate');
        $row->respiration_rate  = $request->input('respiration_rate');
        $row->weight            = $request->input('weight');
        $row->tuberculosis      = $request->input('tuberculosis');
        $row->hepatitis_b        = $request->input('hepatitis_b');
        $row->hepatitis_c        = $request->input('hepatitis_c');
        $row->pneumocystis_pneumonia = $request->input('pneumocystis_pneumonia');
        $row->oropharyngeal_candidiasis = $request->input('oropharyngeal_candidiasis');
        $row->syphilis          = $request->input('syphilis');
        $row->stis              = $request->input('stis');
        $row->diagnosed_other   =$request->input('diagnosed_other');
        $row->diagnosed_other_input = $request->input('diagnosed_other_input');
        $row->site              = $request->input('site');
        $row->regimen           = $request->input('regimen');
        $row->drug_resistance_value  = $request->input('drug_resistance_value');
        $row->drug_resistance_value_specify= $request->input('drug_resistance_value_specify');
        $row->recommendation    = $request->input('recommendation');
        $row->save();
        return redirect()->route('checkup_edit', $id)->with('status','Patient record successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }  

}
