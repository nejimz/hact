<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ARVRefill;

class ARVRefillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = '';
        $number_per_page = 50;

        if($request->has('search'))
        {
            $search = trim($request->input('search'));

            $patients = ARVRefill::whereHas('EnrolmentDemographics', function($query) use ($search){
                $query->where('code_name', 'LIKE', '%'.$search.'%')
                        ->orWhere('saccl_code', 'LIKE', '%'.$search.'%')
                        ->orWhere('first_name', 'LIKE', '%'.$search.'%')
                        ->orWhere('last_name', 'LIKE', '%'.$search.'%')
                        ->orderBy('ui_code', 'ASC');
            })->paginate($number_per_page);
        }
        else
        {
            $patients = ARVRefill::paginate($number_per_page);
        }

        /*$code_name_sort     = $this->demographics_link_sort('code_name', $search, $sort, $request);
        $saccl_code_sort    = $this->demographics_link_sort('saccl_code', $search, $sort, $request); 
        $last_name_sort     = $this->demographics_link_sort('last_name', $search, $sort, $request); 
        $ui_code_sort       = $this->demographics_link_sort('ui_code', $search, $sort, $request); 
        $pagination         = ['search' => $search, 'order_by' => $order_by, 'sort' => $sort];*/
        $pagination         = ['search' => $search];

         #$data = compact('patients', 'search', 'code_name_sort', 'saccl_code_sort', 'ui_code_sort', 'last_name_sort', 'pagination');
         $data = compact('patients', 'search', 'pagination');

        return view('hact.event.arv-refill.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action  = route('arv_refill_store');
        $demographics_id = old('demographics_id');
        $search_patient  = old('search_patient');

        $blood_pressure = old('blood_pressure');
        $temperature    = old('temperature');
        $pulse_rate     = old('pulse_rate');
        $respiration_rate  = old('respiration_rate');
        $weight         = old('weight');

        $change_arv_regimen = old('change_arv_regimen');
        $change_arv_regimen_yes_reason = old('change_arv_regimen_yes_reason');
        $date_change        = old('date_change');
        $new_arv_regimen    = old('new_arv_regimen');
        $recommendations    = old('recommendations');

        $data = compact( 'action', 'demographics_id', 'item_id', 'search_patient',
            'blood_pressure', 'temperature', 'pulse_rate', 'respiration_rate', 'weight',
            'change_arv_regimen', 'change_arv_regimen_yes_reason', 'date_change', 'new_arv_regimen', 'recommendations' );

        return view('hact.event.arv-refill.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\ARVRefillRequest $request)
    {
        $row = new ARVRefill;
        $row->demographics_id   = $request->input('demographics_id');
        $row->blood_pressure    = $request->input('blood_pressure');
        $row->temperature       = $request->input('temperature');
        $row->pulse_rate        = $request->input('pulse_rate');
        $row->respiration_rate  = $request->input('respiration_rate');
        $row->weight            = $request->input('weight');
        $row->change_arv_regimen= $request->input('change_arv_regimen');
        $row->change_arv_regimen_yes_reason = $request->input('change_arv_regimen_yes_reason');
        $row->date_change       = $request->input('date_change');
        $row->new_arv_regimen   = $request->input('new_arv_regimen');
        $row->recommendations   = $request->input('recommendations');
        $row->save();

        return redirect()->route('arv_refill_create')->with('status', 'Patient Record Event successfully saved!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $action  = route('arv_refill_update', $id);
        #$row = ARVRefill::with('enrolment_demographics')->Demographics()->where('enrolment_demographics.first_name', 'LIKE', '%' . $id . '%')->get();
        #dd($row);
        $row = ARVRefill::where('id', $id)->first();

        $demographics_id = $row->demographics_id;
        $search_patient  = $row->EnrolmentDemographics->full_name2;

        $blood_pressure = $row->blood_pressure;
        $temperature    = $row->temperature;
        $pulse_rate     = $row->pulse_rate;
        $respiration_rate  = $row->respiration_rate;
        $weight         = $row->weight;

        $change_arv_regimen = $row->change_arv_regimen;
        $change_arv_regimen_yes_reason = $row->change_arv_regimen_yes_reason;
        $date_change        = date('F d, Y', strtotime($row->date_change));
        $new_arv_regimen    = $row->new_arv_regimen;
        $recommendations    = $row->recommendations;

        $data = compact( 'action', 'demographics_id', 'item_id', 'search_patient',
            'blood_pressure', 'temperature', 'pulse_rate', 'respiration_rate', 'weight',
            'change_arv_regimen', 'change_arv_regimen_yes_reason', 'date_change', 'new_arv_regimen', 'recommendations' );

        return view('hact.event.arv-refill.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\ARVRefillRequest $request, $id)
    {
        $row = ARVRefill::find($id);
        $row->demographics_id   = $request->input('demographics_id');
        $row->blood_pressure    = $request->input('blood_pressure');
        $row->temperature       = $request->input('temperature');
        $row->pulse_rate        = $request->input('pulse_rate');
        $row->respiration_rate  = $request->input('respiration_rate');
        $row->weight            = $request->input('weight');
        $row->change_arv_regimen= $request->input('change_arv_regimen');
        $row->change_arv_regimen_yes_reason = $request->input('change_arv_regimen_yes_reason');
        $row->date_change       = $request->input('date_change');
        $row->new_arv_regimen   = $request->input('new_arv_regimen');
        $row->recommendations   = $request->input('recommendations');
        $row->save();

        return redirect()->route('arv_refill_edit', $id)->with('status', 'Patient Record Event successfully updated!');
    }
}
