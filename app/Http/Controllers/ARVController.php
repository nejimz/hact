<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\EnrolmentDemographics;
use App\ARV;

class ARVController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $paginate   = 50;
        $search     = '';
        $order_by   = $this->order_by($request);
        $sort       = $this->sort_query($request);
    
        if($request->has('search'))
        {
            $search = trim($request->input('search'));

            $patients = EnrolmentDemographics::searchDemographicsList($search)
                ->orderBy($order_by, $sort)
                ->paginate($paginate);
        }
        else
        {
            $patients = EnrolmentDemographics::orderBy($order_by, $sort)->paginate($paginate);
        }

        $ui_code_sort       = $this->link_sort('ui_code', $search, $sort, $request); 
        $saccl_code_sort    = $this->link_sort('saccl_code', $search, $sort, $request); 
        $code_name_sort     = $this->link_sort('code_name', $search, $sort, $request);
        $saccl_code_sort     = $this->link_sort('saccl_code', $search, $sort, $request);
        $last_name_sort     = $this->link_sort('last_name', $search, $sort, $request); 
        $pagination         = ['search' => $search, 'order_by' => $order_by, 'sort' => $sort];

        $data = compact('patients', 'search', 'laboratory_sort', 'code_name_sort','saccl_code_sort', 'last_name_sort', 'ui_code_sort', 'pagination');
    

        return view('hact.event.arv.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id = null)
    {
        $action             = route('arv_store');
        $demographics_id    = old('demographics_id');
        $search_patient     = old('search_patient');

        if(!is_null($id))
        {
            $row = EnrolmentDemographics::where('id', $id)->first();

            $demographics_id    = $row->id;
            $search_patient     = $row->full_name2;
        }

        $blood_pressure     = old('blood_pressure');
        $temperature        = old('temperature');
        $pulse_rate         = old('pulse_rate');
        $respiration_rate   = old('respiration_rate');
        $weight             = old('weight');

        $tuberculosis       = old('tuberculosis');
        $site               = old('site');
        $regimen            = old('regimen');
        $drug_resistance_value = old('drug_resistance_value');
        $drug_resistance_value_specify = old('drug_resistance_value_specify');

        $hepatitisb         = old('hepatitisb');
        $hepatitisc         = old('hepatitisc');
        $pneumocystis_pneumonia = old('pneumocystis_pneumonia');
        $oropharyngeal_candidiasis = old('oropharyngeal_candidiasis');
        $syphilis           = old('syphilis');
        $stis               = old('stis');
        $stis_reason        = old('stis_reason');
        $diagnosed_other    = old('diagnosed_other');
        $diagnosed_other_input = old('diagnosed_other_input');

        $classification     = old('classification');

        $arv_date           = old('arv_date');
        $low_cd4_count      = old('low_cd4_count');
        $active_tb          = old('active_tb');
        $child_5yr          = old('child_5yr');
        $hep_b_c            = old('hep_b_c');
        $pregnant_breastfeeding = old('pregnant_breastfeeding');
        $who_classification = old('who_classification');
        $reason_other       = old('reason_other');
        $recommendations    = old('recommendations');

        $data= compact('action','demographics_id','search_patient',
            'blood_pressure','temperature', 'pulse_rate', 
            'respiration_rate', 'weight','tuberculosis', 'hepatitisb', 'hepatitisc',
            'pneumocystis_pneumonia', 'oropharyngeal_candidiasis', 'syphilis', 'stis',
            'stis_reason','diagnosed_other','diagnosed_other_input', 'site', 'regimen', 
            'drug_resistance_value','drug_resistance_value_specify', 'classification', 'arv_date', 
            'low_cd4_count', 'active_tb','child_5yr', 'hep_b_c', 'pregnant_breastfeeding', 
            'who_classification', 'reason_other', 'recommendations','search_patient', 'demographics_id',
            'search_patient_url');

        return view('hact.event.arv.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\ARVStoreRequest $request)
    {
        $arv = new ARV;
        $arv->demographics_id   = $request->input('demographics_id');

        $arv->blood_pressure    = $request->input('blood_pressure');
        $arv->temperature       = $request->input('temperature');
        $arv->pulse_rate        = $request->input('pulse_rate');
        $arv->respiration_rate  = $request->input('respiration_rate');
        $arv->weight            = $request->input('weight');

        $arv->tuberculosis      = $request->input('tuberculosis');
        $arv->site              = $request->input('site');
        $arv->regimen           = $request->input('regimen');
        $arv->drug_resistance_value         = $request->input('drug_resistance_value');
        $arv->drug_resistance_value_specify = $request->input('drug_resistance_value_specify');

        $arv->hepatitis_b       = $request->input('hepatitisb');
        $arv->hepatitis_c       = $request->input('hepatitisc');
        $arv->pneumocystis_pneumonia    = $request->input('pneumocystis_pneumonia');
        $arv->oropharyngeal_candidiasis = $request->input('oropharyngeal_candidiasis');
        $arv->syphilis          = $request->input('syphilis');
        //  STI
        $arv->stis              = $request->input('stis');
        $arv->stis_reason       = $request->input('stis_reason');
        //  Others
        $arv->diagnosed_other        = $request->input('diagnosed_other');
        $arv->diagnosed_other_input  = $request->input('diagnosed_other_input');

        $arv->classification         = $request->input('classification');
        $arv->arv_date               = $request->input('arv_date');
        $arv->low_cd4_count          = $request->input('low_cd4_count');
        $arv->active_tb              = $request->input('active_tb');
        $arv->child_5yr              = $request->input('child_5yr');
        $arv->hep_b_c                = $request->input('hep_b_c');
        $arv->pregnant_breastfeeding = $request->input('pregnant_breastfeeding');
        $arv->who_classification     = $request->input('who_classification');
        $arv->reason_other           = $request->input('reason_other');

        $arv->recommendations        = $request->input('recommendations');
        $arv->save();

        return redirect()->route('arv_create')->with('status','Patient record successfully created!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patient    = EnrolmentDemographics::where('id', $id)->first();
        $rows       = ARV::where('demographics_id', $id)->orderBy('arv_date', 'DESC')->paginate(50);

        $data = compact('patient', 'rows');

        return view('hact.event.arv.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $action             = route('arv_update', $id);
        $row                = ARV::where('id', $id)->first();

        $demographics_id    = $row->demographics_id;
        $search_patient     = $row->EnrolmentDemographics->full_name2;

        $blood_pressure     = $row->blood_pressure;
        $temperature        = $row->temperature;
        $pulse_rate         = $row->pulse_rate;
        $respiration_rate   = $row->respiration_rate;
        $weight             = $row->weight;

        $tuberculosis       = $row->tuberculosis;
        $site               = $row->site;
        $regimen            = $row->regimen;
        $drug_resistance_value = $row->drug_resistance_value;
        $drug_resistance_value_specify = $row->drug_resistance_value_specify;

        $hepatitisb         = $row->hepatitis_b;
        $hepatitisc         = $row->hepatitis_c;
        $pneumocystis_pneumonia = $row->pneumocystis_pneumonia;
        $oropharyngeal_candidiasis = $row->oropharyngeal_candidiasis;
        $syphilis           = $row->syphilis;
        $stis               = $row->stis;
        $stis_reason        = $row->stis_reason;
        $diagnosed_other    = $row->diagnosed_other;
        $diagnosed_other_input = $row->diagnosed_other_input;

        $classification     = $row->classification;

        $arv_date           = $row->arv_date_format;
        $low_cd4_count      = $row->low_cd4_count;
        $active_tb          = $row->active_tb;
        $child_5yr          = $row->child_5yr;
        $hep_b_c            = $row->hep_b_c;
        $pregnant_breastfeeding = $row->pregnant_breastfeeding;
        $who_classification = $row->who_classification;
        $reason_other       = $row->reason_other;
        $recommendations    = $row->recommendations;

        $data= compact('action','demographics_id','search_patient',
            'blood_pressure','temperature', 'pulse_rate', 
            'respiration_rate', 'weight','tuberculosis', 'hepatitisb', 'hepatitisc',
            'pneumocystis_pneumonia', 'oropharyngeal_candidiasis', 'syphilis', 'stis',
            'stis_reason','diagnosed_other','diagnosed_other_input', 'site', 'regimen', 
            'drug_resistance_value','drug_resistance_value_specify', 'classification', 'arv_date', 
            'low_cd4_count', 'active_tb','child_5yr', 'hep_b_c', 'pregnant_breastfeeding', 
            'who_classification', 'reason_other', 'recommendations');

        return view('hact.event.arv.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\ARVStoreRequest $request, $id)
    {
        $arv = ARV::find($id);
        $arv->demographics_id   = $request->input('demographics_id');

        $arv->blood_pressure    = $request->input('blood_pressure');
        $arv->temperature       = $request->input('temperature');
        $arv->pulse_rate        = $request->input('pulse_rate');
        $arv->respiration_rate  = $request->input('respiration_rate');
        $arv->weight            = $request->input('weight');

        $arv->tuberculosis      = $request->input('tuberculosis');
        $arv->site              = $request->input('site');
        $arv->regimen           = $request->input('regimen');
        $arv->drug_resistance_value         = $request->input('drug_resistance_value');
        $arv->drug_resistance_value_specify = $request->input('drug_resistance_value_specify');

        $arv->hepatitis_b       = $request->input('hepatitisb');
        $arv->hepatitis_c       = $request->input('hepatitisc');
        $arv->pneumocystis_pneumonia    = $request->input('pneumocystis_pneumonia');
        $arv->oropharyngeal_candidiasis = $request->input('oropharyngeal_candidiasis');
        $arv->syphilis          = $request->input('syphilis');
        //  STI
        $arv->stis              = $request->input('stis');
        $arv->stis_reason       = $request->input('stis_reason');
        //  Others
        $arv->diagnosed_other        = $request->input('diagnosed_other');
        $arv->diagnosed_other_input  = $request->input('diagnosed_other_input');

        $arv->classification         = $request->input('classification');
        $arv->arv_date               = $request->input('arv_date');
        $arv->low_cd4_count          = $request->input('low_cd4_count');
        $arv->active_tb              = $request->input('active_tb');
        $arv->child_5yr              = $request->input('child_5yr');
        $arv->hep_b_c                = $request->input('hep_b_c');
        $arv->pregnant_breastfeeding = $request->input('pregnant_breastfeeding');
        $arv->who_classification     = $request->input('who_classification');
        $arv->reason_other           = $request->input('reason_other');

        $arv->recommendations        = $request->input('recommendations');
        $arv->save();

        return redirect()->route('arv_edit', $id)->with('status','Patient record successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function order_by(Request $request)
    {
        if($request->has('order_by'))
        {
            $order_by = trim($request->input('order_by'));

            if($order_by == 'last_name')
            {
                return 'last_name';
            }
            elseif($order_by == 'code_name')
            {
                return 'code_name';
            }

            elseif($order_by == 'saccl_code')
            {
                return 'saccl_code';
            }
            elseif($order_by == 'ui_code')
            {
                return 'ui_code';
            }
            /*elseif($order_by == 'laboratory')
            {
                return 'laboratory';
            }
            elseif($order_by == 'result')
            {
                return 'result';
            }*/            
            else
            {
                return 'last_name';
            }
        }
        else
        {
            return 'last_name';
        }
    }

    public function sort_query($request)
    {

        if($request->has('sort'))
        {
            $sort = $request->input('sort');

            if($sort == 'ASC')
            {
                return 'ASC';
            }
            elseif($sort == 'DESC')
            {
                return 'DESC';
            }
        }
        else
        {
            return 'ASC';
        }
    }

    public function link_sort($order_by, $search, $sort, $request)
    {
        $sort  = ($sort == 'DESC')? 'ASC' : 'DESC';

        if($request->has('page'))
        {
            return route('caip_index', ['search' => $search, 'order_by' => $order_by, 'sort' => $sort, 'page' => $request->input('page')]);
        }
        else
        {
            return route('caip_index', ['search' => $search, 'order_by' => $order_by, 'sort' => $sort]);
        }
    }
}
