<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\VCT;
use App\EnrolmentDemographics;
use App\EnrolmentClinicalAndImmunologic;
use App\EnrolmentMedication;
use App\EnrolmentOpportunisticInfections;
use Auth;

class ClinicalAndImmunologicProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $paginate   = 50;
        $search     = '';
        $order_by   = $this->clinical_and_immunologic_order_by($request);
        $sort       = $this->sort_query($request);
    
        if($request->has('search'))
        {
            $search = trim($request->input('search'));

            $patients = EnrolmentDemographics::searchDemographicsList($search)
                ->orderBy($order_by, $sort)
                ->paginate($paginate);
        }
        else
        {
            $patients = EnrolmentDemographics::orderBy($order_by, $sort)->paginate($paginate);
        }

        $ui_code_sort       = $this->clinical_and_immunologic_link_sort('ui_code', $search, $sort, $request); 
        $saccl_code_sort    = $this->clinical_and_immunologic_link_sort('saccl_code', $search, $sort, $request); 
        $code_name_sort     = $this->clinical_and_immunologic_link_sort('code_name', $search, $sort, $request);
        $saccl_code_sort     = $this->clinical_and_immunologic_link_sort('saccl_code', $search, $sort, $request);
        $last_name_sort     = $this->clinical_and_immunologic_link_sort('last_name', $search, $sort, $request); 
        $pagination         = ['search' => $search, 'order_by' => $order_by, 'sort' => $sort];

        $data = compact('patients', 'search', 'laboratory_sort', 'code_name_sort','saccl_code_sort', 'last_name_sort', 'ui_code_sort', 'pagination');
    
        return view('hact.enrolment.clinical-and-immunologic-profile.index', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
            $caips      = EnrolmentClinicalAndImmunologic::where('demographics_id',$id)
                            ->orderBy('date_result', 'DESC')
                            ->orderBy('laboratory', 'ASC')
                            ->get();
            $patient    = EnrolmentDemographics::where('id',$id)->first();
            $data       = compact('caips','patient');

            return view('hact.enrolment.clinical-and-immunologic-profile.show', $data);
    }
    
    public function create($id = null)
    {
        $action = route('caip_store');
        $demographics_id = old('demographics_id');
        $search_patient  = old('search_patient');

        if(!is_null($id))
        {
            $row = EnrolmentDemographics::where('id', $id)->first();

            $demographics_id    = $row->id;
            $search_patient     = $row->full_name2;
        }

        $laboratory = old('laboratory');
        $laboratory_others = old('laboratory_others');
        $date_result = old('date_result');
        $result = old('result');
        
        $data = compact('action',
            'search_patient','demographics_id', 
            'laboratory','laboratory_others', 
            'date_result','result'
            );

        return view('hact.enrolment.clinical-and-immunologic-profile.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\ClinicalAndImmunologicStoreRequest $request)
    {
        $row = new EnrolmentClinicalAndImmunologic;
        $row->demographics_id   = $request->input('demographics_id');
        $row->laboratory        = $request->input('laboratory');
        $row->laboratory_others = $request->input('laboratory_others');
        $row->date_result       = $request->input('date_result');
        $row->result            = $request->input('result');
        $row->save();

        return redirect()->route('caip_create')->with('status', 'Patient laboratory successfully enroled!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $action = route('caip_update', [$id]);
        $patient = EnrolmentClinicalAndImmunologic::where('id', $id)->first();
        
        $search_patient     = $patient->EnrolmentDemographics->full_name2;
        $demographics_id    = $patient->demographics_id;
        $laboratory         = $patient->laboratory;
        $laboratory_others  = $patient->laboratory_others;
        $date_result        = $patient->date_result;
        $result             = $patient->result;

        $data = compact('action',
            'search_patient','demographics_id', 
            'laboratory','laboratory_others', 
            'date_result','result'
            );

        return view('hact.enrolment.clinical-and-immunologic-profile.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $row = EnrolmentClinicalAndImmunologic::find($id);
        $row->demographics_id   = $request->input('demographics_id');
        $row->laboratory        = $request->input('laboratory');
        $row->laboratory_others = $request->input('laboratory_others');
        $row->date_result       = $request->input('date_result');
        $row->result            = $request->input('result');
        $row->save();

        return redirect()->route('caip_edit', [$id])->with('status', 'Patient laboratory successfully update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $caip = EnrolmentClinicalAndImmunologic::where('id','=',$id)->first();
        $demographics_id = $caip->demographics_id;
        $caip->delete();

        return redirect()->route('caip_show',[$demographics_id])->with('status', 'clinical record deleted successfully!');
    }
    
    public function clinical_and_immunologic_order_by(Request $request)
    {
        if($request->has('order_by'))
        {
            $order_by = trim($request->input('order_by'));

            if($order_by == 'last_name')
            {
                return 'last_name';
            }
            elseif($order_by == 'code_name')
            {
                return 'code_name';
            }

            elseif($order_by == 'saccl_code')
            {
                return 'saccl_code';
            }
            elseif($order_by == 'ui_code')
            {
                return 'ui_code';
            }
            /*elseif($order_by == 'laboratory')
            {
                return 'laboratory';
            }
            elseif($order_by == 'result')
            {
                return 'result';
            }*/            
            else
            {
                return 'last_name';
            }
        }
        else
        {
            return 'last_name';
        }
    }

    public function sort_query($request)
    {

        if($request->has('sort'))
        {
            $sort = $request->input('sort');

            if($sort == 'ASC')
            {
                return 'ASC';
            }
            elseif($sort == 'DESC')
            {
                return 'DESC';
            }
        }
        else
        {
            return 'ASC';
        }
    }

    public function clinical_and_immunologic_link_sort($order_by, $search, $sort, $request)
    {
        $sort  = ($sort == 'DESC')? 'ASC' : 'DESC';

        if($request->has('page'))
        {
            return route('caip_index', ['search' => $search, 'order_by' => $order_by, 'sort' => $sort, 'page' => $request->input('page')]);
        }
        else
        {
            return route('caip_index', ['search' => $search, 'order_by' => $order_by, 'sort' => $sort]);
        }
    }
}
