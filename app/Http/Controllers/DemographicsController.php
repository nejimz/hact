<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\VCT;
use App\EnrolmentDemographics;
use App\EnrolmentClinicalAndImmunologic;
use App\EnrolmentMedication;
use App\EnrolmentOpportunisticInfections;
use DB;

class DemographicsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
     
        $paginate   = 50;
        $search     = '';
        $order_by   = $this->demographics_order_by($request);
        $sort       = $this->sort_query($request);
        
        if($request->has('search'))
        {
            $search = trim($request->input('search'));

            $patients = EnrolmentDemographics::searchDemographicsList($search)->orderBy($order_by, $sort)->paginate($paginate);
        }
        else
        {
            $patients = EnrolmentDemographics::orderBy($order_by, $sort)->paginate($paginate);
        }

        $code_name_sort     = $this->demographics_link_sort('code_name', $search, $sort, $request);
        $saccl_code_sort    = $this->demographics_link_sort('saccl_code', $search, $sort, $request); 
        $last_name_sort     = $this->demographics_link_sort('last_name', $search, $sort, $request); 
        $ui_code_sort       = $this->demographics_link_sort('ui_code', $search, $sort, $request); 
        $pagination         = ['search' => $search, 'order_by' => $order_by, 'sort' => $sort];

        $data = compact('patients', 'search', 'code_name_sort', 'saccl_code_sort', 'ui_code_sort', 'last_name_sort', 'pagination');
    
      return view('hact.enrolment.demographics.index', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $action = route('demographics_store');

        //$vct_id         = old('vct_id');
        $search_patient = old('search_patient');
        $first_name     = old('first_name');
        $middle_name    = old('middle_name');
        $last_name      = old('last_name');

        $initial_contract_date  = old('initial_contract_date');
        $enrolment_date         = old('enrolment_date');
        $diagnosis_date         = old('diagnosis_date');
        $code_name              = old('code_name');
        $saccl                  = old('saccl');

        $uic_mother_name = old('uic_mother_name');
        $uic_father_name = old('uic_father_name');
        $uic_birth_order = old('uic_birth_order');

        $mother_first_name  = old('mother_first_name');
        $mother_middle_name = old('mother_middle_name');
        $mother_last_name   = old('mother_last_name');

        $gender         = old('gender');
        $civil_status   = old('civil_status');
        $birth_date     = old('birth_date');
        $age            = old('age');

        $contact_number     = old('contact_number');
        $philheath_number   = old('philheath_number');

        $birth_place_city       = old('birth_place_city');
        $birth_place_province   = old('birth_place_province');

        $street = old('street');
        $purok  = old('purok');
        $barangay = old('barangay');
        $city   = old('city');

        $blood_transfusion      = old('blood_transfusion');
        $injecting_drug_user    = old('injecting_drug_user');
        $substance_abuse        = old('substance_abuse');
        $occupational_exposure  = old('occupational_exposure');

        $provided_post_exposure_prophylaxis = old('provided_post_exposure_prophylaxis');
        $sexually_transmitted_infections    = old('sexually_transmitted_infections');

        $multiple_sexual_partners = old('multiple_sexual_partners');
        $male_sex_with_other_male = old('male_sex_with_other_male');

        $sex_worker_client  = old('sex_worker_client');
        $sex_worker         = old('sex_worker');

        $child_of_hiv_infected_mother = old('child_of_hiv_infected_mother');

        $hiv_risk_others        = old('hiv_risk_others');

        $search_physician       = old('search_physician');
        $attending_physician    = old('attending_physician');

        $data = compact('action', 

            'initial_contract_date', 'enrolment_date', 'diagnosis_date', 

            'vct_id', 'search_patient', 'first_name', 'middle_name', 'last_name', 

            'code_name', 'saccl', 

            'uic_mother_name', 'uic_father_name', 'uic_birth_order', 

            'mother_first_name', 'mother_middle_name', 'mother_last_name', 

            'gender', 'birth_date', 'age', 'civil_status', 

            'birth_place_city', 'birth_place_province', 

            'street', 'purok', 'city', 'barangay', 'contact_number', 'philheath_number', 

            'blood_transfusion', 'injecting_drug_user', 'substance_abuse', 'occupational_exposure', 

            'provided_post_exposure_prophylaxis', 'sexually_transmitted_infections', 

            'multiple_sexual_partners', 'male_sex_with_other_male', 

            'sex_worker_client', 'sex_worker', 'child_of_hiv_infected_mother',

            'hiv_risk_others', 

            'search_physician', 'attending_physician', 

            'id'
            );

        return view('hact.enrolment.demographics.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\DemographicsStoreRequest $request)
    {
        $ui_code                = $request->input('uic_mother_name') . '-' . $request->input('uic_father_name') . '-' . $request->input('uic_birth_order');

        $blood_transfusion      = ($request->has('blood_transfusion'))? 1 : 0;
        $injecting_drug_user    = ($request->has('injecting_drug_user'))? 1 : 0;
        $substance_abuse        = ($request->has('substance_abuse'))? 1 : 0;
        $occupational_exposure  = ($request->has('occupational_exposure'))? 1 : 0;

        $provided_post_exposure_prophylaxis = ($request->has('provided_post_exposure_prophylaxis'))? 1 : 0;
        $sexually_transmitted_infections    = ($request->has('sexually_transmitted_infections'))? 1 : 0;

        $multiple_sexual_partners       = ($request->has('multiple_sexual_partners'))? 1 : 0;
        $male_sex_with_other_male       = ($request->has('male_sex_with_other_male'))? 1 : 0;
        $sex_worker_client              = ($request->has('sex_worker_client'))? 1 : 0;
        $sex_worker                     = ($request->has('sex_worker'))? 1 : 0;
        $child_of_hiv_infected_mother   = ($request->has('child_of_hiv_infected_mother'))? 1 : 0;
        $other_hiv_risks                = $request->input('hiv_risk_others');

        $demographics = new EnrolmentDemographics;

        $demographics->ui_code        = $ui_code;
        $demographics->first_name     = $request->input('first_name');
        $demographics->middle_name    = $request->input('middle_name');
        $demographics->last_name      = $request->input('last_name');

        $demographics->gender         = $request->input('gender');
        $demographics->birth_date     = $request->input('birth_date');
        $demographics->contact_number = $request->input('contact_number');

        $demographics->mother_first_name     = $request->input('mother_first_name');
        $demographics->mother_middle_name    = $request->input('mother_middle_name');
        $demographics->mother_last_name      = $request->input('mother_last_name');

        $demographics->street    = $request->input('street');
        $demographics->sitio     = $request->input('purok');
        $demographics->brgy      = $request->input('barangay');
        $demographics->city      = $request->input('city');

        $demographics->initial_contract_date = $request->input('initial_contract_date');
        $demographics->enrolment_date        = $request->input('enrolment_date');
        $demographics->code_name             = $request->input('code_name');
        $demographics->saccl_code            = $request->input('saccl');
        $demographics->diagnosis_date        = $request->input('diagnosis_date');
        $demographics->civil_status          = $request->input('civil_status');
        $demographics->birth_place_city      = $request->input('birth_place_city');
        $demographics->birth_place_province  = $request->input('birth_place_province');
        $demographics->philheath_number      = $request->input('philheath_number');

        $demographics->blood_transfusion     = $blood_transfusion;
        $demographics->injecting_drug_user   = $injecting_drug_user;
        $demographics->substance_abuse       = $substance_abuse;
        $demographics->occupational_exposure = $occupational_exposure;

        $demographics->provided_post_exposure_prophylaxis = $provided_post_exposure_prophylaxis;
        $demographics->sexually_transmitted_infections    = $sexually_transmitted_infections;

        $demographics->multiple_sexual_partners     = $multiple_sexual_partners;
        $demographics->male_sex_with_other_males    = $male_sex_with_other_male;
        $demographics->sex_worker_client            = $sex_worker_client;
        $demographics->sex_worker                   = $sex_worker;
        $demographics->child_of_hiv_infected_mother = $child_of_hiv_infected_mother;
        $demographics->other_hiv_risks              = $other_hiv_risks;
        
        $demographics->physician_user_id = $request->input('attending_physician');

        $demographics->save();

        return redirect()->route('demographics_create')->with('status', 'Patient successfully enroled in demographics!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $demographics = EnrolmentDemographics::where('id', $id)->first();
        $action = route('demographics_update', [$id]);

        $search_patient = $demographics->full_name2;
        $first_name     = $demographics->first_name;
        $middle_name    = $demographics->middle_name;
        $last_name      = $demographics->last_name;

        $initial_contract_date  = $demographics->initial_contract_date;
        $enrolment_date         = $demographics->enrolment_date;
        $diagnosis_date         = $demographics->diagnosis_date;
        $code_name              = $demographics->code_name;
        $saccl                  = $demographics->saccl_code;

        $ui_code = explode('-',  $demographics->ui_code);
        $uic_mother_name = $ui_code[0];
        $uic_father_name = $ui_code[1];
        $uic_birth_order = $ui_code[2];

        $mother_first_name  = $demographics->mother_first_name;
        $mother_middle_name = $demographics->mother_middle_name;
        $mother_last_name   = $demographics->mother_last_name;

        $gender     = strtolower($demographics->gender);
        $civil_status   = $demographics->civil_status;
        $birth_date = $demographics->birth_date_format;
        $age        = \Carbon\Carbon::parse($demographics->birth_date)->age;

        $contact_number     = $demographics->contact_number;
        $philheath_number   = $demographics->philheath_number;

        $birth_place_city       = $demographics->birth_place_city;
        $birth_place_province   = $demographics->birth_place_province;

        $street = $demographics->street;
        $purok  = $demographics->sitio;
        $barangay = $demographics->brgy;
        $city   = $demographics->city;

        $blood_transfusion      = $demographics->blood_transfusion;
        $injecting_drug_user    = $demographics->injecting_drug_user;
        $substance_abuse        = $demographics->substance_abuse;
        $occupational_exposure  = $demographics->occupational_exposure;

        $provided_post_exposure_prophylaxis = $demographics->provided_post_exposure_prophylaxis;
        $sexually_transmitted_infections    = $demographics->sexually_transmitted_infections;

        $multiple_sexual_partners = $demographics->multiple_sexual_partners;
        $male_sex_with_other_male = $demographics->male_sex_with_other_male;

        $sex_worker_client  = $demographics->sex_worker_client;
        $sex_worker         = $demographics->sex_worker;

        $child_of_hiv_infected_mother = $demographics->child_of_hiv_infected_mother;

        $hiv_risk_others        = $demographics->other_hiv_risks;

        $physician = User::where('id', $demographics->physician_user_id)->first();
        $search_physician       = $physician->full_name2;
        $attending_physician    = $demographics->physician_user_id;

        $data = compact('action', 

            'initial_contract_date', 'enrolment_date', 'diagnosis_date', 

            'search_patient', 'first_name', 'middle_name', 'last_name', 

            'code_name', 'saccl', 

            'uic_mother_name', 'uic_father_name', 'uic_birth_order', 

            'mother_first_name', 'mother_middle_name', 'mother_last_name', 

            'gender', 'civil_status', 'birth_date', 'age',

            'birth_place_city', 'birth_place_province', 

            'street', 'purok', 'city', 'barangay', 'contact_number', 'philheath_number', 

            'blood_transfusion', 'injecting_drug_user', 'substance_abuse', 'occupational_exposure', 

            'provided_post_exposure_prophylaxis', 'sexually_transmitted_infections', 

            'multiple_sexual_partners', 'male_sex_with_other_male', 

            'sex_worker_client', 'sex_worker', 'child_of_hiv_infected_mother',

            'hiv_risk_others', 

            'search_physician', 'attending_physician', 

            'id'
            );

        return view('hact.enrolment.demographics.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\DemographicsUpdateRequest $request, $id)
    {
        $ui_code                = $request->input('uic_mother_name') . '-' . $request->input('uic_father_name') . '-' . $request->input('uic_birth_order');

        $blood_transfusion      = ($request->has('blood_transfusion'))? 1 : 0;
        $injecting_drug_user    = ($request->has('injecting_drug_user'))? 1 : 0;
        $substance_abuse        = ($request->has('substance_abuse'))? 1 : 0;
        $occupational_exposure  = ($request->has('occupational_exposure'))? 1 : 0;

        $provided_post_exposure_prophylaxis = ($request->has('provided_post_exposure_prophylaxis'))? 1 : 0;
        $sexually_transmitted_infections    = ($request->has('sexually_transmitted_infections'))? 1 : 0;

        $multiple_sexual_partners       = ($request->has('multiple_sexual_partners'))? 1 : 0;
        $male_sex_with_other_male       = ($request->has('male_sex_with_other_male'))? 1 : 0;
        $sex_worker_client              = ($request->has('sex_worker_client'))? 1 : 0;
        $sex_worker                     = ($request->has('sex_worker'))? 1 : 0;
        $child_of_hiv_infected_mother   = ($request->has('child_of_hiv_infected_mother'))? 1 : 0;
        $other_hiv_risks                = $request->input('hiv_risk_others');

        $demographics = EnrolmentDemographics::find($id);

        $demographics->ui_code        = $ui_code;
        $demographics->first_name     = $request->input('first_name');
        $demographics->middle_name    = $request->input('middle_name');
        $demographics->last_name      = $request->input('last_name');

        $demographics->gender         = $request->input('gender');
        $demographics->birth_date     = $request->input('birth_date');
        $demographics->contact_number = $request->input('contact_number');

        $demographics->mother_first_name     = $request->input('mother_first_name');
        $demographics->mother_middle_name    = $request->input('mother_middle_name');
        $demographics->mother_last_name      = $request->input('mother_last_name');

        $demographics->street    = $request->input('street');
        $demographics->sitio     = $request->input('purok');
        $demographics->brgy      = $request->input('barangay');
        $demographics->city      = $request->input('city');

        $demographics->initial_contract_date = $request->input('initial_contract_date');
        $demographics->enrolment_date        = $request->input('enrolment_date');
        $demographics->code_name             = $request->input('code_name');
        $demographics->saccl_code            = $request->input('saccl');
        $demographics->diagnosis_date        = $request->input('diagnosis_date');
        $demographics->civil_status          = $request->input('civil_status');
        $demographics->birth_place_city      = $request->input('birth_place_city');
        $demographics->birth_place_province  = $request->input('birth_place_province');
        $demographics->philheath_number      = $request->input('philheath_number');

        $demographics->blood_transfusion     = $blood_transfusion;
        $demographics->injecting_drug_user   = $injecting_drug_user;
        $demographics->substance_abuse       = $substance_abuse;
        $demographics->occupational_exposure = $occupational_exposure;

        $demographics->provided_post_exposure_prophylaxis = $provided_post_exposure_prophylaxis;
        $demographics->sexually_transmitted_infections    = $sexually_transmitted_infections;

        $demographics->multiple_sexual_partners     = $multiple_sexual_partners;
        $demographics->male_sex_with_other_males    = $male_sex_with_other_male;
        $demographics->sex_worker_client            = $sex_worker_client;
        $demographics->sex_worker                   = $sex_worker;
        $demographics->child_of_hiv_infected_mother = $child_of_hiv_infected_mother;
        $demographics->other_hiv_risks              = $other_hiv_risks;
        
        $demographics->physician_user_id = $request->input('attending_physician');

        $demographics->save();

        return redirect()->route('demographics_edit', [$id])->with('status', 'Patient demographics successfully update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search_patient(Request $request)
    {
        //
        $search = '%' . trim($request->input('search_patient')) . '%';

        $patients = EnrolmentDemographics::select(DB::raw('CONCAT(last_name, \', \', first_name) AS full_name_raw'), 'id')
                    ->whereNotIn('id', function($query) {
                        $query->select('demographics_id')->from('event_death');
                    })
                    ->where(function($query) use ($search) {
                        $query->where('first_name', 'LIKE', $search)
                              ->orWhere('last_name', 'LIKE', $search);
                    })
                    ->take(20)
                    ->lists('full_name_raw', 'id');
        //return Response::json($products)->setCallback(Input::get('callback'));
        return response()->json($patients);
    }
    
    public function demographics_order_by($request)
    {
        if($request->has('order_by'))
        {
            $order_by = trim($request->order_by);

            if($order_by == 'last_name')
            {
                return 'last_name';
            }
            elseif($order_by == 'saccl_code')
            {
                return 'saccl_code';
            }
            elseif($order_by == 'ui_code')
            {
                return 'ui_code';
            }
            elseif($order_by == 'code_name')
            {
                return 'code_name';
            }          
            else
            {
                return 'last_name';
            }
        }
        else
        {
            return 'last_name';
        }
    }

    public function sort_query($request)
    {

        if($request->has('sort'))
        {
            $sort = $request->input('sort');

            if($sort == 'ASC')
            {
                return 'ASC';
            }
            elseif($sort == 'DESC')
            {
                return 'DESC';
            }
        }
        else
        {
            return 'ASC';
        }
    }

    public function demographics_link_sort($order_by, $search, $sort, $request)
    {
        $sort  = ($sort == 'DESC')? 'ASC' : 'DESC';

        if($request->has('page'))
        {
            return route('demographics_index', ['search' => $search, 'order_by' => $order_by, 'sort' => $sort, 'page' => $request->input('page')]);
        }
        else
        {
            return route('demographics_index', ['search' => $search, 'order_by' => $order_by, 'sort' => $sort]);
        }
    }
}
