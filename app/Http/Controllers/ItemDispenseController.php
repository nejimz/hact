<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\Item;
use App\ItemDispense;
use App\User;

class ItemDispenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(Request $request)
    {
        $paginate       = 50;
        $search         = '';
        $action         = route('dispense_store');
        $search_patient = old('search_patient');
        $demographics_id= old('demographics_id');
        $quantity       = old('quantity');
        $item_id        = old('id');
        $search_item    = old('search_item');
        
        if($request->has('search'))
        {
            $search = trim($request->input('search'));
            $item_dispenses = ItemDispense::join('item','item_dispense.item_id','=','item.id')
                    ->select('*','item_dispense.id as item_id')
                    ->searchItemDispenseList($search)
                    ->orderBy('transaction_date', 'DESC')
                    ->paginate($paginate);

        }
        else
        {
            $item_dispenses = ItemDispense::join('item','item_dispense.item_id','=','item.id')
                    ->select('*','item_dispense.id as dispense_id')
                    ->orderBy('transaction_date', 'DESC')
                    ->paginate($paginate);
        }

        $pagination         = ['search' => $search];

        $data = compact('action','search_patient','demographics_id','quantity','search_item','item_id', 'item_dispenses', 'search', 'pagination');
    
        return view('hact.item.dispense.index', $data);

    }

    public function create(Request $request)
    {

        if($request->has('id')){
            $item_id = $request->input('id');
        }else{
            $item_id = old('id');
        }
        if($request->has('search_item')){
            $search_item  = $request->input('search_item');
        }else{
            $search_item  = old('search_item');
        }

        $quantity = old('quantity');
        $action = route('dispense_store',$request->input('id'));
        $data = compact('action','quantity','search_item','item_id');
        return view('hact.item.dispense.create', $data);
    }

    public function edit($id)
    {
        //
        $item_dispense = ItemDispense::where('id','=',$id)->first();
        $item = Item::where('id',$item_dispense->item_id)->first();
        $search_item = $item->item_name;
        $item_id = $item->id;
        $quantity = $item_dispense->quantity;
        $action = route('dispense_update',$id);
        $data = compact('action','quantity','search_item','item_id');
        return view('hact.item.dispense.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\ItemDispenseRequest $request)
    {
        $row = new ItemDispense;
        $row->demographics_id = $request->demographics_id;
        $row->item_id         = $request->item_id;
        $row->quantity        = $request->quantity;
        $row->user_id         = Auth::user()->id;
        $row->save();


        return redirect()->route('dispense_index')->with('status', 'Item successfully dispense!');
    }

    public function update(Requests\ItemDispenseRequest $request, $id)
    {   
        $row = ItemDispense::find($id);
        $row->demographics_id   = $request->demographics_id;
        $row->item_id           = $request->item_id;
        $row->quantity          = $request->quantity;
        $row->transaction_date  = Carbon::now()->toDateString();
        $row->user_id           = Auth::user()->id;
        $row->save();

        return redirect()->route('dispense_index')->with('status', 'Storage Report successfully updated!');
    }

    public function destroy($id)
    {
        $item_dispense = ItemDispense::where('id','=',$id)->first();
        $item_dispense->delete();

        return redirect()->route('dispense_index')->with('status', 'Report successfully deleted');
    }
    
    public function item_dispense_order_by($request)
    {
        if($request->has('order_by'))
        {
            $order_by = trim($request->has('order_by'));

            if($order_by == 'item_name')
            {
                return 'item.item_name';
            }
            elseif($order_by == 'transaction_date')
            {
                return 'transaction_date';
            }
            elseif($order_by == 'quantity')
            {
                return 'quantity';
            }            
            else
            {
                return 'transaction_date';
            }
        }
        else
        {
            return 'transaction_date';
        }

    }

    public function sort_query($request)
    {

        if($request->has('sort'))
        {
            $sort = $request->input('sort');

            if($sort == 'ASC')
            {
                return 'ASC';
            }
            elseif($sort == 'DESC')
            {
                return 'DESC';
            }
        }
        else
        {
            return 'ASC';
        }
    }

    public function item_dispenses_link_sort($order_by, $search, $sort, $request)
    {
        $sort  = ($sort == 'DESC')? 'ASC' : 'DESC';

        if($request->has('page'))
        {
            return route('dispense_index', ['search' => $search, 'order_by' => $order_by, 'sort' => $sort, 'page' => $request->input('page')]);
        }
        else
        {
            return route('dispense_index', ['search' => $search, 'order_by' => $order_by, 'sort' => $sort]);
        }
    }
}
