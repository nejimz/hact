<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\EnrolmentMedication;
use App\EnrolmentDemographics;

class MedicationProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $paginate   = 50;
        $search     = '';
        $order_by   = $this->medication_order_by($request);
        $sort       = $this->sort_query($request);

        if($request->has('search'))
        {
            $search = trim($request->input('search'));

            $patients = EnrolmentDemographics::searchDemographicsList($search)->orderBy($order_by, $sort)->paginate($paginate);
        }
        else
        {
            $patients = EnrolmentDemographics::orderBy($order_by, $sort)->paginate($paginate);
        }

        $ui_code_sort       = $this->medication_link_sort('ui_code', $search, $sort, $request);
        $code_name_sort     = $this->medication_link_sort('code_name', $search, $sort, $request);
        $saccl_code_sort    = $this->medication_link_sort('saccl_code', $search, $sort, $request);
        $last_name_sort     = $this->medication_link_sort('last_name', $search, $sort, $request);
        $pagination         = ['search' => $search, 'order_by' => $order_by, 'sort' => $sort];

        $data = compact('patients', 'search', 'saccl_code_sort','code_name_sort', 'last_name_sort', 'ui_code_sort','pagination');
    
        return view('hact.enrolment.medication-profile.index', $data);
    }
    
    public function medication_order_by($request)
    {
        if($request->has('order_by'))
        {
            $order_by = trim($request->input('order_by'));

            if($order_by == 'last_name')
            {
                return 'last_name';
            }
            elseif($order_by == 'code_name')
            {
                return 'code_name';
            }
            elseif($order_by == 'ui_code')
            {
                return 'ui_code';
            }
            elseif($order_by == 'saccl_code')
            {
                return 'saccl_code';
            }
            else
            {
                return 'last_name';
            }
        }
        else
        {
            return 'last_name';
        }
    }

    public function sort_query($request)
    {

        if($request->has('sort'))
        {
            $sort = $request->input('sort');

            if($sort == 'ASC')
            {
                return 'ASC';
            }
            elseif($sort == 'DESC')
            {
                return 'DESC';
            }
        }
        else
        {
            return 'ASC';
        }
    }

    public function medication_link_sort($order_by, $search, $sort, $request)
    {
        $sort  = ($sort == 'DESC')? 'ASC' : 'DESC';

        if($request->has('page'))
        {
            return route('medication_index', ['search' => $search, 'order_by' => $order_by, 'sort' => $sort, 'page' => $request->input('page')]);
        }
        else
        {
            return route('medication_index', ['search' => $search, 'order_by' => $order_by, 'sort' => $sort]);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patient     = EnrolmentDemographics::where('id', $id)->first();
        /*$medications = EnrolmentMedication::leftJoin('item','enrolment_medication.item_id','=','item.id')
                        ->select('*','enrolment_medication.id as medication_id')
                        ->where('demographics_id',$id)
                        ->get();*/
        $medications = EnrolmentMedication::where('demographics_id',$id)->paginate(50);

        $data = compact('medications','patient');

        return view('hact.enrolment.medication-profile.show', $data);
    }
    
    public function create($id = null)
    {
        $action  = route('medication_store');
        $search_patient  = old('search_patient');
        $demographics_id = old('demographics_id');

        if(!is_null($id))
        {
            $row = EnrolmentDemographics::where('id', $id)->first();

            $demographics_id    = $row->id;
            $search_patient     = $row->full_name2;
        }

        $item_id        = old('item_id');
        $search_item    = old('search_item');
        $dosage         = old('dosage');
        $frequency      = old('frequency');
        $date_started   = old('date_started');


        $data = compact( 'action', 'demographics_id', 'item_id', 'search_patient','search_item', 'dosage', 'frequency', 'date_started' );

        return view('hact.enrolment.medication-profile.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\MedicationStoreRequest $request)
    {
        $row = new EnrolmentMedication;
        $row->demographics_id   = $request->input('demographics_id');
        $row->item_id           = $request->input('item_id');
        $row->dosage            = $request->input('dosage');
        $row->frequency         = $request->input('frequency');
        $row->date_started      = $request->input('date_started');
        $row->save();

        return redirect()->route('medication_create')->with('status', 'Patient medication record updated successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $action  = route('medication_update', $id);
        $patient = EnrolmentMedication::where('id', $id)->first();
        
        $demographics_id = $patient->demographics_id;
        $item_id         = $patient->item_id;

        $search_patient  = $patient->EnrolmentDemographics->full_name_2;
        $search_item = $patient->Item->item_name;
        $dosage          = $patient->dosage;
        $frequency       = $patient->frequency;
        $date_started    = $patient->date_started_formatted;

        $data = compact( 'action', 'demographics_id', 'item_id', 'search_patient','search_item', 'dosage', 'frequency', 'date_started' );

        return view('hact.enrolment.medication-profile.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\MedicationStoreRequest $request, $id)
    {
        $row = EnrolmentMedication::find($id);
        $row->demographics_id   = $request->input('demographics_id');
        $row->item_id           = $request->input('item_id');
        $row->dosage            = $request->input('dosage');
        $row->frequency         = $request->input('frequency');
        $row->date_started      = $request->input('date_started');
        $row->save();

        return redirect()->route('medication_show',$row->demographics_id)->with('status', 'Patient medication record updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_stop($id)
    {
        $action  = route('medication_update_stop', $id);
        $medication = EnrolmentMedication::where('id', $id)->first();
        $demographics_id = $medication->demographics_id;
        $item_id         = $medication->item_id;

        $search_patient  = $medication->EnrolmentDemographics->full_name_2;
        $search_item     = $medication->Item->item_name;
        $dosage          = $medication->dosage;
        $frequency       = $medication->frequency;
        $date_started    = $medication->date_started_formatted;

        $date_discontinued  = $medication->date_discontinued_formatted;
        $discontinuation_reason = $medication->discontinuation_reason;
        
        

        $data = compact( 'action', 'demographics_id', 'item_id', 'search_patient','search_item', 
            'dosage', 'frequency', 'date_started', 'date_discontinued', 'discontinuation_reason');

        return view('hact.enrolment.medication-profile.stop', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_stop(Requests\MedicationStopRequest $request, $id)
    {
        $row = EnrolmentMedication::find($id);
        $row->date_discontinued         = $request->input('date_discontinued');
        $row->discontinuation_reason    = $request->input('discontinuation_reason');
        $row->save();

        return redirect()->route('medication_show',$row->demographics_id)->with('status', 'Patient medication record updated successfully!');
    }

    public function continue_medication($id)
    {
        $row = EnrolmentMedication::find($id);
        $row->date_discontinued         = null;
        $row->discontinuation_reason    = null;
        $row->save();

        return redirect()->route('medication_show',$row->demographics_id)->with('status', 'Patient medication record updated successfully!');
    }

    public function destroy($id)
    {
        $medication = EnrolmentMedication::where('id','=',$id)->first();
        $demographics_id = $medication->demographics_id;
        $medication->delete();

        return redirect()->route('medication_show',[$demographics_id])->with('status', 'Patient medication record deleted successfully!');
    }
}
