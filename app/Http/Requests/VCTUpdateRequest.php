<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class VCTUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vct_date'              => 'required',
            'first_name'            => 'required|max:255',
            'last_name'             => 'required|max:255',

            'uic_mother_name'       => 'required|max:2',
            'uic_father_name'       => 'required|max:2',
            'uic_birth_order'       => 'required|max:2',

            'mother_first_name'     => 'required|max:255',
            'mother_middle_name'    => 'required|max:255',
            'mother_last_name'      => 'required|max:255',

            'birth_date'            => 'required',
            'street'                => 'max:255',
            'purok'                 => 'max:255',
            'barangay'              => 'max:255',
            'city'                  => 'required|max:255',
            'contact_number'        => 'required|max:255',
            'reason_for_not_testing'=> 'required_if:tested_for_hiv,0|max:255',
            'positive_for_hiv'      => 'required_if:positive_for_hiv,0',

            'ui_code'               => 'unique:vct,ui_code,' . $this->id
        ];
    }
    
    public function messages()
    {
        return [
            'vct_date.required'                 => 'VCT Date is required.',

            'first_name.required'               => 'First Name is required.',
            'first_name.max'                    => 'First Name exceeded to 255 characters.',

            'last_name.required'                => 'Last Name is required.',
            'last_name.max'                     => 'Last Name exceeded to 255 characters.',

            'uic_mother_name.required'          => 'First 2 letters of mother`s name is required.',
            'uic_mother_name.max'               => 'First 2 letters of mother`s name exceeded to 2 characters.',

            'uic_father_name.required'          => 'First 2 letters of fathers name is required.',
            'uic_father_name.max'               => 'First 2 letters of father`s name to 2 characters.',

            'uic_birth_order.required'          => '2-digit birth order is required.',
            'uic_birth_order.max'               => '2-digit birth order exceeded to 2 characters.',

            'mother_first_name.required'        => 'Mother First Name is required.',
            'mother_first_name.max'             => 'Mother First Name exceeded to 255 characters.',

            'mother_middle_name.required'       => 'Mother Middle Name is required.',
            'mother_middle_name.max'            => 'Mother Middle Name exceeded to 255 characters.',

            'mother_last_name.required'         => 'Mother Last Name is required.',
            'mother_last_name.max'              => 'Mother Last Name exceeded to 255 characters.',

            'birth_date.required'               => 'Birth Date is required.',

            'street.max'                        => 'Street exceeded to 255 characters.',
            'purok.max'                         => 'Purok exceeded to 255 characters.',
            'barangay.max'                      => 'Barangay exceeded to 255 characters.',
            'city.required'                     => 'City is required.',
            'city.max'                          => 'City exceeded to 255 characters.',

            'contact_number.required'           => 'Contact Number is required.',
            'contact_number.max'                => 'Contact Number exceeded to 255 characters.',

            'reason_for_not_testing.required_if'=> 'Reason for not testing is required.',
            'reason_for_not_testing.max'        => 'Reason for not testing exceeded to 255 characters..',
            'positive_for_hiv.required_if'      => 'Date of re-test is required.',

            'ui_code.unique'                    => 'Unique Identifier Code already exists.'
        ];
    }
    
    public function getValidatorInstance()
    {
        $data = $this->all();

        $data['ui_code'] = $data['uic_mother_name'] . '-' . $data['uic_father_name'] . '-' . $data['uic_birth_order'];
        $this->getInputSource()->replace($data);

        return parent::getValidatorInstance();
    }
}
