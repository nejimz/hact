<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MedicationStopRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //$this->route();
        return [
            'date_discontinued'         => 'required',
            'discontinuation_reason'    => 'required'
        ];
    }
    
    public function messages()
    {
        return [
            'date_discontinued.required'        => 'Date Discontinued is required.',
            'discontinuation_reason.required'   => 'Reason for Discontinuation is required.'
        ];
    }
}
