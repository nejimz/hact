<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OpportunisticInfectionsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //$this->route();
        return [
            'demographics_id'                   => 'required|unique:enrolment_opportunistic_infections,demographics_id|exists:enrolment_demographics,id',
            'ipt_reason'                        => 'required_if:ipt,0',
            'sti_reason'                        => 'required_if:sti,1',
            'others_reason'                     => 'required_if:others,1',
            'drug_resistance_value_specify'     => 'required_if:drug_resistance_value,Other',
            'treatment_outcome_value_specify'   => 'required_if:treatment_outcome_value,Other'
        ];
    }
    
    public function messages()
    {
        return [
            'demographics_id.required'                      => 'Patient is required.',
            'demographics_id.unique'                        => 'Patient already have a record.',
            'demographics_id.exists'                        => 'Patient not exists enroled in demographics.',
            'ipt_reason.required_if'                        => 'IPT Reason is required.',
            'sti_reason.required_if'                        => 'STI`s Reason is required.',
            'others_reason.required_if'                     => 'Others Reason is required.',
            'drug_resistance_value_specify.required_if'     => 'Drug Resistance Specify is required.',
            'treatment_outcome_value_specify.required_if'   => 'Treatment Outcome Specify is required.'
        ];
    }
}
