<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PasswordResetRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'          => 'required|min:6|max:60|same:password_again'
        ];
    }
    
    public function messages()
    {
        return [
            'password.required'     => 'Password is required.',
            'password.min'          => 'Password must be minimum of 6 characters.',
            'password.max'          => 'Password must be maximum of 60 characters.',
            'password.same'         => 'Password are not the same.'
        ];
    }
}
