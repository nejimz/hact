<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DemographicsUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'            => 'required|max:255',
            'last_name'             => 'required|max:255',

            'uic_mother_name'       => 'required|max:2',
            'uic_father_name'       => 'required|max:2',
            'uic_birth_order'       => 'required|max:2',

            'mother_first_name'     => 'required|max:255',
            'mother_middle_name'    => 'required|max:255',
            'mother_last_name'      => 'required|max:255',

            'gender'                => 'required',
            'civil_status'          => 'required',
            'birth_date'            => 'required',
            'street'                => 'max:255',
            'purok'                 => 'max:255',
            'barangay'              => 'max:255',
            'city'                  => 'required|max:255',
            
            'initial_contract_date' => 'required',
            'enrolment_date'        => 'required',
            'diagnosis_date'        => 'required',
            'code_name'             => 'required|max:255',
            'saccl'                 => 'required|max:255',
            'birth_place_city'      => 'max:255',
            'birth_place_province'  => 'max:255',
            'philheath_number'      => 'max:60',
            'other_hiv_risks'       => 'max:255',
            'attending_physician'   => 'required|exists:users,id',

            'ui_code'               => 'unique:enrolment_demographics,ui_code,' . $this->id
        ];
    }
    
    public function messages()
    {
        return [

            'first_name.required'               => 'First Name is required.',
            'first_name.max'                    => 'First Name exceeded to 255 characters.',

            'last_name.required'                => 'Last Name is required.',
            'last_name.max'                     => 'Last Name exceeded to 255 characters.',

            'uic_mother_name.required'          => 'First 2 letters of mother`s name is required.',
            'uic_mother_name.max'               => 'First 2 letters of mother`s name exceeded to 2 characters.',

            'uic_father_name.required'          => 'First 2 letters of fathers name is required.',
            'uic_father_name.max'               => 'First 2 letters of father`s name to 2 characters.',

            'uic_birth_order.required'          => '2-digit birth order is required.',
            'uic_birth_order.max'               => '2-digit birth order exceeded to 2 characters.',

            'mother_first_name.required'        => 'Mother First Name is required.',
            'mother_first_name.max'             => 'Mother First Name exceeded to 255 characters.',

            'mother_middle_name.required'       => 'Mother Middle Name is required.',
            'mother_middle_name.max'            => 'Mother Middle Name exceeded to 255 characters.',

            'mother_last_name.required'         => 'Mother Last Name is required.',
            'mother_last_name.max'              => 'Mother Last Name exceeded to 255 characters.',

            'gender.required'                   => 'Gender is required.',
            'civil_status.required'             => 'Civil Status is required.',
            'birth_date.required'               => 'Birth Date is required.',

            'street.max'                        => 'Street exceeded to 255 characters.',
            'purok.max'                         => 'Purok exceeded to 255 characters.',
            'barangay.max'                      => 'Barangay exceeded to 255 characters.',
            'city.required'                     => 'City is required.',
            'city.max'                          => 'City exceeded to 255 characters.',

            'contact_number.required'           => 'Contact Number is required.',
            'contact_number.max'                => 'Contact Number exceeded to 255 characters.',

            'initial_contract_date.required'    => 'Initial Contract Date is required.',
            'enrolment_date.required'           => 'Enrolment Date is required.',
            'diagnosis_date.required'           => 'Diagnosis Date is required.',

            'code_name.required'                => 'Code Name is required.',
            'code_name.max'                     => 'Code Name exceeded to 255 characters.',

            'saccl.required'                    => 'SACCL is required.',
            'saccl.max'                         => 'SACCL exceeded to 255 characters.',

            'birth_place_city.max'              => 'Place of Birth City exceeded to 255 characters.',
            'birth_place_province.max'          => 'Place of Birth Province exceeded to 255 characters.',
            'philheath_number.max'              => 'Philheath Number exceeded to 60 characters.',
            'other_hiv_risks.max'               => 'Other HIV Risks exceeded to 255 characters.',

            'attending_physician.required'      => 'Attending Physician is required.',
            'attending_physician.exists'        => 'Attending Physician not exists.',

            'ui_code.unique'                    => 'Unique Identifier Code already exists.'
        ];
    }
    
    public function getValidatorInstance()
    {
        $data = $this->all();

        $data['ui_code'] = $data['uic_mother_name'] . '-' . $data['uic_father_name'] . '-' . $data['uic_birth_order'];
        $this->getInputSource()->replace($data);

        return parent::getValidatorInstance();
    }
}
