<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
//use Requests;

class UserUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //$this->route();
        return [
            'email'         => 'required|unique:users,email,' . $this->id,
            'first_name'    => 'required|max:255',
            'last_name'     => 'required|max:255',
            'role'          => 'required'
        ];
    }
    
    public function messages()
    {
        return [
            'email.required'        => 'Email is required.',
            'email.unique'          => 'Email has been already taken.',

            'first_name.required'   => 'First Name is required.',
            'first_name.max'        => 'First Name exceeded to 255 characters.',

            'last_name.required'    => 'Last Name is required.',
            'last_name.max'         => 'Last Name exceeded to 255 characters.',

            'role.required'         => 'Role is required.'
        ];
    }
}
