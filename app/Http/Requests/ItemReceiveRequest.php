<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ItemReceiveRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_id'   => 'required:exists:item,id',
            'quantity'  => 'required|integer'
        ];
    }
    
    public function messages()
    {
        return [
            'item_id.required'  => 'Item is required.',
            'item_id.exists'    => 'Item not exists.',

            'quantity.required' => 'Quantity is required.',
            'quantity.integer'  => 'Quantity must be a number.'
        ];
    }
}
