<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CheckupRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'checkup_date'      => 'required',
            'blood_pressure'    => 'required',
            'temperature'       => 'required',
            'pulse_rate'        => 'required',
            'respiration_rate'  => 'required',
            'weight'            => 'required',
            'stis_reason'       => 'required_if:stis,1',
            'diagnosed_other_input'  => 'required_if:diagnosed_other,1' ,
            'site'              => 'required_if:tuberculosis,1',
            'regimen'           => 'required_if:tuberculosis,1',
            'drug_resistance_value'  => 'required_if:tuberculosis,1',
            'diagnosed_other_input' => 'required_if:diagnosed_other,1',
            'recommendation'    => 'required'
        ];
    }

    public function messages()
    {
        return [
            // 'checkup_date.required'     => 'Check Up date is required',
            'blood_pressure.required'   => 'Blood Pressure field is Required.',
            'temperature.required'      => 'Temperature field is Required.',
            'pulse_rate.required'       => 'Pulse Rate field is Reequired.',
            'weight.required'           => 'Weight field is Required.',
            'stis_reason.required_if'   => "Please specify STI's reason field.",
            'diagnosed_other_input.required_if'    => 'Please specify if diagnoses are not found in the list.',
            'site.required_if'          => 'Please choose Site type',
            'regimen.required_if'       => 'Please choose TB regimen',
            'drug_resistance_value.required_if'    => 'Please choose Drug Resistance',
            'recommendation.required'   => 'Please put Recommendation'
        ];
    }
}
