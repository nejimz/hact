<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ClinicalAndImmunologicStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //$this->route();
        return [
            'demographics_id'   => 'required|exists:enrolment_demographics,id',
            'laboratory'        => 'required',
            'laboratory_others' => 'required_if:laboratory,others',
            'date_result'       => 'required',
            'result'            => 'required'
        ];
    }
    
    public function messages()
    {
        return [
            'demographics_id.required'      => 'Patient is required.',
            'demographics_id.exists'        => 'Patient not exists in Demographics.',

            'laboratory.required'           => 'Laboratory is required.',
            'laboratory_others.required_if' => 'Others is required.',

            'date_result.required'          => 'Result Date is required.',
            'result.required'               => 'Result is required.'
        ];
    }
}
