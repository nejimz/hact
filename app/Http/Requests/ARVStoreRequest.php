<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ARVStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'demographics_id'               => 'required|exists:enrolment_demographics,id',
            'blood_pressure'                => 'required',
            'temperature'                   => 'required',
            'pulse_rate'                    => 'required',
            'respiration_rate'              => 'required',
            'weight'                        => 'required',
            'drug_resistance_value_specify' => 'required_if:drug_resistance_value,Other',
            'stis_reason'                   => 'required_if:stis,1',
            'diagnosed_other_input'         => 'required_if:diagnosed_other,1',
            'classification'                => 'required',
            'arv_date'                      => 'required'

        ];
    }

    public function messages()
    {
        return [
            'demographics_id.required'  => 'Patient is required.',
            'demographics_id.exists'    => 'Patient not exists.',
            'blood_pressure.required'   => 'Blood Pressure is required',
            'temperature.required'      => 'Temperature is required',
            'pulse_rate.required'       => 'Pulse Rate is required',
            'weight.required'           => 'Weight is Required',

            'drug_resistance_value_specify.required_if' => 'Other Drug Resistance Specify is required',
            'sti_reason.required_if'                    => 'STI`s Reason is required',
            'diagnosed_other_input.required_if'         => 'Other Diagnosed is required',
            'classification.required'                   => 'Classification is required',
            'arv_date.required'                         => 'ARV Date is required'
        ];
    }
}
