<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ItemDispenseRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'demographics_id'   => 'required|exists:enrolment_demographics,id',
            'item_id'           => 'required|exists:item,id',
            'quantity'          => 'required|integer',
        ];
    }
    
    public function messages()
    {
        return [
            'demographics_id.required'  => 'Patient is required.',
            'demographics_id.exists'    => 'Patient not exists.',

            'item_id.required'          => 'Medicine is required.',
            'item_id.exists'            => 'Medicine not exists.',

            'quantity.required'         => 'Quantity is required.',
            'quantity.integer'          => 'Quantity must be a number.'
        ];
    }
}
