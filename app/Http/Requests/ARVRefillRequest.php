<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ARVRefillRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //$this->route();
        return [
            'demographics_id'   => 'required|exists:enrolment_demographics,id',

            'blood_pressure'    => 'required',
            'temperature'       => 'required',
            'pulse_rate'        => 'required',
            'respiration_rate'  => 'required',
            'weight'            => 'required',

            'change_arv_regimen'=> 'required',
            'change_arv_regimen_yes_reason' => 'required_if:change_arv_regimen,1',
            'date_change'       => 'required_if:change_arv_regimen,1',
            'new_arv_regimen'   => 'required_if:change_arv_regimen,1',

            'recommendations'   => 'required'
        ];
    }
    
    public function messages()
    {
        return [
            'demographics_id.required'  => 'Patient is required.',
            'demographics_id.exists'    => 'Patient not exists.',

            'blood_pressure.required'   => 'Blood Pressure is required.',
            'temperature.required'      => 'Temperature is required.',
            'pulse_rate.required'       => 'Pulse Rate is required.',
            'respiration_rate.required' => 'Respiration Rate is required.',
            'weight.required'           => 'Weight is required.',

            'change_arv_regimen.required'   => 'Change ARV Regimen is required.',
            'change_arv_regimen_yes_reason.required_if' => 'Reason is required.',
            'date_change.required_if'       => 'Date Change is required.',
            'new_arv_regimen.required_if'   => 'New ARV Regimen is required.',

            'recommendations.required'      => 'Recommendations is required.'
        ];
    }
}
