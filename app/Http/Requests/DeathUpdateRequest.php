<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DeathUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'demographics_id'   => 'required|unique:event_death,demographics_id,' . $this->id . '|exists:enrolment_demographics,id',
            'death_date'        => 'required',
            'death_cause'       => 'required',
            'final_diagnosis'   => 'required'

        ];
    }

    public function messages()
    {
        return [
            'demographics_id.required'  => 'Patient is required.',
            'demographics_id.unique'    => 'Patient already exists.',
            'demographics_id.exists'    => 'Patient not exists.',

            'death_date.required'       => 'Date of Death is required',
            'death_cause.required'      => 'Cause of Death is required',
            'final_diagnosis.required'  => 'Final Diagnosis is required'
        ];
    }
}
