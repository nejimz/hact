<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ItemStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_name'             => 'required|max:255|unique:item',
            'item_code'             => 'required|max:255',
            'lot_number'            => 'required|max:255',
            'quantity_per_bottle'   => 'required|max:10000|integer',
            'expiration_date'       => 'required|date'
        ];
    }
}
