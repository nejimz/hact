<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MedicationStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //$this->route();
        return [
            'demographics_id'   => 'required|exists:enrolment_demographics,id',
            'item_id'           => 'required:exists:item,id',
            'dosage'            => 'required',
            'frequency'         => 'required',
            'date_started'      => 'required'
        ];
    }
    
    public function messages()
    {
        return [
            'demographics_id.required'  => 'Patient is required.',
            'demographics_id.exists'    => 'Patient not exists.',
            'item_id.required'          => 'Name of Medication is required.',
            'item_id.exists'            => 'Name of Medication not exists.',
            'dosage.required_if'        => 'Dosage is required.',
            'frequency.required_if'     => 'Frequency is required.',
            'date_started.required_if'  => 'Date Started is required.'
        ];
    }
}
