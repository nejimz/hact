<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Death extends Model
{
    protected $table = 'event_death';

    public $timestamps = false;

    /**
    Relationships
    **/

    public function EnrolmentDemographics()
    {
        return $this->hasOne('App\EnrolmentDemographics', 'id', 'demographics_id');
    }

    /*public function EnrolmentDemographicsData()
    {
        return $this->belongsTo('App\EnrolmentDemographics', 'id', 'demographics_id');
    }*/

    /**
    Setters to Insert
    **/

    public function setDeathDateAttribute($value)
    {
    	$this->attributes['death_date'] = date('Y-m-d', strtotime($value));
    }

    public function setTuberculosisAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = 1;
    	}

    	$this->attributes['tuberculosis'] = $value;
    }

    public function setPneumocysticPneumoniaAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = 1;
    	}

    	$this->attributes['pneumocystic_pneumonia'] = $value;
    }

    public function setCryptococcalMeningitisAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = 1;
    	}

    	$this->attributes['cryptococcal_meningitis'] = $value;
    }

    public function setCytomegalovirusAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = 1;
    	}

    	$this->attributes['cytomegalovirus'] = $value;
    }

    public function setCandidiasisAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = 1;
    	}

    	$this->attributes['candidiasis'] = $value;
    }

    public function setToxoplasmosisAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = 1;
    	}

    	$this->attributes['toxoplasmosis'] = $value;
    }

    public function setNoneAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = 1;
    	}

    	$this->attributes['none'] = $value;
    }

    /**
    Getters
    **/

    public function getDeathDateFormatAttribute()
    {
    	return date('F d, Y', strtotime($this->death_date));
    }
       
}
