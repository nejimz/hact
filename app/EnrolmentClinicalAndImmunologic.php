<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnrolmentClinicalAndImmunologic extends Model
{
    //
    protected $table = 'enrolment_clinical_and_immunologic';

    public $timestamps = false;

    /**
    Relationships
    **/

    public function EnrolmentDemographics()
    {
        return $this->hasOne('App\EnrolmentDemographics', 'id', 'demographics_id');
    }

    /**
    Setters to Insert
    **/

    public function setDateResultAttribute($value)
    {
    	$this->attributes['date_result'] = date('Y-m-d', strtotime($value));
    }

    public function setLaboratoryOthersAttribute($value)
    {
    	if(is_null($value))
    	{
    		$value = '';
    	}

    	$this->attributes['laboratory_others'] = $value;
    }

    public function getDateResultAttribute($value)
    {
        return date('F d, Y', strtotime($value));
    }

    /**
    Other functionality
    **/
    
    public function scopeSearchClinicalAndImmunologicList($query, $search)
    {
        $search = '%' . $search . '%';

        return $query->where('enrolment_demographics.first_name', 'LIKE', $search)
                    ->orWhere('enrolment_demographics.last_name', 'LIKE', $search)
                    ->orWhere('enrolment_demographics.code_name', 'LIKE', $search)
                    ->orWhere('laboratory', 'LIKE', $search)
                    ->orWhere('result', 'LIKE', $search);
    }
}
