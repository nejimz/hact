<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemDispense extends Model
{
    protected $table = 'item_dispense';

    public function setTransactionDateAttribute($value)
    {
        $this->attributes['transaction_date'] = date("Y-m-d", strtotime($value));
    }

    public function scopeSearchItemDispenseList($query, $search)
    {
        $search = '%' . $search . '%';

        return $query->where('item.item_name', 'LIKE', $search)
                    ->orWhere('quantity', 'LIKE', $search);
    }

    public function Item()
    {
        return $this->belongsTo('App\Item', 'id', 'id');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public $timestamps = false;
}
