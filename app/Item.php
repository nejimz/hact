<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\ItemReceive;
use App\ItemDispense;

class Item extends Model
{
	use SoftDeletes;
    protected $table = 'item';
    protected $dates = ['deleted_at'];

    public $timestamps = true;

    /**
    Relationships
    **/

    public function scopeSearchItemList($query, $search)
    {
        $search = '%' . $search . '%';

        return $query->where('item_code', 'LIKE', $search)
                    ->orWhere('item_name', 'LIKE', $search)
                    ->orWhere('lot_number', 'LIKE', $search);
    }
    
    public function EnrolmentMedication()
    {
        return $this->belongsTo('App\EnrolmentMedication', 'id', 'id');
    }

    public function ItemReceive()
    {
        return $this->hasMany('App\ItemReceive', 'id', 'item_id');
    }

    public function ItemDispense()
    {
        return $this->hasMany('App\ItemDispense', 'id', 'item_id');
    }

    public function user()
    {
        return $this->hasOne('App\User');
    }

    /**
    Setters to Insert
    **/

    public function setExpirationDateAttribute($value)
    {
        $this->attributes['expiration_date'] = date("Y-m-d", strtotime($value));
    }

    /**
    Getters to Insert
    **/

    public function getCurrentQuantityAttribute()
    {
        return $this->attributes['quantity_per_bottle'] - $this->attributes['quantity'];
    }

    public function getTotalQuantityAttribute()
    {
        $id = $this->id;
        $total_receive  = ItemReceive::where('item_id', $id)->sum('quantity');
        $total_dispense = ItemDispense::where('item_id', $id)->sum('quantity');
        $total_quantity = $total_receive - $total_dispense;

        return $total_quantity;
    }

    /**/
}
