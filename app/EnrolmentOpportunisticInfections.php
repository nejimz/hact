<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnrolmentOpportunisticInfections extends Model
{
    protected $table = 'enrolment_opportunistic_infections';

    public $timestamps = false;

    public function EnrolmentDemographics()
    {
        return $this->hasOne('App\EnrolmentDemographics', 'id', 'demographics_id');
    }

    public function scopeSearchOpportunisticInfectionsList($query, $search)
    {
        $search = '%' . $search . '%';

        return $query->where('enrolment_demographics.first_name', 'LIKE', $search)
                    ->orWhere('enrolment_demographics.last_name', 'LIKE', $search)
                    ->orWhere('enrolment_demographics.code_name', 'LIKE', $search);
    
    }

    public function setHepatitisBAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = $value;
    	}

    	$this->attributes['hepatitis_b'] = $value;
    }

    public function setHepatitisCAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = $value;
    	}

    	$this->attributes['hepatitis_c'] = $value;
    }

    public function setPneumocystisPneumoniaAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = $value;
    	}

    	$this->attributes['pneumocystis_pneumonia'] = $value;
    }

    public function setOropharyngealCandidiasisAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = $value;
    	}

    	$this->attributes['oropharyngeal_candidiasis'] = $value;
    }

    public function setSyphilisAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = $value;
    	}

    	$this->attributes['syphilis'] = $value;
    }

    public function setStisAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = $value;
    	}

    	$this->attributes['stis'] = $value;
    }

    public function setStisSpecifyAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = $value;
    	}

    	$this->attributes['stis_specify'] = $value;
    }

    public function setOthersAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = $value;
    	}

    	$this->attributes['others'] = $value;
    }

    public function setOthersSpecifyAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = $value;
    	}

    	$this->attributes['others_specify'] = $value;
    }

    public function setSiteValueAttribute($value)
    {
        if(is_null($value)){
            $value = 0;
        }else{
            $value = $value;
        }

        $this->attributes['site_value'] = $value;
    }

    public function setTbRegimenValueAttribute($value)
    {
        if(is_null($value)){
            $value = '';
        }else{
            $value = $value;
        }

        $this->attributes['tb_regimen_value'] = $value;
    }

    public function setDrugResistanceValueAttribute($value)
    {
        if(is_null($value)){
            $value = '';
        }else{
            $value = $value;
        }

        $this->attributes['drug_resistance_value'] = $value;
    }

    public function setDrugResistanceValueSpecifyAttribute($value)
    {
        if(is_null($value)){
            $value = '';
        }else{
            $value = $value;
        }

        $this->attributes['drug_resistance_value_specify'] = $value;
    }

    public function setTreatmentOutcomeValueAttribute($value)
    {
        if(is_null($value)){
            $value = '';
        }else{
            $value = $value;
        }

        $this->attributes['treatment_outcome_value'] = $value;
    }

    public function setTreatmentOutcomeValueSpecifyAttribute($value)
    {
        if(is_null($value)){
            $value = '';
        }else{
            $value = $value;
        }

        $this->attributes['treatment_outcome_value_specify'] = $value;
    }

}
