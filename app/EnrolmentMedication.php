<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnrolmentMedication extends Model
{
    //
    protected $table = 'enrolment_medication';

    public $timestamps = false;


    public function scopeMedicationList($query, $search)
    {
        $search = '%' . $search . '%';

        return $query->where('enrolment_demographics.first_name', 'LIKE', $search)
                    ->orWhere('enrolment_demographics.last_name', 'LIKE', $search)
                    ->orWhere('enrolment_demographics.code_name', 'LIKE', $search)
                    ->orWhere('item.item_name', 'LIKE', $search)
                    ->orWhere('dosage', 'LIKE', $search)
                    ->orWhere('date_started', 'LIKE', $search)
                    ->orWhere('frequency', 'LIKE', $search);
    }

    /**
    Relationships
    **/

    public function EnrolmentDemographics()
    {
        return $this->hasOne('App\EnrolmentDemographics', 'id', 'demographics_id');
    }

    public function Item()
    {
        return $this->hasOne('App\Item', 'id', 'item_id');
    }

    /**
    Setters to Insert
    **/

    public function setDateStartedAttribute($value)
    {
        $this->attributes['date_started'] = date('Y-m-d', strtotime($value));
    }

    public function setDateDiscontinuedAttribute($value)
    {
        $this->attributes['date_discontinued'] = date('Y-m-d', strtotime($value));
    }

    /**
    Getters to Select
    **/

    public function getDateStartedFormattedAttribute()
    {
        return date('F d, Y', strtotime($this->date_started));
    }

    public function getDateDiscontinuedFormattedAttribute()
    {
        if(is_null($this->date_discontinued))
        {
            return '';
        }
        else
        {
            return date('F d, Y', strtotime($this->date_discontinued));
        }
    }
}
