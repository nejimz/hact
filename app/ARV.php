<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ARV extends Model
{
    //
    protected $table = 'event_arv';
    #protected $fillable = ['role_name'];

    public $timestamps = false;

    /**
    Relationships
    **/

    public function EnrolmentDemographics()
    {
        return $this->hasOne('App\EnrolmentDemographics', 'id', 'demographics_id');
    }

    /**
    Setters to Insert
    **/

    public function setRegimenAttribute($value)
    {
        if(is_null($value)){
            $value = 0;
        }else{
            $value = 1;
        }

        $this->attributes['regimen'] = $value;
    }

    public function setArvDateAttribute($value)
    {
        $this->attributes['arv_date'] = date('Y-m-d', strtotime($value));
    }

    public function setTuberculosisAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = 1;
    	}

    	$this->attributes['tuberculosis'] = $value;
    }

    public function setHepatitisBAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = 1;
    	}

    	$this->attributes['hepatitis_b'] = $value;
    }

    public function setHepatitisCAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = 1;
    	}

    	$this->attributes['hepatitis_c'] = $value;
    }

    public function setPneumocystisPneumoniaAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = 1;
    	}

    	$this->attributes['pneumocystis_pneumonia'] = $value;
    }

    public function setOropharyngealCandidiasisAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = 1;
    	}

    	$this->attributes['oropharyngeal_candidiasis'] = $value;
    }

    public function setSyphilisAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = 1;
    	}

    	$this->attributes['syphilis'] = $value;
    }

    public function setStisAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = 1;
    	}

    	$this->attributes['stis'] = $value;
    }

    public function setDiagnosedOtherAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = 1;
    	}

    	$this->attributes['diagnosed_other'] = $value;
    }

    public function setDiagnosedOtherInputAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = 1;
    	}

    	$this->attributes['diagnosed_other_input'] = $value;
    }

    public function setLowCd4CountAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = 1;
    	}

    	$this->attributes['low_cd4_count'] = $value;
    }

    public function setActiveTbAttribute($value)
    {
        if(is_null($value)){
            $value = 0;
        }else{
            $value = 1;
        }

        $this->attributes['active_tb'] = $value;
    }

    public function setChild5yrAttribute($value)
    {
        if(is_null($value)){
            $value = 0;
        }else{
            $value = 1;
        }

        $this->attributes['child_5yr'] = $value;
    }

    public function setHepBCAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = 1;
    	}

    	$this->attributes['hep_b_c'] = $value;
    }

    public function setPregnantBreastfeedingAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = 1;
    	}

    	$this->attributes['pregnant_breastfeeding'] = $value;
    }

    public function setWhoClassificationAttribute($value)
    {
    	if(is_null($value)){
    		$value = 0;
    	}else{
    		$value = 1;
    	}

    	$this->attributes['who_classification'] = $value;
    }

    /**
    Getters
    **/

    /**
    Other functionality
    **/

    public function getArvDateFormatAttribute()
    {
        return date('F d, Y', strtotime($this->arv_date));
    }
}