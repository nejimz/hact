<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemCategory extends Model
{
    protected $table = 'item_category';

    public $timestamps = false;

    public function setTransactionDateAttribute($value)
    {
        $this->attributes['transaction_date'] = date("Y-m-d", strtotime($value));
    }

    public function Item()
    {
        return $this->belongsTo('App\Item', 'id', 'id');
    }
}
