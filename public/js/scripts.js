
var xhr_user_reset_password = null;
$(function(){
	$('#user_reset_password').click(function(e){

        xhr_user_reset_password = $.ajax({
            cache : false,
            url : $('#user_reset_password_url').val(),
            dataType : "json",
            beforeSend: function(xhr) 
            {
                if (xhr_user_reset_password != null){
                    xhr_user_reset_password.abort();
                }
            }
        }).done(function(data) {
        	$('#generated_password').val(data['password'])
        }).fail(function(jqXHR, textStatus) {
            //console.log("Fail");
            alert('Pls. refresh your browser!');
        });

		e.preventDefault();
	});
});


var xhr_item_search = null;
$(function(){
    $("#item_search").autocomplete({
        source: function (request, response) 
        {
            xhr_item_search = $.ajax({
                type : 'get',
                url : $("#search_item_url").val(),
                data : 'item_search=' + request.term,
                cache : false,
                dataType : "json",
                beforeSend: function(xhr) 
                {
                    if (xhr_item_search != null)
                    {
                        xhr_item_search.abort();
                    }
                }
            }).done(function(data) 
            {
                response($.map( data, function(value, key) 
                {
                    return { label: value, value: key }
                }));

            }).fail(function(jqXHR, textStatus) 
            {
                //console.log('Request failed: ' + textStatus);
            });
        }, 
        minLength: 2,
        autoFocus: true,
        select: function(a, b)
        {
            var id = b.item.value;
            var name = b.item.label;

            $('#item_id').val(id);
            $('#item_search').val(name).focus();
            return false;
        }
    });
});

var xhr_item_search = null;
$(function(){
    $("#search_physician").autocomplete({
        source: function (request, response) 
        {
            xhr_item_search = $.ajax({
                type : 'get',
                url : $("#search_physician_url").val(),
                data : 'search_physician=' + request.term,
                cache : false,
                dataType : "json",
                beforeSend: function(xhr) 
                {
                    if (xhr_item_search != null)
                    {
                        xhr_item_search.abort();
                    }
                }
            }).done(function(data) 
            {
                response($.map( data, function(value, key) 
                {
                    return { label: value, value: key }
                }));

            }).fail(function(jqXHR, textStatus) 
            {
                //console.log('Request failed: ' + textStatus);
            });
        }, 
        minLength: 2,
        autoFocus: true,
        select: function(a, b)
        {
            var id = b.item.value;
            var name = b.item.label;

            $('#attending_physician').val(id);
            $(this).val(name).focus();
            return false;
        }
    });
});

$(function(){
    $(document).foundation({
        "magellan-expedition": {
            active_class: 'active', // specify the class used for active sections
            threshold: 0, // how many pixels until the magellan bar sticks, 0 = auto
            destination_threshold: 20, // pixels from the top of destination for it to be considered active
            throttle_delay: 50, // calculation throttling to increase framerate
            fixed_top: 0, // top distance in pixels assigend to the fixed element on scroll
            offset_by_height: true // whether to offset the destination by the expedition height. Usually you want this to be true, unless your expedition is on the side.
        }
    });
});

$(function(){
    $("input[name='expiration_date'], .fdatepicker").fdatepicker({
        format: 'MM dd, yyyy',
        disableDblClickSelection: true
    });
});

$(function(){
    $("input[name='birth_date']").on('change', function(){
        var date    = $(this).val();
        var age     = getAge(date);
        $("input[name='age']").val(age);
    });
});

function getAge(getDate)
{
    dob = new Date(getDate);
    var today   = new Date();
    var age     = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
    return age;
}




