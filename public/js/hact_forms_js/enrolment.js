$('document').ready(function(){
	$('#others_name').prop('disabled',true);
	$('#provided_post_exposure_prophylaxis_yes').prop('disabled',true);
	$('#provided_post_exposure_prophylaxis_no').prop('disabled',true);

	$('#others').click(function(){
		if($(this).is(':checked')==true){
			$('#others_name').prop('disabled',false);
		}else{
			$('#others_name').prop('disabled',true);
		}
	});
	$('#occupational_exposure').click(function(){
		if($(this).is(':checked')==true){
			$('#provided_post_exposure_prophylaxis_yes').prop('disabled',false);
			$('#provided_post_exposure_prophylaxis_no').prop('disabled',false);
		}else{
			$('#provided_post_exposure_prophylaxis_yes').prop('disabled',true);
			$('#provided_post_exposure_prophylaxis_no').prop('disabled',true);
		}
	});
	$('#tuberculosis').click(function(){
		if($(this).is(':checked') == true){
			$('#pulmonary').prop('disabled', false);
		}
		else{
			$('#pulmonary').prop('disabled', true);
		}
	})

});

