$('document').ready(function(){
	$('#provided_post_exposure_prophylaxis_yes').prop('disabled',true);
	$('#provided_post_exposure_prophylaxis_no').prop('disabled',true);
	$('#occupational_exposure').click(function(){
		if($(this).is(':checked')==true){
			$('#provided_post_exposure_prophylaxis_yes').prop('disabled',false);
			$('#provided_post_exposure_prophylaxis_no').prop('disabled',false);
		}else{
			$('#provided_post_exposure_prophylaxis_yes').prop('disabled',true);
			$('#provided_post_exposure_prophylaxis_no').prop('disabled',true);
		}
	});

	$('#positive_for_hiv_yes').prop('disabled',true);	
		$('#positive_for_hiv_no').prop('disabled',true);
		$('#provided_post-test_counseling_and_hiv_result_yes').prop('disabled',true);
		$('#provided_post-test_counseling_and_hiv_result_no').prop('disabled',true);		
	
	$('#tested_for_hiv_yes').click(function(){
		if($(this).is(':checked')==true){
			$('#positive_for_hiv_yes').prop('disabled',false);	
			$('#positive_for_hiv_no').prop('disabled',false);
		}else{
			$('#positive_for_hiv_yes').prop('disabled',true);	
			$('#positive_for_hiv_no').prop('disabled',true);	
			$('#provided_post-test_counseling_and_hiv_result_yes').prop('disabled',true);	
			$('#provided_post-test_counseling_and_hiv_result_no').prop('disabled',true);
		}
	});

	$('#tested_for_hiv_no').click(function(){
		if($(this).is(':checked')==true){
			$('#positive_for_hiv_yes').prop('disabled',true);	
			$('#positive_for_hiv_no').prop('disabled',true);
			$('#provided_post-test_counseling_and_hiv_result_yes').prop('disabled',true);	
			$('#provided_post-test_counseling_and_hiv_result_no').prop('disabled',true);	
		}else{
			$('#positive_for_hiv_yes').prop('disabled',false);	
			$('#positive_for_hiv_no').prop('disabled',false);
			$('#provided_post-test_counseling_and_hiv_result_yes').prop('disabled',false);	
			$('#provided_post-test_counseling_and_hiv_result_no').prop('disabled',false);		
		}
	});

	$('#positive_for_hiv_yes').click(function(){
		if($(this).is(':checked')==true){
			$('#provided_post-test_counseling_and_hiv_result_yes').prop('disabled',false);	
			$('#provided_post-test_counseling_and_hiv_result_no').prop('disabled',false);	
		}else{
			$('#provided_post-test_counseling_and_hiv_result_yes').prop('disabled',true);	
			$('#provided_post-test_counseling_and_hiv_result_no').prop('disabled',true);	
		}
	});

	$('#positive_for_hiv_no').click(function(){
		if($(this).is(':checked')==true){
			$('#provided_post-test_counseling_and_hiv_result_yes').prop('disabled',true);	
			$('#provided_post-test_counseling_and_hiv_result_no').prop('disabled',true);	
		}else{
			$('#provided_post-test_counseling_and_hiv_result_yes').prop('disabled',false);	
			$('#provided_post-test_counseling_and_hiv_result_no').prop('disabled',false);	
		}
	});




});

